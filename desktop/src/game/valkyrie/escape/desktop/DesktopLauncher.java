package game.valkyrie.escape.desktop;

import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import game.valkyrie.escape.*;

public class DesktopLauncher {
	
	public static void main (String[] arg) {
		
		// Sets up a config file, which is then passed into the Application Handler alongside our game.
		
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.foregroundFPS = 60; // 60 Frames per Second, can be changed as we're using Delta Time.
		config.backgroundFPS = 10; // When not tabbed into the game, set to 10 fps.
		config.width = EscapeGame.SCREEN_WIDTH; // 1280 pixels.
		config.height = EscapeGame.SCREEN_HEIGHT; // 720 pixels.
		config.resizable = false; // Do not allow resizing of screen (which will skew aspect ratio).
		config.addIcon("sprites/game_icon.png", FileType.Internal);
		new LwjglApplication(new EscapeGame(), config); // Launch Game.
		
	}
}
