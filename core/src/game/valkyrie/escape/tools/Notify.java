package game.valkyrie.escape.tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Align;

import game.valkyrie.escape.EscapeGame;

/**
 * Notify is a classed used to display point values/strings gained during minigames and on the map screen
 * @author LukeC, Team Valkyrie
 *
 */
public class Notify {

	private static final int SPEED = 40;
	
	float x; // X coordinate of the item
	float y; // Y coordinate of the item
	float timer; // Time till notify item is set to be removed
	String message; // Message to display by notify
	BitmapFont uiFont; // Font of the notify item
	boolean toRemove; // When set to false, delete from game
	
	public Notify(EscapeGame game, String message, float x, float y) {
		
		this.x = x;
		this.y = y;
		this.message = message;
		timer = 0.8f;
		uiFont = new BitmapFont(Gdx.files.internal("fonts/lunchtime.fnt"));
		uiFont.getData().setScale(0.7f);
		toRemove = false;
		
	}
	
	/**
	 * Update the notify items position, set to remove once timer is cleared
	 * @param deltaTime Delta time
	 */
	public void update(float deltaTime) {
		
		y += SPEED * deltaTime;
		timer -= deltaTime;
		
		if (timer < 0) {
			toRemove = true;
		}
		
	}
	
	/**
	 * Render the notify item
	 * @param batch Sprite Batch of EscapeGame
	 */
	public void render(SpriteBatch batch) {
		
		GlyphLayout messageLayout = new GlyphLayout(uiFont, "" + message, Color.YELLOW, 1f, Align.center, true);
		uiFont.draw(batch, messageLayout, x, y + 10); 
		
	}
	
	/**
	 * 
	 * @return If item is to be removed on the enxt frame
	 */
	public boolean toBeRemoved() {
		return toRemove;
	}
	
}
