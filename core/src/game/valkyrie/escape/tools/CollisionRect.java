package game.valkyrie.escape.tools;

/**
 * CollisionRect is used to determine if two objects are overlapping therefore colliding, this is used universally and provides the basis for collision checks between objects that don't used Tiled's map collision layers
 * @author LukeC, Team Valkyrie
 *
 */
public class CollisionRect {
	
	// Holds the X, Y, Width, Height of a Texture/Sprite/Object e.c.t.
	
	float x, y;
	int width, height;
	
	// Passes variables of an object
	
	public CollisionRect(float x, float y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	// Updates values when object moves
	
	public void move (float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	// Checks to see if another object is colliding with this current, returns true if so
	
	public boolean collidesWith(CollisionRect rect) {
		return x < rect.x + rect.width && y < rect.y + rect.height && x + width > rect.x && y + height > rect.y;
		
	}
		
}
