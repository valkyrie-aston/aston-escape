package game.valkyrie.escape.tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Align;

/**
 * Timer is an object that displays a time that counts downwards
 * @author LukeC. Team Valkyrie
 *
 */
public class Timer {
	
	float x; // X coordinate of the timer
	float y; // Y coordinate of the timer
	BitmapFont uiFont; // Font of the timer 
	float time; // time the timer displays 
	String timeString; // String of the time
	String timeStringTwo; // This is a shortened version, displaying only the first two characters
	
	public Timer (float x, float y, float time) {
		this.x = x;
		this.y = y;
		this.time = time;
		timeString = "";
		timeStringTwo = "";
		
		// Load font
		uiFont = new BitmapFont(Gdx.files.internal("fonts/lunchtime.fnt"));
	}
	
	/**
	 * Update the time and change the converted string if time is < 10
	 * @param delta Delta time
	 */
	public void update(float delta) {
		time -= delta;
		timeString = Float.toString(time); // Convert time from float to string
		
		 // Convert to two point string, or one point string if < 10
		if (time < 10) {
		timeStringTwo = timeString.substring(0, 1);
		} else  {
		timeStringTwo = timeString.substring(0, 2);
		}
	}
	
	/**
	 * Render the time
	 * @param batch Sprite Batch of EscapeGame
	 */
	public void render(SpriteBatch batch) {
		GlyphLayout timerHeaderLayout = new GlyphLayout(uiFont, "TIME:", Color.WHITE, 0, Align.left, false);
		GlyphLayout timerTimeLayout = new GlyphLayout(uiFont, "" + timeStringTwo, Color.YELLOW, 0, Align.left, false);
		uiFont.draw(batch, timerHeaderLayout, x, y);
		uiFont.draw(batch, timerTimeLayout, x, y - 40);

	}
	
	/**
	 * Get the time displayed on the timer
	 * @return The time
	 */
	public float getTime() {
		return time;
	}
	
}
