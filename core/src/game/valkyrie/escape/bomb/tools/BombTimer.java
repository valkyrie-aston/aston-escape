package game.valkyrie.escape.bomb.tools;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Align;

/**
 * This class is used to generate a timer beneath BombZombie and BombPlayer to represent the time they can hold the bomb.
 * @author LukeC, Team Valkyrie
 *
 */
public class BombTimer {
	
	float x; // X coordinate of the timer
	float y; // Y coordinate of the timer
	BitmapFont uiFont; // Font of the timer
	float time; // Time shown by timer
	String timeString; // Time shown by timer as string
	String timeStringFive; // This is a shortened version, displaying only the first two characters
	Color setColor; // Colour of the timer
	
	public BombTimer (float x, float y, float time, Color setColor) {
		this.x = x;
		this.y = y;
		this.time = time;
		timeString = "";
		timeStringFive = "";
		this.setColor = setColor;
		
		// Load font
		uiFont = new BitmapFont(Gdx.files.internal("fonts/lunchtime.fnt"));
	}
	
	/**
	 * Method to update the timer, if time is below 10 seconds, use a shorter string
	 * @param time Value to update time variable
	 */
	public void update(float time) {
		
		this.time = time;
		timeString = Float.toString(time); // Convert time from float to string
		
		 // Convert to three point string
		if (time >= 10) {
			timeStringFive = timeString.substring(0, 4);
		} else {
			timeStringFive = timeString.substring(0, 3);
		}
		
	}
	
	/**
	 * Render the timer
 	 * @param batch Sprite Batch of EscapeGame
	 */
	public void render(SpriteBatch batch) {
		
		GlyphLayout timerTimeLayout = new GlyphLayout(uiFont, "" + timeStringFive, setColor, 0, Align.center, false);
		uiFont.draw(batch, timerTimeLayout, x, y);

	}
	
	/**
	 * Get the time held by this object
	 * @return Time
	 */
	public float getTime() {
		return time;
	}
	
	/**
	 * Change colour of the text
	 * @param setColor Colour to change to
	 */
	public void setColor (Color setColor) {
		this.setColor = setColor;
	}
	
}

