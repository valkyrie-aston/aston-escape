package game.valkyrie.escape.bomb.entities;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import game.valkyrie.escape.bomb.tools.BombTimer;
import game.valkyrie.escape.tools.CollisionRect;

/**
 * The BombPlayer is the play character who can throw the bomb to BombZombie in BombGameScreen
 * @author LukeC, Team Valkyrie
 *
 */
public class BombPlayer {
	
	// Many static variables that are used within the class several times.
	public static final int PLAYER_WIDTH = 64;
	public static final int PLAYER_HEIGHT = 52;
	public static final float DEFAULT_X = 400;
	public static final float DEFAULT_Y = Gdx.graphics.getHeight() / 2 - PLAYER_HEIGHT / 2;
	public static final float MIN_BOMB_HOLD_TIME = 0.5f; // Minimum hold time for bomb
	public static final float MAX_BOMB_HOLD_TIME = 2.0f; // Maximum hold time for bomb
	
	float holdTimer; // How long the player can hold onto the bomb.
	float x; // The x coordinate of the player
	float y; // The y coordinate of the player
	Texture playerTexture; // The texture of the player sprite
	CollisionRect rect; // Player collision Rectangle
	Random random; // Random variable generator
	boolean holdingBomb; // If player is holding onto bomb
	BombTimer timerDisplay; // Timer display when holding the bomb
	
	public BombPlayer() {
		
		x = DEFAULT_X;
		y = DEFAULT_Y;
		holdTimer = 0;
		holdingBomb = false;
		random = new Random();
		timerDisplay = new BombTimer(DEFAULT_X + 20, DEFAULT_Y - 20, 100, Color.WHITE); // Create timer 20 units below player sprite
		
		// Load texture
		playerTexture = new Texture("sprites/bomb_player.png");
		rect = new CollisionRect(x, y, PLAYER_WIDTH, PLAYER_HEIGHT);
		
	}
	
	/**
	 * Update the amount of time the player can hold the bomb and the timer to represent this
	 * @param delta delta time
	 */
	public void update(float delta) {
		
		// If player is holding a bomb, reduce the bomb counter.
		if (holdingBomb()) {
			holdTimer -= delta;
		}
		
		timerDisplay.update(holdTimer);
		rect.move(x, y);
	}
	
	/**
	 * Render the player sprite alongside the hold timer
	 * @param batch The Sprite Batch of EscapeGame
	 */
	public void render (SpriteBatch batch) {
		
		batch.draw(playerTexture, x, y, PLAYER_WIDTH, PLAYER_HEIGHT);
		
		// If player is holding bomb, show how much time is remaining before it is thrown
		if (holdingBomb() && holdTimer > 0) {
		timerDisplay.render(batch);
		}
		
	}
		
	/**
	 * Return the collision rectangle
	 * @return The collision rectangle of the player
	 */
	public CollisionRect getCollisionRect() {
		return rect;
	}
	
	/**
	 * 
	 * @return The x float value of the player
	 */
	public float getX() {
		return x;
	}
	
	/**
	 * 
	 * @return The y float value of the player
	 */
	public float getY() {
		return y;
	}
	
	/**
	 * 
	 * @return True if the player is holding the bomb
	 */
	public boolean holdingBomb() {
		return holdingBomb;
	}
	
	/**
	 * Set the player to be holding the bomb and determine a random value for how long they can hold the bomb for
	 */
	public void holdBomb() {
		holdingBomb = true;
		holdTimer = random.nextFloat() * (MAX_BOMB_HOLD_TIME - MIN_BOMB_HOLD_TIME) + MIN_BOMB_HOLD_TIME;
	}
	
	/**
	 * Set the player to not be holding the bomb and set the holdTimer to an initial value of 0
	 */
	public void throwBomb() {
		holdTimer = 0;
		holdingBomb = false;
	}
	
	/**
	 * @return How long the player can hold the bomb for before it is automatically thrown
	 */
	public float getHoldTimer() {
		return holdTimer;
	}
	
}
