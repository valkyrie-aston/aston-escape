package game.valkyrie.escape.bomb.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * The BombExplosion is an animated texture that is created once the bomb explodes, it plays once before the game is over, and is then disposed
 * @author LukeC, Team Valkyrie
 *
 */
public class BombExplosion {
	
	// Many static variables that are used within the class several times.
	public static final float FRAME_LENGTH = 0.1f; // How long each animation frame plays
	private static final int OFFSET = 20;
	public static final int EXPLOSION_WIDTH = 269;
	public static final int EXPLOSION_HEIGHT = 269;
	
	private static Animation<TextureRegion> animation = null; // Animation of the explosion
	float x, y; // X and Y coordinates of the explosion
	float stateTime; // What point of the animation the current display is at
	public boolean remove = false; // When to remove the explosion and dispose of the object
	
	
	public BombExplosion (float x, float y) {
		this.x = x - OFFSET;
		this.y = y - OFFSET;
		stateTime = 0;
		
		if (animation == null)
			animation = new Animation<TextureRegion>(FRAME_LENGTH, TextureRegion.split(new Texture("sprites/bomb_explosion.png"), EXPLOSION_WIDTH, EXPLOSION_HEIGHT) [0]);
	}
	
	/**
	 * Update the explosions animation state
	 * @param deltaTime Delta time
	 */
	public void update (float deltaTime) {
		stateTime += deltaTime; // This statement is unneeded if the animation loops
		if (animation.isAnimationFinished(stateTime)) {
			remove = true;
		}
	}
	
	/**
	 * Draw the explosion
	 * @param batch The Sprite Batch of EscapeGame
	 */
	public void render (SpriteBatch batch) {
		batch.draw(animation.getKeyFrame(stateTime), x - BombBomb.BOMB_WIDTH / 2  - 50, y - BombBomb.BOMB_HEIGHT / 2 - 50);
	}
	
}