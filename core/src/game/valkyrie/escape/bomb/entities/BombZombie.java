package game.valkyrie.escape.bomb.entities;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import game.valkyrie.escape.bomb.tools.BombTimer;
import game.valkyrie.escape.tools.CollisionRect;

/**
 * The BombZombie is a class that can throw a bomb back to the BombPlayer in BombGameScreen
 * @author LukeC, Team Valkyrie
 *
 */
public class BombZombie {
	
	// Many static variables that are used within the class several times.
	public static final int ZOMBIE_WIDTH = 64;
	public static final int ZOMBIE_HEIGHT = 52;
	public static final float DEFAULT_X = 880 - ZOMBIE_WIDTH;
	public static final float DEFAULT_Y = Gdx.graphics.getHeight() / 2 - ZOMBIE_HEIGHT / 2;
	public static final float MIN_BOMB_HOLD_TIME = 0.5f; // Minimum hold time for bomb
	public static final float MAX_BOMB_HOLD_TIME = 2.0f; // Maximum hold time for bomb
	
	float holdTimer; // How long the zombie can hold onto the bomb. - Random value
	float throwTimer; // When the zombie will throw the bomb
	float x; // The x coordinate of the zombie
	float y; // The y coordinate of the bomb
	Texture zombieTexture;
	CollisionRect rect; // Zombie collision Rect
	Random random; // Random varaiable generator
	boolean holdingBomb; // If zombie is holding onto bomb
	BombTimer timerDisplay; // Display the amount of time before the zombie will release the bomb
	
	public BombZombie() {
		
		x = DEFAULT_X;
		y = DEFAULT_Y;
		holdTimer = 0;
		holdingBomb = false;
		random = new Random();
		timerDisplay = new BombTimer(DEFAULT_X + 25, DEFAULT_Y - 20, 100, Color.WHITE);  // Create timer 20 units below player sprite
		
		// Load texture
		zombieTexture = new Texture("sprites/bomb_zombie.png");
		rect = new CollisionRect(x, y, ZOMBIE_WIDTH, ZOMBIE_HEIGHT);
		
		
	}
	
	/**
	 * Update the amount of time the zombie can hold the bomb and the timer to represent this
	 * @param delta delta time
	 */
	public void update(float delta) {
		
		// If zombie is holding a bomb, reduce the bomb counter.
		if (holdingBomb()) {
			holdTimer -= delta;
			throwTimer -= delta;
		}
		
		timerDisplay.update(holdTimer);
		rect.move(x, y);
	}
		
	/**
	 * Render the player sprite alongside the hold timer
	 * @param batch The Sprite Batch of EscapeGame
	 */
	public void render (SpriteBatch batch) {
		
		batch.draw(zombieTexture, x, y, ZOMBIE_WIDTH, ZOMBIE_HEIGHT);
		
		// If zombie is holding bomb, show how much time is remaining before it is thrown
		if (holdingBomb() && holdTimer > 0) {
		timerDisplay.render(batch);
		}
				
	}
		
	/**
	 * Return the collision rectangle
	 * @return The collision rectangle of the zombie
	 */
	public CollisionRect getCollisionRect() {
		return rect;
	}
	
	/**
	 * 
	 * @return The x float value of the zombie
	 */
	public float getX() {
		return x;
	}
	
	/**
	 * 
	 * @return The y float value of the zombie
	 */
	public float getY() {
		return y;
	}
	
	/**
	 * 
	 * @return True if the zombie is holding the bomb
	 */
	public boolean holdingBomb() {
		return holdingBomb;
	}
	
	/**
	 * Set the zombie to be holding the bomb and determine a random value for how long they can hold the bomb for, and when they will throw the bomb
	 */
	public void holdBomb() {
		holdingBomb = true;
		holdTimer = random.nextFloat() * (MAX_BOMB_HOLD_TIME - MIN_BOMB_HOLD_TIME) + MIN_BOMB_HOLD_TIME;
		throwTimer = random.nextFloat() * (MAX_BOMB_HOLD_TIME - MIN_BOMB_HOLD_TIME) + MIN_BOMB_HOLD_TIME;
	}
	
	/**
	 * Set the zombie to not be holding the bomb and set the holdTimer to an initial value of 0
	 */
	public void throwBomb() {
		holdTimer = 0;
		holdingBomb = false;
	}
	
	/**
	 * @return How long the zombie can hold the bomb for before it is automatically thrown
	 */
	public float getHoldTimer() {
		return holdTimer;
	}
	
	/**
	 * @return How long till the zombie will throw the bomb
	 */
	public float getThrowTimer() {
		return throwTimer;
	}
	
}

