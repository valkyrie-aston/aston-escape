package game.valkyrie.escape.bomb.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import game.valkyrie.escape.tools.CollisionRect;

/**
 * The BombBomb class is the Bomb object that is passed between opponents in BombGameScreen
 * @author LukeC, Team Valkyrie
 *
 */
public class BombBomb {
	
	// Many static variables that are used within the class several times.
	public static final int BOMB_WIDTH = 64;
	public static final int BOMB_HEIGHT = 64;
	public static final float DEFAULT_X = Gdx.graphics.getWidth() / 2 - BOMB_WIDTH / 2;
	public static final float DEFAULT_Y = Gdx.graphics.getHeight() / 2 - BOMB_HEIGHT / 2;
	public static final float DEFAULT_SPEED = 300;
	
	float x; // X coordinate of the bomb
	float y; // Y coordinate of the bomb
	Texture bombTexture; // Texture of the bomb
	int state; // Indicates if the bomb is being held, being thrown right, or being thrown left using values 0, 1, 2. 0 = Going left, 1 = Going Right, 2 = Being held
	CollisionRect rect; // Bomb collision Rect
	float speed; // Speed of the bomb
	
	public BombBomb (int state) {
		
		x = DEFAULT_X;
		y = DEFAULT_Y;
		speed = DEFAULT_SPEED; // Initial speed is 300;
		this.state = state; // 0 = Left, 1 = Right, 2 = Held
		
		// Load Texture
		bombTexture = new Texture("sprites/bomb_bomb.png");
		
		rect = new CollisionRect(x, y, BOMB_WIDTH, BOMB_HEIGHT);
		
	}
	
	/**
	 * Method used to update the location and collision rectangle of the bomb
	 * @param delta Delta time
	 */
	public void update(float delta) {
		
		if (state == 0) {
			x -= speed * delta;
		}
		
		if (state == 1) {
			x += speed * delta;
		}
		
		// Increase speed as game goes on
		speed += 70 * delta;
		
		rect.move(x, y);
	}
	
	/**
	 * Method to draw the bomb
	 * @param batch The Sprite Batch of EscapeGame
	 */
	public void render (SpriteBatch batch) {
		batch.draw(bombTexture, x, y, BOMB_WIDTH, BOMB_HEIGHT);
	}
	
	/**
	 * Return the collision rectangle
	 * @return The collision rectangle of the bomb
	 */
	public CollisionRect getCollisionRect() {
		return rect;
	}
	
	/**
	 * @return The x float value of the bomb
	 */
	public float getX() {
		return x;
	}
	
	/**
	 * @return The y float value of the bomb
	 */
	public float getY() {
		return y;
	}
	
	/**
	 * Set state of the bomb, 0 = Left, 1 = Right, 2 = Held
	 * @param state The state of the bomb to change to
	 */
	public void setState(int state) {
		this.state = state;
	}
	
	/**
	 * @return The state of the bomb, 0 = Left, 1 = Right, 2 = Held
	 */
	public int getState() {
		return state;
	}
	
	/**
	 * Called when the game ends, sets the bomb to offscreen and makes sure it doesn't move, this is before the game is disposed so it is not in the way
	 */
	public void remove() {
		state = 2;
		x = 1400;
		y = 1400;
	}
	
}
