package game.valkyrie.escape.key.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import game.valkyrie.escape.tools.CollisionRect;

/**
 * KeyFallPlayer is the player's hand which catches the key in KeyFallGameScreen
 * @author LukeC, Team Valkyrie
 *
 */
public class KeyFallPlayer {

	// Many static variables that are used within the class several times.
	public static final int PLAYER_WIDTH = 138;
	public static final int PLAYER_HEIGHT = 70;
	public static final float DEFAULT_X = 600;
	public static final float DEFAULT_Y = 118;
	
	float x; // X coordinate of the player
	float y; // Y coordinate of the player
	boolean openHand; // If hand is open or closed
	Texture playerTexture; // Texture of the hand
	CollisionRect rect; // Player Collision Rect
	
	public KeyFallPlayer() {
		x = DEFAULT_X;
		y = DEFAULT_Y;
		openHand = true;
		
		// Load texture, set rect
		playerTexture = new Texture("sprites/key_player_open.png");
		rect = new CollisionRect(x, y, PLAYER_WIDTH, PLAYER_HEIGHT);
	}
	
	/**
	 * Draws the player sprite
	 * @param batch Sprite batch of EscapeGame
	 */
	public void render(SpriteBatch batch) {
		batch.draw(playerTexture, x, y, PLAYER_WIDTH, PLAYER_HEIGHT);
	}
	
	/**
	 * 
	 * @return Collision rectangle of the player
	 */
	public CollisionRect getCollisionRect() {
		return rect;
	}
	
	/**
	 * 
	 * @return X coordinate of the player
	 */
	public float getX() {
		return x;
	}
	
	/**
	 * 
	 * @return Y coordinate of the player
	 */
	public float getY() {
		return y;
	}
	
	/**
	 * Change the texture to a closed hand and set the openHand to false, meaning a catch attempt has been made
	 */
	public void closeHand() {
		openHand = false;
		playerTexture = new Texture("sprites/key_player_closed.png");
	}
	
	/**
	 * 
	 * @return If the hand is open or not
	 */
	public boolean getOpenHand() {
		return openHand;
	}
	
}
