package game.valkyrie.escape.key.entities;

import java.util.Random;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import game.valkyrie.escape.tools.CollisionRect;

/**
 * KeyFallKey is a key object that falls in KeyFallGameScreen that the player must catch.
 * @author LukeC, Team Valkyrie
 *
 */
public class KeyFallKey {

	// Many static variables that are used within the class several times.
	public static final int KEY_WIDTH = 70;
	public static final int KEY_HEIGHT_SHORT = 120;
	public static final int KEY_HEIGHT_MEDIUM = 150;
	public static final int KEY_HEIGHT_LONG = 170;
	public static final int KEY_STRING_HEIGHT = 360;
	public static final float DEFAULT_X = 610;
	public static final float DEFAULT_Y = 500;
	public static final float DEFAULT_SPEED = 600;
	
	float x; // X coordinate of the key
	float y; // Y coordinate of the key
	float fallSpeed; // Randomly determined fall speed
	int keyHeight; // Height of the key, determines which texture to use
	boolean released; // If the key has been released due to the timer counting down
	Texture keyTexture; // Texture of the key
	Texture stringTexture; // String holding key up, remove when key is released
	Random random; // Random variable generator
	CollisionRect rect; // Key Collision Rectangle 
	
	public KeyFallKey(int keySize) {
		x = DEFAULT_X;
		y = DEFAULT_Y;
		
		// Set texture, collision rect
		switch (keySize) {
			case 1:
				keyTexture = new Texture("sprites/key_key_short.png");
				rect = new CollisionRect(x, y, KEY_WIDTH, KEY_HEIGHT_SHORT);
				keyHeight = KEY_HEIGHT_SHORT;
			break;
			case 2:
				keyTexture = new Texture("sprites/key_key_medium.png");
				rect = new CollisionRect(x, y, KEY_WIDTH, KEY_HEIGHT_MEDIUM);
				keyHeight = KEY_HEIGHT_MEDIUM;
			break;
			case 3:
				keyTexture = new Texture("sprites/key_key_long.png");
				rect = new CollisionRect(x, y, KEY_WIDTH, KEY_HEIGHT_LONG);
				keyHeight = KEY_HEIGHT_LONG;
			break;
		}
		
		// Determine fall speed
		random = new Random();
		float min = 600.0f;
		float max = 1400.0f;
		fallSpeed = min + random.nextFloat() * (max - min);
		
		stringTexture = new Texture("sprites/key_key_string.png");
		released = false;
	}
	
	/**
	 * Update the location of the key as it falls
	 * @param delta Delta time
	 */
	public void update(float delta) {
		y -= fallSpeed * delta;
		rect.move(x, y);
	}
	
	/**
	 * Render the key object, alongside a string if the key hasn't fallen
	 * @param batch
	 */
	public void render(SpriteBatch batch) {
		// Draw string if not released
		if (released == false) {
			batch.draw(stringTexture, x + 5, y + 20, KEY_WIDTH, KEY_STRING_HEIGHT);
		}
		
		batch.draw(keyTexture, x, y, KEY_WIDTH, keyHeight);
	}
	
	/**
	 * 
	 * @return The collision rectangle for the key
	 */
	public CollisionRect getCollisionRect() {
		return rect;
	}
	
	/**
	 * 
	 * @return The x coordinate for the key
	 */
	public float getX() {
		return x;
	}
	
	/**
	 * 
	 * @return The y coordinate for the key
	 */
	public float getY() {
		return y;
	}
	
	/**
	 * Set released as true, which causes it to fall
	 */
	public void dropKey() {
		released = true;
	}
	
	/**
	 * 
	 * @return If the key is released or not
	 */
	public boolean isDropped() {
		return released;
	}
	
}
