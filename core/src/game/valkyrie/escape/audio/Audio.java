package game.valkyrie.escape.audio;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

/**
 * The audio class holds the available soundbites and audio files that can be played throughout the game.
 * @author ArjunS, Team Valkyrie
 *
 */
public class Audio {

	private static HashMap<String, Sound> sounds; // Holds the available audio files

	static {
		sounds = new HashMap<String, Sound>();
	}

	/**
	 * Loads the audio file so it can be used by the game when loop/stop/play is called
	 * @param path The path to the audio files
	 * @param name The key of the sound/song
	 */
	public static void load(String path, String name) {
		Sound sound = Gdx.audio.newSound(Gdx.files.internal(path));
		sounds.put(name, sound);
	}

	/**
	 * Once an audio file is played using the 'play' method, this method allows it to loop
	 * @param name The key of the sound/song
	 */
	public static void loop(String name) {
		sounds.get(name).loop();
	}
	
	/**
	 * Stops an audio file that is currently playing
	 * @param name The key of the sound/song
	 */
	public static void stop(String name) {
		sounds.get(name).stop();
	}
	
	/**
	 * Plays an audio file
	 * @param name They key of t he sound/song
	 */
	public static void play(String name) {
		sounds.get(name).play();
	}
	
	/**
	 * Stops all audio files that are currently playing
	 */
	public static void stopAll() {
		for (Sound s : sounds.values()){
			s.stop();
		}
	}
}
