package game.valkyrie.escape.dodge.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import game.valkyrie.escape.tools.CollisionRect;

/**
 * DodgePillar is a pillar obstacle that the player must duck under in DodgeGameScreen
 * @author LukeC, Team Valkyrie
 *
 */
public class DodgePillar {
	
	// Many static variables that are used within the class several times.
	public static final int PILLAR_WIDTH = 150;
	public static final int PILLAR_HEIGHT = 700;
	public static final float DEFAULT_Y = 330;
	public static final int DEFAULT_SPEED = 850;
	
	float x; // X coordinate of the pillar
	float y; // Y coordinate of the pillar
	int speed; // Speed of the pillar
	CollisionRect rect; // Collision Rectangle of the pillar
	Texture pillar; // Texture of the pillar
	
	public DodgePillar(float x) {
		this.x = x;
		y = DEFAULT_Y;
		speed = DEFAULT_SPEED;
		pillar = new Texture("sprites/dodge_pillar.png");
		rect = new CollisionRect(x, y, PILLAR_WIDTH, PILLAR_HEIGHT);
	}
	
	/**
	 * Update the location of the pillar and its collision rectangle
	 * @param delta Delta time
	 */
	public void update(float delta) {
		x -= speed * delta;
		rect.move(x, y);
	}
	
	/**
	 * Render the pillar
	 * @param batch The sprite batch of EscapeGame
	 */
	public void render(SpriteBatch batch) {
		batch.draw(pillar, x, y, PILLAR_WIDTH, PILLAR_HEIGHT);
	}
	
	/**
	 * 
	 * @return Collision Rectangle of Pillar
	 */
	public CollisionRect getCollisionRect() {
		return rect;
	}
	
}
