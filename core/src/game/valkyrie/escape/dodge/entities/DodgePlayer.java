package game.valkyrie.escape.dodge.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import game.valkyrie.escape.audio.Audio;
import game.valkyrie.escape.tools.CollisionRect;

/**
 * DodgePlayer is the player character for DodgeGameScreen. They can jump and duck under obstacles.
 * @author LukeC, Team Valkyrie
 *
 */
public class DodgePlayer {
	
	// Many static variables that are used within the class several times.
	public static final int PLAYER_WIDTH = 184;
	public static final int PLAYER_HEIGHT = 177;
	public static final float DEFAULT_X = 284;
	public static final float DEFAULT_Y = 170;
	public static final float JUMP_SPEED = 1200;
	
	float x; // X coordinate of the player
	float y; // Y coordinate of the player
	Texture playerTexture; // Texture of the player, which changes depending on the action being performed
	Texture playerTextureStanding; // Texture of the player whilst they are standing
	Texture playerTextureActing; // Texture of the player when they are jumping or ducking
	CollisionRect rect; // Player collision rect
	boolean isJumping; // if the player is jumping
	boolean ascending; // If player is jumping upwards or falling post jump
	float speed; // Speed of the player's jump
	boolean isCrouching; // If the player is standing
	float crouchTime; // time player crouches when pressed
	
	public DodgePlayer() {
		x = DEFAULT_X;
		y = DEFAULT_Y;
		crouchTime = 0.6f;
		playerTextureStanding = new Texture("sprites/dodge_player_standing.png");
		playerTextureActing = new Texture("sprites/dodge_player_action.png");
		playerTexture = playerTextureStanding;
		isJumping = false;
		ascending = false;
		speed = JUMP_SPEED;
		isCrouching = false;
		rect = new CollisionRect(x, y, PLAYER_WIDTH, PLAYER_HEIGHT);
	}
	
	/**
	 * Update the players position, and their state, whether they are jumping or crouching. Also updates the player's collision rectangle
	 * @param delta Delta time
	 */
	public void update(float delta) {
		// Update crouch timer if crouching
		if (isCrouching == true) {
			crouchTime -= delta;
		}
		
		// Reset position if crouch timer goes below 0
		if (crouchTime < 0.0f) {
			crouchTime = 0.6f;
			playerTexture = playerTextureStanding;
			isCrouching = false;
		}
		
		// If down key is presed, crouch if not already crouching or jumping
		if (Gdx.input.isKeyJustPressed(Keys.DOWN)) {
			if (isJumping == false && isCrouching == false) {
				playerTexture = playerTextureActing;
				Audio.play("item collect");
				isCrouching = true;
			}
		}
		
		// If up key is pressed, and not crouching, jump
		if (Gdx.input.isKeyJustPressed(Keys.UP)) {
			if (isJumping == false && isCrouching == false) {
				playerTexture = playerTextureActing;
				Audio.play("item collect");
				isJumping = true;
				ascending = true;
			}
		}
				
		if (isJumping == true && ascending == true) {
			y += speed * delta;
			
			speed -= 40;
			if (speed <= 0) {
				ascending = false;
			}
		}
		
		if (isJumping == true && ascending == false) {
			y -= speed * delta;
			speed += 40;
			if (y <= DEFAULT_Y) {
				y = DEFAULT_Y;
				speed = JUMP_SPEED;
				playerTexture = playerTextureStanding;
				isJumping = false;
			}
		}
		
		rect.move(x, y);
	}
	
	/**
	 * Renders the player sprite, texture determined by action
	 * @param batch Sprite batch of EscapeGame
	 */
	public void render(SpriteBatch batch) {
		batch.draw(playerTexture, x, y, PLAYER_WIDTH, PLAYER_HEIGHT);
	}
	
	/**
	 * 
	 * @return The collision rectangle of the player
	 */
	public CollisionRect getCollisionRect() {
		return rect;
	}
	
	/**
	 * 
	 * @return X coordinate of the player
	 */
	public float getX() {
		return x;
	}
	
	/**
	 * 
	 * @return Y coordinate of the player
	 */
	public float getY() {
		return y;
	}
	
	/**
	 * 
	 * @return If the player is currently crouching, true is returned
	 */
	public boolean getCrouching() {
		return isCrouching;
	}
}
