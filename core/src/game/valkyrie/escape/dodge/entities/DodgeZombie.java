package game.valkyrie.escape.dodge.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import game.valkyrie.escape.tools.CollisionRect;

/**
 * DodgeZombie is an obstacle the player must jump over in DodgeGameScreen
 * @author LukeC, Team Valkyrie
 *
 */
public class DodgeZombie {
	
	// Many static variables that are used within the class several times.
	public static final int ZOMBIE_WIDTH = 123;
	public static final int ZOMBIE_HEIGHT = 214;
	public static final float DEFAULT_Y = 145;
	public static final int DEFAULT_SPEED = 850;
	
	float x; // X coordinate of the zombie
	float y; // Y coordinate of the zombie
	int speed; // Speed of the zombie
	CollisionRect rect; // Collision Rectangle of the zombie
	Texture zombie; // Texture of the zombie
	
	public DodgeZombie(float x) {
		this.x = x;
		y = DEFAULT_Y;
		speed = DEFAULT_SPEED;
		zombie = new Texture("sprites/dodge_zombie.png");
		rect = new CollisionRect(x, y, ZOMBIE_WIDTH, ZOMBIE_HEIGHT);
	}
	
	/**
	 * Update the collision of the zombie as well as its collision rectangle
	 * @param delta Delta time
	 */
	public void update(float delta) {
		x -= speed * delta;
		rect.move(x, y);
	}
	
	/**
	 * Render the zombie
	 * @param batch Sprite batch of EscapeGame
	 */
	public void render(SpriteBatch batch) {
		batch.draw(zombie, x, y, ZOMBIE_WIDTH, ZOMBIE_HEIGHT);
	}
	
	/**
	 * 
	 * @return Collision rectangle of the zombie
	 */
	public CollisionRect getCollisionRect() {
		return rect;
	}
	
}
