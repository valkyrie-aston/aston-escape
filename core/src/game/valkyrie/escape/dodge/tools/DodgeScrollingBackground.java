package game.valkyrie.escape.dodge.tools;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import game.valkyrie.escape.tools.CollisionRect;

/**
 * This class represent the scrolling background in DodgeGameScreen. it uses two images that lie side by side
 * @author LukeC, Team Valkyrie
 *
 */
public class DodgeScrollingBackground {
	
	// Many static variables that are used within the class several times.
	public static final int DEFAULT_SPEED = 850;
	public static final int DEFAULT_Y = 17;
	
	Texture dodgeFloor; // Texture of the background
	float x1; // First image
	float x2; // Second image
	int speed; // Speed of the background
	
	public DodgeScrollingBackground() {
		dodgeFloor = new Texture("sprites/dodge_floor.png");
		x1 = 192;
		x2 = 192 + dodgeFloor.getWidth();
		speed = DEFAULT_SPEED;
	}
	
	/**
	 * Update the x coordinates of both images, if either reach the edge of the screen, correct position
	 * @param delta Delta time
	 */
	public void update(float delta) {
		x1 -= speed * delta;
		x2 -= speed * delta;
			
		// If image goes off screen, set to correct position
		if (x1 + dodgeFloor.getWidth() <= 192) {
			x1 = x2 + dodgeFloor.getWidth();
		}
		
		if (x2 + dodgeFloor.getWidth() <= 192) {
			x2 = x1 + dodgeFloor.getWidth();
		}
	}
	
	/**
	 * Render both textures
	 * @param batch Sprite batch of EscapeGame
	 */
	public void render(SpriteBatch batch) {
		batch.draw(dodgeFloor, x1, DEFAULT_Y);
		batch.draw(dodgeFloor, x2, DEFAULT_Y);
	}
	
}
