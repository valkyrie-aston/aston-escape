/**
 * @author Pritesh
 */
package game.valkyrie.escape.breakout.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import game.valkyrie.escape.EscapeGame;
import game.valkyrie.escape.tools.CollisionRect;

/**
 * BreakoutPaddle is the player in BreakoutGameScreen, it moves left and right and hits the ball in the opposite direction
 * @author PriteshP, LukeC, Team Valkyrie
 *
 */
public class BreakoutPaddle {

	// Many static variables that are used within the class several times.
	public static int SPEED = 270; // The speed at which the paddle moves
	public static final int PADDLE_WIDTH = 100; // The Width of the Paddle
	public static final int PADDLE_HEIGHT = 40; // The Height of the Paddle
	public static final float DEFAULT_X = 400; //Position of the paddle
	public static final float DEFAULT_Y = 110; //Position of the paddle
	
	Texture paddle; // Texture of the paddle
	float x; // X coordinate of the paddle
	float y; // Y coordinate of the paddle
	int paddleSpeed = SPEED; // Speed oft he paddle
	CollisionRect rect; // Paddle collision rect
	
	public BreakoutPaddle() {
		x = EscapeGame.SCREEN_WIDTH / 2 - PADDLE_WIDTH / 2; 
		y = 30; 		
		rect = new CollisionRect(x, y, PADDLE_WIDTH, PADDLE_HEIGHT);
		paddle = new Texture("sprites/breakout_paddle.png");
	}
	
	/**
	 * Update the location and collision rectangle of the paddle
	 * @param x The new value for the x coordinate
	 */
	public void update(float x) {
		this.x = x;
		rect.move(x, y);
	}
	
	/**
	 * Render the paddle
	 * @param batch Sprite batch for EscapeGame
	 */
	public void render (SpriteBatch batch) {
		batch.draw(paddle, x, y, PADDLE_WIDTH, PADDLE_HEIGHT);
	}
	
	/**
	 * 
	 * @return Collision rectangle for paddle
	 */
	public CollisionRect getCollisionRect() {
		return rect;
	}
	
	/**
	 * 
	 * @return X coordinate of paddle
	 */
	public float getX() {
		return x;
	}
	
	/**
	 * 
	 * @return Y coordinate of paddle
	 */
	public float getY() {
		return y;
	}
	
	/**
	 * 
	 * @return Speed of the paddle
	 */
	public int getSpeed() {
		return paddleSpeed;
	}
	
	/**
	 * 
	 * @param speed Increase the speed of the paddle by a value given
	 */
	public void increaseSpeed(int speed) {
		paddleSpeed += speed;
	}
}
