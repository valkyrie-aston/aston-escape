/**
 * @author Pritesh
 */
package game.valkyrie.escape.breakout.entities;

import java.util.Random;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;


import game.valkyrie.escape.tools.CollisionRect;

/**
 * BreakoutBall is the ball in BreakoutGameScreen that collides with the paddle and bricks
 * @author PriteshP, LukeC, Team Valkyrie
 *
 */
public class BreakoutBall {

	// Many static variables that are used within the class several times.
	public static  int SPEED = 250; // The speed at which the ball moves
	public static final int BALL_WIDTH = 20; // The Width of the Ball
	public static final int BALL_HEIGHT = 20; // The Height of the Ball
	public static final float DEFAULT_X = 640; //position of the ball on the x axis
	public static final float DEFAULT_Y = 131; //position of the ball on the y axis
	
	Texture ball; // Texture of the ball
	Random random; // Random variable generator
	float x; // X coordinate of the ball
	float y; // Y coordinat of the ball
	public static int ballXdir;	//Ball moving in the x direction
	public static int ballYdir;	//Ball moving in the y direction
	CollisionRect rect; // Ball collision rect
	int ballSpeed = SPEED; // Speed of ball
	
	public BreakoutBall() {
		
		// Random X Location
		int low = 310;
		int high = 970;
		random = new Random();
		int result = random.nextInt(high-low) + low;
		float resultFloat = (float) result;
		x = result;
		y = DEFAULT_Y;
		ballXdir = 1;	//Direction which the ball goes on the x axis
		ballYdir = 1;	//Direction which the ball goes on the y axis
		rect = new CollisionRect(x, y, BALL_WIDTH, BALL_HEIGHT);
		ball = new Texture("sprites/breakout_ball.png");
		
	}
	
	/**
	 * Updates the location of the ball depending on the direction, also changes the direction of ball when it collides with the edge of the screen
	 * @param delta
	 */
	public void update(float delta) {
		
		if (ballXdir == 0) {
			x -= ballSpeed * delta;
		} else if (ballXdir == 1) {
			x += ballSpeed * delta;
		}
		
		if (ballYdir == 0) {
			y -= ballSpeed * delta;
		} else if (ballYdir == 1) {
			y += ballSpeed * delta;
		}
		
		//Ball colliding with the sides of the screen
		if (x < game.valkyrie.escape.screens.BreakoutGameScreen.LEFT_BOUNDARY) {
			changeXdir();
		} else if (x + BreakoutBall.BALL_WIDTH > game.valkyrie.escape.screens.BreakoutGameScreen.RIGHT_BOUNDARY) {
			changeXdir();
		} else if(y + BALL_HEIGHT > game.valkyrie.escape.screens.BreakoutGameScreen.TOP_BOUNDARY) {
			changeYdir();
		}
		
		rect.move(x, y);
		
	}
	
	
	/**
	 * Render the ball
	 * @param batch Sprite Batch of EscapeGame
	 */
	public void render (SpriteBatch batch) {
		batch.draw(ball, x, y, BALL_WIDTH, BALL_HEIGHT);
	}
	
	/**
	 * 
	 * @return Collision rectangle for the ball
	 */
	public CollisionRect getCollisionRect() {
		return rect;
	}
	
	/**
	 * 
	 * @return X coordinate of the ball
	 */
	public float getX() {
		return x;
	}
	
	/**
	 * 
	 * @return Y coordinate of the ball
	 */
	public float getY() {
		return y;
	}
	
	/**
	 * 
	 * @return The x direction of the ball, 0 for left, 1 for right
	 */
	public int getXdir() {
		return ballXdir;
	}
	
	/**
	 * Changes the direction of the ball, left to right or right to left
	 */
	public void changeXdir() {	
		if (ballXdir == 0) {
			ballXdir = 1;
		} else if (ballXdir == 1) {
			ballXdir = 0;
		}
	}
	
	/**
	 * 
	 * @return The y direction of the ball, 0 for down, 1 for up
	 */
	public int getYdir() {
		return ballYdir;
	}
	
	/**
	 * Changes the direction of the ball, up to down or down to up
	 */
	public void changeYdir() { 
		if (ballYdir == 0) {
			ballYdir = 1;
		} else if (ballYdir == 1) {
			ballYdir = 0;
		}
	}
	
	/**
	 * 
	 * @return The speed of the ball
	 */
	public int getSpeed() {
		return ballSpeed;
	}
	
	/**
	 * Increases the speed of the ball by a given amount
	 * @param speed Amount to increase the ball speed by
	 */
	public void increaseSpeed(int speed) {
		ballSpeed += speed;
	}
}
