/**
 * @author Pritesh
 */
package game.valkyrie.escape.breakout.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import game.valkyrie.escape.tools.CollisionRect;

/**
 * BreakoutBrick used in BreakoutGameScreen that can be destroyed by the ball
 * @author PriteshP, LukeC, Team Valkyrie
 *
 */
public class BreakoutBrick {

	// Many static variables that are used within the class several times.
	public static final int BRICK_WIDTH = 128; // The Width of the Brick
	public static final int BRICK_HEIGHT = 40; // The Height of the Brick
	
	Texture brick; // Texture of the brick
	float x; // X coordinate of the brick
	float y; // Y coordinate of the brick
	CollisionRect rect; // Brick collision rect
	CollisionRect rightSide; //Brick collision right side of brick
	CollisionRect leftSide; //Brick collision left side of brick
	
	public BreakoutBrick(float x, float y) {
		this.x = x;
		this.y = y;
		rect = new CollisionRect(x, y, BRICK_WIDTH, BRICK_HEIGHT);
		brick = new Texture("sprites/breakout_brick.png");
		rightSide = new CollisionRect(x + BRICK_WIDTH, y, 1, BRICK_HEIGHT);	//Collision detection for the right side of the block
		leftSide = new CollisionRect(x -1,y,1,BRICK_HEIGHT); //Collision detection for the left side of the block
	}
	
	/**
	 * Rendering the brick
	 * @param batch Sprite batch of EscapeGame
	 */
	public void render (SpriteBatch batch) {
		batch.draw(brick, x, y, BRICK_WIDTH, BRICK_HEIGHT);
	}
	
	/**
	 * 
	 * @return Collision Rectangle of the brick
	 */
	public CollisionRect getCollisionRect() {
		return rect;
	}
	
	/**
	 * 
	 * @return Collision rectangle of the right edge of the brick
	 */
	public CollisionRect getRightSide() {
		return rightSide;
	}
	
	/**
	 * 
	 * @return Collision rectangle of the left edge of the brick
	 */
	public CollisionRect getLeftSide() {
		return leftSide;
	}
	
	/**
	 * 
	 * @return X coordinate of brick
	 */
	public float getX() {
		return x;
	}
	
	/**
	 * 
	 * @return Y coordinate of brick
	 */
	public float getY() {
		return y;
	}

	
}
