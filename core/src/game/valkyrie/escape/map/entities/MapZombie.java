package game.valkyrie.escape.map.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;

import game.valkyrie.escape.tools.CollisionRect;

/**
 * MapZombie is a class that moves around the MapLevelScreen and changes screen to minigames when players encounter them
 * @author LukeC, Team Valkyrie
 *
 */
public class MapZombie {

	public static final int ZOMBIE_DIMENSION = 64;
	
	Texture zombieTexture;	// Zombie Texture
	float x; // X coordinate of zombie
	float y; // Y coordinate of zombie
	private int speed; // Spped of zombie
	
	private TiledMapTileLayer collisionLayer;// Collision layer of the map on the MapGameScreen, determines collision with solid objects		
	CollisionRect rect; // for checking collisions
	private ZombieDirection direction; // Direction EnumClass the zombie travels in
	
	public MapZombie(float x, float y, MapLayer mapLayer, ZombieDirection direction, int speed) {
		
		this.x = x;
		this.y = y;
		this.speed = speed;
		this.direction = direction;
		
		zombieTexture = new Texture("sprites/map_zombie.png");
		rect = new CollisionRect(x, y, ZOMBIE_DIMENSION, ZOMBIE_DIMENSION);
		this.collisionLayer = (TiledMapTileLayer) mapLayer;
		
	}
	
	/**
	 * Update the zombies position and collision rect, reversing directions if it collides with a solid wall from the collision layer
	 * @param deltaTime Detla time
	 */
	public void update (float deltaTime) {
		
		boolean collision = false;
		
		switch (direction) {
		
		case UP:
			
			collision = collisionLayer.getCell(getXint() / ZOMBIE_DIMENSION, (getYint() + ZOMBIE_DIMENSION) / ZOMBIE_DIMENSION).getTile().getProperties().containsKey("blocked");
			if (collision) {
				direction = ZombieDirection.DOWN;
			} else {
				y += speed * deltaTime;
			}
			break;
			
		case DOWN:
			
			collision = collisionLayer.getCell(getXint() / ZOMBIE_DIMENSION, getYint() / ZOMBIE_DIMENSION).getTile().getProperties().containsKey("blocked");
			if (collision) {
				direction = ZombieDirection.UP;
			} else {
				y -= speed * deltaTime;
			}
			break;
				
		case RIGHT:
			
			collision = collisionLayer.getCell((getXint() + ZOMBIE_DIMENSION) / ZOMBIE_DIMENSION, getYint() / ZOMBIE_DIMENSION).getTile().getProperties().containsKey("blocked");
			if (collision) {
				direction = ZombieDirection.LEFT;
			} else {
				x += speed * deltaTime;
			}
			break;
				
		case LEFT:
			
			collision = collisionLayer.getCell(getXint() / ZOMBIE_DIMENSION, getYint() / ZOMBIE_DIMENSION).getTile().getProperties().containsKey("blocked");
			if (collision) {
				direction = ZombieDirection.RIGHT;
			} else {
				x -= speed * deltaTime;
			}
			break;
		
		case NONE:
			;;
			break;
				
		}
		
		rect.move(x, y);
		
	}
	
	/**
	 * Render the zombie
	 * @param batch SpriteBatch of EscapeGame
	 */
	public void render (SpriteBatch batch) {
		
		batch.draw(zombieTexture, x, y, ZOMBIE_DIMENSION, ZOMBIE_DIMENSION);
		
	}
	
	/**
	 * 
	 * @return Collision rectangle for zombie
	 */
	public CollisionRect getCollisionRect() {
		return rect;
	}
	
	/**
	 * 
	 * @return X coordinate of zombie
	 */
	public float getX() {
		
		return x;
		
	}
	
	/**
	 * 
	 * @return Y coordinate of zombie
	 */
	public float getY() {
		
		return y;
	}
	
	/**
	 * 
	 * @return X coordinate as an integer rather than a float
	 */
	public int getXint() {
		
		return (int) x;
		
	}
	
	/**
	 * 
	 * @return Y coordinate as an integer rather than a float
	 */
	public int getYint () {
		
		return (int) y;
		
	}
	
	// Movement Directions
	public enum ZombieDirection {
		
		UP, DOWN, LEFT, RIGHT, NONE
		
	}
	
}
