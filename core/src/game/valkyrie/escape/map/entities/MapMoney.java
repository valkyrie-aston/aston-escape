package game.valkyrie.escape.map.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;

import game.valkyrie.escape.tools.CollisionRect;

/**
 * MapMoney is an item the player can pickup on the MapLevelScreen to increase their score
 * @author LukeC, Team Valkyrie
 *
 */
public class MapMoney {

	public static final int MONEY_DIMENSION = 64;
	
	Texture moneyTexture; // Texture for the money
	float x; // X coordinate of the money
	float y; // Y coordinate of the money 
	
	private TiledMapTileLayer collisionLayer; // Collision layer of the map on the MapGameScreen, determines collision with solid objects		
	CollisionRect rect; // for checking collisions
	
	public MapMoney(float x, float y, MapLayer mapLayer) {
		
		this.x = x; // Coordinates on the map
		this.y = y;
		
		moneyTexture = new Texture("sprites/map_money.png");
		rect = new CollisionRect(x, y, MONEY_DIMENSION, MONEY_DIMENSION);
		this.collisionLayer = (TiledMapTileLayer) mapLayer;
		
	}
	
	/**
	 * Render the money
	 * @param batch Sprite Batch of EscapeGame
	 */
	public void render (SpriteBatch batch) {
		
		batch.draw(moneyTexture, x, y, MONEY_DIMENSION, MONEY_DIMENSION);
		
	}
	
	/**
	 * 
	 * @return The collision rectangle of the money
	 */
	public CollisionRect getCollisionRect() {
		return rect;
	}
		
	
}

