package game.valkyrie.escape.map.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.Vector2;

import game.valkyrie.escape.tools.CollisionRect;

public class MapPlayer {
	
	public static final int SPEED = 250;
	public static final int PLAYER_DIMENSION = 64;
	
	// Player Texture
	Texture playerTexture;
			
	float x;
	float y;
	
	private TiledMapTileLayer collisionLayer; // Collision layer of the map on the MapGameScreen, determines collision with solid objects		
	CollisionRect rect; // for checking collisions
	
	// Pass starting coordinate
	public MapPlayer(float x, float y, MapLayer mapLayer) {
		
		this.x = x;
		this.y = y;
		
		playerTexture = new Texture("sprites/map_player.png");
		rect = new CollisionRect(x, y, PLAYER_DIMENSION, PLAYER_DIMENSION);
		this.collisionLayer = (TiledMapTileLayer) mapLayer;
		
	}
	
	// Move character based on direction
	public void update(float deltaTime, PlayerDirection direction) {
		
		boolean collisionX = false;
		boolean collisionY = false;
		
		switch (direction) {
		
		// For each direction, check for collision with tiles, if colliding, do not move. Checks three times from top to bottom of player sprite as player does not move in grid coordinates, has smooth movement
		
		// CURRENT ISSUE - It feels awful, when you collide with a wall you get stuck until you press the opposite direction of the wall
		
		case RIGHT:
			
			// Top RIGHT
			collisionX = collisionLayer.getCell((getXint() + PLAYER_DIMENSION) / PLAYER_DIMENSION, ((getYint() + PLAYER_DIMENSION) / PLAYER_DIMENSION)).getTile().getProperties().containsKey("blocked");
			
			// Middle RIGHT
			if (!collisionX) {
			collisionX = collisionLayer.getCell((getXint() + PLAYER_DIMENSION) / PLAYER_DIMENSION, ((getYint() + (PLAYER_DIMENSION / 2)) / PLAYER_DIMENSION)).getTile().getProperties().containsKey("blocked");
			}
			
			// Bottom RIGHT
			if (!collisionX) {
			collisionX = collisionLayer.getCell((getXint() + PLAYER_DIMENSION) / PLAYER_DIMENSION, (getYint() / PLAYER_DIMENSION)).getTile().getProperties().containsKey("blocked");
			}
			
			if (!collisionX) {
				x += SPEED * deltaTime;
			} else {
				;;
			}

			break;	
			
		
		case LEFT:
			
			// Top Left
			collisionX = collisionLayer.getCell(getXint() / PLAYER_DIMENSION, ((getYint() + PLAYER_DIMENSION) / PLAYER_DIMENSION)).getTile().getProperties().containsKey("blocked");
			
			// Middle Left
			if (!collisionX) {
			collisionX = collisionLayer.getCell(getXint() / PLAYER_DIMENSION, ((getYint() + (PLAYER_DIMENSION / 2)) / PLAYER_DIMENSION)).getTile().getProperties().containsKey("blocked");
			}
			
			// Bottom Left
			if (!collisionX) {
			collisionX = collisionLayer.getCell(getXint() / PLAYER_DIMENSION, (getYint() / PLAYER_DIMENSION)).getTile().getProperties().containsKey("blocked");
			}
			
			if (!collisionX) {
				x -= SPEED * deltaTime;
			} else {
				;;
			}

			break;	
			
		case UP:
			
			// TOP LEFT
			collisionY = collisionLayer.getCell(getXint() / PLAYER_DIMENSION, ((getYint() + PLAYER_DIMENSION) / PLAYER_DIMENSION)).getTile().getProperties().containsKey("blocked");
			
			// TOP MIDDLE
			if (!collisionY) {
			collisionY = collisionLayer.getCell((getXint() + (PLAYER_DIMENSION / 2)) / PLAYER_DIMENSION, ((getYint() + PLAYER_DIMENSION) / PLAYER_DIMENSION)).getTile().getProperties().containsKey("blocked");	
			}
			
			// TOP RIGHT
			if (!collisionY) {
			collisionY = collisionLayer.getCell((getXint() + PLAYER_DIMENSION) / PLAYER_DIMENSION, ((getYint() + PLAYER_DIMENSION) / PLAYER_DIMENSION)).getTile().getProperties().containsKey("blocked");	
			}

			if (!collisionY) {
				y += SPEED * deltaTime;
			} else {
				;;
			}
			
			break;
			
			case DOWN:
			
			// BOTTOM LEFT
			collisionY = collisionLayer.getCell(getXint() / PLAYER_DIMENSION, (getYint() / PLAYER_DIMENSION)).getTile().getProperties().containsKey("blocked");
			
			// BOTTOM MIDDLE
			if (!collisionY) {
			collisionY = collisionLayer.getCell((getXint() + (PLAYER_DIMENSION / 2)) / PLAYER_DIMENSION, (getYint()/ PLAYER_DIMENSION)).getTile().getProperties().containsKey("blocked");	
			}
			
			// BOTTOM RIGHT
			if (!collisionY) {
			collisionY = collisionLayer.getCell((getXint() + PLAYER_DIMENSION) / PLAYER_DIMENSION, (getYint()/ PLAYER_DIMENSION)).getTile().getProperties().containsKey("blocked");	
			}

			if (!collisionY) {
				y -= SPEED * deltaTime;
			} else {
				;;
			}
			
			break;
			
		}
	
		rect.move(x, y);
		
	}
	
	public void render (SpriteBatch batch) {
		
		batch.draw(playerTexture, x, y, PLAYER_DIMENSION, PLAYER_DIMENSION);
		
	}
	
	public void setX(float x) {
		
		this.x = x;
		
	}
	
	public void setY(float y) {
		
		this.y = y;
		
	}
	
	public float getX() {
		
		return x;
		
	}
	
	public float getY() {
		
		return y;
	}
	
	public int getXint() {
		
		return (int) x;
		
	}
	
	public int getYint () {
		
		return (int) y;
		
	}
	
	public CollisionRect getCollisionRect() {
		return rect;
	}
	
	// Movement Directions
	public enum PlayerDirection {
		
		UP, DOWN, LEFT, RIGHT
		
	}
	
}

