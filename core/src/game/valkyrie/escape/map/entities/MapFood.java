package game.valkyrie.escape.map.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;

import game.valkyrie.escape.tools.CollisionRect;

/**
 * MapFood is an item the player can pick up on the MapGameScreen to regain lives.
 * @author LukeC, Team Valkyrie
 *
 */
public class MapFood {

	public static final int FOOD_DIMENSION = 64;
	
	Texture foodTexture; // Texture of the food
	float x; // X coordinate of the food
	float y; // Y coordinate of the food
	
	private TiledMapTileLayer collisionLayer; // Collision layer of the map on the MapGameScreen, determines collision with solid objects	
	CollisionRect rect; // for checking collisions
	
	public MapFood(float x, float y, MapLayer mapLayer) {
		
		this.x = x; // Set the coordinates for the food on the map
		this.y = y;
		
		foodTexture = new Texture("sprites/map_food.png");
		rect = new CollisionRect(x, y, FOOD_DIMENSION, FOOD_DIMENSION);
		this.collisionLayer = (TiledMapTileLayer) mapLayer;
		
	}
	
	/**
	 * Render the food
	 * @param batch SpriteBatch of EscapeGame
	 */
	public void render (SpriteBatch batch) {
		
		batch.draw(foodTexture, x, y, FOOD_DIMENSION, FOOD_DIMENSION);
		
	}
	
	/**
	 * 
	 * @return Collision Rectangle of the food
	 */
	public CollisionRect getCollisionRect() {
		return rect;
	}
		
	
}
