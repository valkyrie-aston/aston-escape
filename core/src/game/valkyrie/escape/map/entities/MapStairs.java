package game.valkyrie.escape.map.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.utils.Align;

import game.valkyrie.escape.tools.CollisionRect;

/**
 * MapStairs is an object on the map that is used to proceed to the next floor or win the game
 * @author LukeC, Team Valkyrie
 *
 */
public class MapStairs {

	public static final int STAIRS_DIMENSION = 64;
	public static final int STAIRS_IMAGE_DIMENSION = 194;
	
	Texture stairsTexture; // Stairs texture
	float x; // X coordinate of the stairs
	float y; // Y coordinate of the stairs
	
	private TiledMapTileLayer collisionLayer; // Collision layer of the map on the MapGameScreen, determines collision with solid objects			
	CollisionRect rect; // for checking collisions
	private BitmapFont uiFont; // Font for text
	
	// Pass starting coordinate
	public MapStairs(float x, float y, MapLayer mapLayer) {
		
		this.x = x;
		this.y = y;
		
		stairsTexture = new Texture("sprites/map_stairs.png");
		rect = new CollisionRect(x, y, STAIRS_IMAGE_DIMENSION, STAIRS_IMAGE_DIMENSION);
		this.collisionLayer = (TiledMapTileLayer) mapLayer;
		uiFont = new BitmapFont(Gdx.files.internal("fonts/lunchtime.fnt"));
		uiFont.getData().setScale(0.7f);
		
	}
	
	/**
	 * Render the stairs
	 * @param batch SpriteBatch for EscapeGame
	 */
	public void render (SpriteBatch batch) {
		
		batch.draw(stairsTexture, x, y, STAIRS_IMAGE_DIMENSION + 30, STAIRS_IMAGE_DIMENSION);
		
	}
	
	/** This method will Render text above the stairs if the player does not have enough zombies killed
	 * 
	 * @param batch SpriteBatch for EscapeGame
	 */
	public void renderWarningText (SpriteBatch batch) {
		
		GlyphLayout warningMessageHeaderLayout = new GlyphLayout(uiFont, "YOU NEED TO KILL MORE ZOMBIES.", Color.YELLOW, 1f, Align.center, true);
		uiFont.draw(batch, warningMessageHeaderLayout, x + (STAIRS_IMAGE_DIMENSION / 2), y + STAIRS_IMAGE_DIMENSION + 20);
		
	}
	
	/**
	 * 
	 * @return Collision rectangle for the stairs
	 */
	public CollisionRect getCollisionRect() {
		return rect;
	}
		
	
}
