package game.valkyrie.escape.map.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;

import game.valkyrie.escape.tools.CollisionRect;

/**
 * MapFlareGun is a pickup the player can use in order to skip a minigame
 * @author LukeC, Team Valkyrie
 *
 */
public class MapFlareGun {

	// Many static variables that are used within the class several times.
	public static final int FLARE_GUN_DIMENSION = 64;
	
	Texture flareGunTexture; // Texture of the flare gun
	float x; // X coordinate of the flare gun
	float y; // Y coordinate of the flare gun
	private TiledMapTileLayer collisionLayer; // Collision layer of the map on the MapGameScreen, determines collision with solid objects		
	CollisionRect rect; // for checking collisions
	
	public MapFlareGun(float x, float y, MapLayer mapLayer) {
		
		this.x = x; // Set the coordinates of the flare gun on the map
		this.y = y;
		
		flareGunTexture = new Texture("sprites/map_flare_gun.png");
		rect = new CollisionRect(x, y, FLARE_GUN_DIMENSION, FLARE_GUN_DIMENSION);
		this.collisionLayer = (TiledMapTileLayer) mapLayer;
		
	}
	
	/**
	 * Render the flare gun
	 * @param batch SpriteBatch of EscapeGame
	 */
	public void render (SpriteBatch batch) {
		
		batch.draw(flareGunTexture, x, y, FLARE_GUN_DIMENSION, FLARE_GUN_DIMENSION);
		
	}
	 
	/**
	 * 
	 * @return Collision Rectangle of the flare gun
	 */
	public CollisionRect getCollisionRect() {
		return rect;
	}
		
	
}
