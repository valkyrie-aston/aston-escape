package game.valkyrie.escape.map.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Matrix4;

import game.valkyrie.escape.tools.CollisionRect;
import game.valkyrie.escape.tools.GifDecoder;

/**
 * MapOverlay is the black animated texture that mimics intimidating lighting on the MapLevelScreen
 * @author LukeC
 *
 */
public class MapOverlay {
	
	Texture overlayTexture; // overlay texture
	Animation<TextureRegion> animation; // Animation of the overlay
	float elapsed; // Determines which animation frame to play
	SpriteBatch batch; // Unique sprite batch for this class to not
	float x; // X coordinate of the MapOverlay
	float y; // Y coordinate of the MapOverlay
	
	public MapOverlay() {
		
		x = 0;
		y = 0;
		overlayTexture = new Texture("sprites/map_overlay.png");
		animation = GifDecoder.loadGIFAnimation(Animation.PlayMode.LOOP, Gdx.files.internal("sprites/map_overlay_gif.gif").read());
		batch = new SpriteBatch();

		
	}
	
	/**
	 * Update the position of the overlay based on given coordinates - these are based on the player's position
	 * @param x X coordinate
	 * @param y Y coordinate
	 * @param deltaTime Delta time
	 */
	public void update (float x, float y, float deltaTime) {
		
		this.x = x;
		this.y = y;
		
	}
	
	/**
	 * Render the overlay, using a unique sprite batch and unique camera from MapLevelScreen and EscapeGame
	 */
	public void render () {
		
		elapsed += Gdx.graphics.getDeltaTime() / 7;
		OrthographicCamera cam = new OrthographicCamera();
		Matrix4 uiMatrix = cam.combined.cpy();
		uiMatrix.setToOrtho2D(0, 0, 1280, 720);
		batch.setProjectionMatrix(uiMatrix);
		batch.begin();
		batch.draw(overlayTexture, 0, 0);
		batch.draw(animation.getKeyFrame(elapsed), 0, 0, 1280, 720);
		batch.end();
		
	}
		
	
}

