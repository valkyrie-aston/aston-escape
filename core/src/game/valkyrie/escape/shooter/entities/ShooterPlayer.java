package game.valkyrie.escape.shooter.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import game.valkyrie.escape.EscapeGame;
import game.valkyrie.escape.tools.CollisionRect;

/**
 * ShooterPlayer is the player in ShooterGameScreen who can shoot zombies, pick up ammo and be killed by zombies.
 * @author LukeC, Team Valkyrie
 *
 */
public class ShooterPlayer {

	// Many static variables that are used within the class several times.
	public static final int SPEED = 250; // The speed at which the player moves
	public static final int PLAYER_WIDTH_PIXEL = 26; // Width of the player sprite
	public static final int PLAYER_HEIGHT_PIXEL = 32; // Height of player sprite
	public static final int PLAYER_WIDTH = PLAYER_WIDTH_PIXEL * 2;
	public static final int PLAYER_HEIGHT = PLAYER_HEIGHT_PIXEL * 2;
	
	Texture player; // Texture for the player
	float x; // X coordinate of the player
	float y; // Y coordinate of the player
	CollisionRect rect; // Player collision rect
	int healthCount = 3; // Number of hit points of the player
	
	public ShooterPlayer() {
		x = EscapeGame.SCREEN_WIDTH / 2 - PLAYER_WIDTH / 2;
		y = 30;
		rect = new CollisionRect(x, y, PLAYER_WIDTH, PLAYER_HEIGHT);
		player = new Texture("sprites/shooter_player.png");
	}
	
	/**
	 * Update the x coordinate of the player and the collision rectangle.
	 * @param x New x location
	 */
	public void update(float x) {
		this.x = x;
		rect.move(x, y);
	}
	
	/**
	 * Draw the player sprite
	 * @param batch Sprite batch for EscapeGame
	 */
	public void render (SpriteBatch batch) {
		batch.draw(player, x, y, PLAYER_WIDTH, PLAYER_HEIGHT);
	}
	
	/**
	 * 
	 * @return Collision rectangle for the player
	 */
	public CollisionRect getCollisionRect() {
		return rect;
	}
	
	/**
	 * 
	 * @return X coordinate for the player
	 */
	public float getX() {
		return x;
	}
	
	/**
	 * 
	 * @return Y coordinate for the player
	 */
	public float getY() {
		return y;
	}
	
	/**
	 * Alter the player's health number
	 * @param healthCount New value of health
	 */
	public void updateHealthCount(int healthCount) {
		this.healthCount = healthCount;
	}
	
	/**
	 * 
	 * @return Number of hit points the player has for this minigame.
	 */
	public int getHealthCount() {
		return healthCount;
	}
	
}
