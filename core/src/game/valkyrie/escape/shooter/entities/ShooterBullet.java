package game.valkyrie.escape.shooter.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import game.valkyrie.escape.tools.CollisionRect;

/**
 * ShooterBullet are bullets fired by the player in ShooterGameScreen that can kill zombies.
 * @author AmyW, LukeC, Team Valkyrie
 *
 */
public class ShooterBullet {
	
	// Many static variables that are used within the class several times.
	public static final int SPEED = 500;
	public static final int DEFAULT_Y = 60; // Height of the ShooterPlayer sprite
	public static final int BULLET_WIDTH = 3;
	public static final int BULLET_HEIGHT = 12;
	
	private static Texture bulletTexture; // Texture of the bullet
	float x; // X cooordinate of the bullet
	float y; // Y coordinate of the bullet
	public boolean remove = false; // For checking to remove the object
	CollisionRect rect; // For checking collision
	
	public ShooterBullet(float x) {
		this.x = x;
		this.y = DEFAULT_Y;
		rect = new CollisionRect(x, y, BULLET_WIDTH, BULLET_HEIGHT);
		
		// If bullet doesn't have a texture, apply texture
		if(bulletTexture == null) {
			bulletTexture = new Texture("sprites/shooter_bullet.png"); 
		}
	}
	
	/*
	 * Update the bullet and set to remove if it leaves the screen
	 */
	public void update (float deltaTime) {
		y += SPEED * deltaTime;
		
		// If bullet reaches top of the screen, set to delete
		if (y > Gdx.graphics.getHeight()) {
			remove = true;
		}
		
		rect.move(x, y); // Update CollisionRect with new location
	}
	
	/**
	 * Render the bullet
	 * @param batch Sprite batch of EscapeGame
	 */
	public void render (SpriteBatch batch) {
		batch.draw(bulletTexture, x, y);
	}
	
	/**
	 * 
	 * @return Collision rectangle of the bullet
	 */
	public CollisionRect getCollisionRect() {
		return rect;
	}
	
	/**
	 * 
	 * @return X coordinate of the bullet
	 */
	public float getX() {
		return x;
	}
	
	/**
	 * 
	 * @return Y coordinate of the bullet
	 */
	public float getY() {
		return y;
	}
}
