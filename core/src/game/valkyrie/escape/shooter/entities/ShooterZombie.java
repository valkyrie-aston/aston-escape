package game.valkyrie.escape.shooter.entities;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import game.valkyrie.escape.tools.CollisionRect;


/**
 * ShooterZombie is the zombie in ShooterGameScreen that moves towards the player and is killed by bullets
 * @author LukeC, Team Valkyrie
 *
 */
public class ShooterZombie {
	
	// Many static variables that are used within the class several times
	public static final int DEFAULT_Y = 40;
	public static final int ZOMBIE_HEIGHT_PIXEL = 26;
	public static final int ZOMBIE_WIDTH_PIXEL = 32;
	public static final int ZOMBIE_WIDTH = ZOMBIE_WIDTH_PIXEL * 2;
	public static final int ZOMBIE_HEIGHT = ZOMBIE_HEIGHT_PIXEL * 2;
	
	Texture zombieTexture; // Zombies texture
	float x; // X coordinate of the zombie
	float y; // Y coordinate oft he zombie
	public boolean remove = false; // For removing the object upon death
	CollisionRect rect; // for checking collisions
	private int direction; // Left or right, 0 or 1
	private Random random; // Random variable generator
	private int speed_y; // Speed in the x direction
	private int speed_x; // Speed in the y direction
	
	public ShooterZombie (float x) {
		this.x = x;
		this.y = Gdx.graphics.getHeight(); // initial starting point the top of the screen
		rect = new CollisionRect(x, y, ZOMBIE_WIDTH, ZOMBIE_HEIGHT);
		random = new Random();
		
		// Random y speed
		
		int low = 150;
		int high = 350;
		int result = random.nextInt(high - low) + low;
		
		speed_y = result;
		
		// Random x speed
		
		int lowX = 20;
		int highX = 100;
		int resultX = random.nextInt(highX - lowX) + lowX;
		
		speed_x = resultX;
		
		direction = random.nextInt(2); // Determine a direction, where 0 = left and 1 = right
		
		if (zombieTexture == null) {
			zombieTexture = new Texture("sprites/shooter_zombie.png");
		}
	}
	
	/**
	 * Update the zombie's position and their collision rectangle. If they come into contact with a wall, change direction. If they exit the screen, set to remove them on the next frame
	 * @param deltaTime Delta time
	 */
	public void update (float deltaTime) {
		y -= speed_y * deltaTime; // Moving down
		
		if (y < -ZOMBIE_HEIGHT) { // If beyond the bottom of the screen, set to remove
			remove = true;
		}
		
		// Direction code, if 0, go left, if 1, go right
		
		if (direction == 0) {
			x -= speed_x * deltaTime;
		} else if (direction == 1) {
			x += speed_x * deltaTime;
		}
		
		// Direction code, if hit edge of boundaries, reverse direction
		
		if (x < game.valkyrie.escape.screens.ShooterGameScreen.LEFT_BOUNDARY) {
			direction = 1;
		} else if (x + ShooterZombie.ZOMBIE_WIDTH > game.valkyrie.escape.screens.ShooterGameScreen.RIGHT_BOUNDARY) {
			direction = 0;
		}
		
		// Update collision rect
		
		rect.move(x, y);
	}
	
	/**
	 * Render the zombie
	 * @param batch Sprite Batch for EscpaeGame
	 */
	public void render (SpriteBatch batch) {
		batch.draw(zombieTexture, x, y, ZOMBIE_WIDTH, ZOMBIE_HEIGHT);
	}
	
	/**
	 * 
	 * @return Collision rectangle for zombies
	 */
	public CollisionRect getCollisionRect() {
		return rect;
	}
	
	/**
	 * 
	 * @return X coordinate of the zombie
	 */
	public float getX() {
		return x;
	}
	
	/**
	 * 
	 * @return Y coordinate of the zombie
	 */
	public float getY() {
		return y;
	}
	
	/**
	 * Flips the direction, from right to left or left to right.
	 */
	public void changeDirection() {
		
		if (direction == 0) {
			direction = 1;
		} else if (direction == 1) {
			direction = 0;
		}
		
	}
}
