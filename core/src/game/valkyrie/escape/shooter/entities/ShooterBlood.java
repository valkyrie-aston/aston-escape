package game.valkyrie.escape.shooter.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * ShooterBlood is an animated texture that is created when a zombie is killed in ShooterGameScreen
 * @author LukeC, Team Valkyrie
 *
 */
public class ShooterBlood {
	
	// Many static variables that are used within the class several times.
	public static final float FRAME_LENGTH = 0.1f; // How long each animation frame plays
	public static final int SPEED = 150;
	private static final int OFFSET = 20;
	public static final int BLOOD_WIDTH = 102;
	public static final int BLOOD_HEIGHT = 121;
	
	private static Animation<TextureRegion> animation = null; // Animation of the blood splatter
	float x, y; // X and y coordinates of blood splatter
	float stateTime; // What point of the animation the current display is at
	public boolean remove = false; // When set to true, the object is removed from the game screen
	
	
	public ShooterBlood (float x, float y) {
		this.x = x - OFFSET;
		this.y = y - OFFSET;
		stateTime = 0;
		
		if (animation == null)
			animation = new Animation<TextureRegion>(FRAME_LENGTH, TextureRegion.split(new Texture("sprites/shooter_blood.png"), BLOOD_WIDTH, BLOOD_HEIGHT) [0]);
	}
	
	/**
	 * Update the location of the blood splatter and remove when animation is completed
	 * @param deltaTime Delta time
	 */
	public void update (float deltaTime) {
		y -= SPEED * deltaTime;
		stateTime += deltaTime; // This statement is unneeded if the animation loops
		if (animation.isAnimationFinished(stateTime)) {
			remove = true;
		}
	}
	
	/**
	 * Render the blood splatter at it's correct animation frame
	 * @param batch Sprite Batch of EscapeGame
	 */
	public void render (SpriteBatch batch) {
		batch.draw(animation.getKeyFrame(stateTime), x, y);
	}
	
}
