package game.valkyrie.escape.shooter.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import game.valkyrie.escape.tools.CollisionRect;

/**
 * ShooterAmmo is a pickup that the player can collect in ShooterGameScreen in order to gain ammo.
 * @author LukeC, Team Valkyrie
 *
 */
public class ShooterAmmo {
	
	// Many static variables that are used within the class several times.
	public static final int SPEED = 150;
	public static final int DEFAULT_Y = 40;
	public static final int AMMO_HEIGHT_PIXEL = 32;
	public static final int AMMO_WIDTH_PIXEL = 32;
	public static final int AMMO_WIDTH = AMMO_WIDTH_PIXEL * 1;
	public static final int AMMO_HEIGHT = AMMO_HEIGHT_PIXEL * 1;
	
	Texture bulletTexture; // Texture for ammo
	float x; // X coordinate of ammo
	float y; // Y coordinate of ammo
	public boolean remove = false; // For removing the object upon collection
	CollisionRect rect; // for checking collisions
	
	public ShooterAmmo (float x) {
		this.x = x;
		this.y = Gdx.graphics.getHeight(); // initial starting point the top of the screen
		rect = new CollisionRect(x, y, AMMO_WIDTH, AMMO_HEIGHT);
		
		if (bulletTexture == null) {
			bulletTexture = new Texture("sprites/shooter_ammo.png");
		}
	}
	
	/**
	 * Updates the location of the ammo, removes the item if falls bellow screen
	 * @param deltaTime Delta time
	 */
	public void update (float deltaTime) {
		y -= SPEED * deltaTime; // Moving down
		
		if (y < -AMMO_HEIGHT) { // If beyond the bottom of the screen, set to remove
			remove = true;
		}
		rect.move(x, y);
	}
	
	/**
	 * Render the bullet
	 * @param batch Sprite batch for EscapeGame
	 */
	public void render (SpriteBatch batch) {
		batch.draw(bulletTexture, x, y);
	}
	
	/**
	 * 
	 * @return The collision rectangle for the ammo
	 */
	public CollisionRect getCollisionRect() {
		return rect;
	}
	
	/**
	 * 
	 * @return The x coordinate of the ammo
	 */
	public float getX() {
		return x;
	}
	
	/**
	 * 
	 * @return The y coordinate of the ammo
	 */
	public float getY() {
		return y;
	}
}
