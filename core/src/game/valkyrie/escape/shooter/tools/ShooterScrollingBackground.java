package game.valkyrie.escape.shooter.tools;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import game.valkyrie.escape.screens.ShooterGameScreen;

/**
 * This class represent the scrolling background in ShooterGameScreen. it uses two images that lie above one another
 * @author LukeC, Team Valkyrie
 *
 */
public class ShooterScrollingBackground {
	
	// Many static variables that are used within the class several times.
	public static final int DEFAULT_SPEED = 150;
	public static final int ACCELERATION = 150;
	public static final int GOAL_REACH_ACCELERATION = 200;
	
	Texture shooterFloor; // Texture of the background
	float y1; // First image
	float y2; // Second image
	int speed; // Pixels/second
	int goalSpeed; // Speed after acceleration
	boolean speedFixed; // If the speed is fixed
	
	public ShooterScrollingBackground() {
		shooterFloor = new Texture("sprites/shooter_floor.png");
		y1 = 0;
		y2 = shooterFloor.getHeight();
		speed = 0;
		goalSpeed = DEFAULT_SPEED;
	}
	
	/**
	 * Update the locations of both textures and change their speed to reach goal speed
	 * @param delta Delta time
	 */
	public void update(float delta) {
		// Speed adjustment to reach goal
		if (speed < goalSpeed) {
			speed += GOAL_REACH_ACCELERATION * delta;
				if (speed > goalSpeed) {
					speed = goalSpeed;
				}
			} else if (speed > goalSpeed) {
				speed -= GOAL_REACH_ACCELERATION * delta;
				if (speed < goalSpeed) {
					speed = goalSpeed;
				}
			}	
				
		if (!speedFixed) {
			speed += ACCELERATION * delta;
		}
				
			y1 -= speed * delta;
			y2 -= speed * delta;
				
		// See if image is off screen, set to top
		if (y1 + shooterFloor.getHeight() <= 0) {
			y1 = y2 + shooterFloor.getHeight();
		}
				
		if (y2 + shooterFloor.getHeight() <= 0) {
			y2 = y1 + shooterFloor.getHeight();
		}
	}
	
	/**
	 * Renders both background textures
	 * @param batch Sprite batch of EscapeGame
	 */
	public void render(SpriteBatch batch) {
		batch.draw(shooterFloor, ShooterGameScreen.LEFT_BOUNDARY, y1);
		batch.draw(shooterFloor, ShooterGameScreen.LEFT_BOUNDARY, y2);
	}
	
	/**
	 * Sets the final speed of the background
	 * @param goalSpeed the final speed of the background
	 */
	public void setSpeed(int goalSpeed) {
		this.goalSpeed = goalSpeed;
	}
	
	/**
	 * Sets the fixed speed of the background
	 * @param speedFixed the fixed speed of the background
	 */
	public void setSpeedFixed (boolean speedFixed) {
		this.speedFixed = speedFixed;
	}
}

