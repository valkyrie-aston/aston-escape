package game.valkyrie.escape.defender.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import game.valkyrie.escape.EscapeGame;

/**
 * DefenderHold represent locations in which a zombie or survivor is held. It controls if they live/die and will render either a zombie or survivor depending on what is being held.
 * @author LukeC, Team Valkyrie
 *
 */
public class DefenderHole {
	
	// Many static variables that are used within the class several times.
	public static final float DEFENDER_HOLE_WIDTH = 128;
	public static final float DEFENDER_HOLE_HEIGHT = 128;
	
	// Hold either a zombie or a survivor
	public DefenderZombie zombie;
	public DefenderSurvivor survivor;
	
	// Texture of hole
	Texture holeTexture;
	
	// Position of hole in door
	float x;
	float y;
	
	public DefenderHole(float x, float y) {
		this.x = x;
		this.y = y;
		zombie = null;
		survivor = null;
		
		// Load Texture
		holeTexture = new Texture("sprites/defender_hole.png");
	}
	
	/**
	 * Render the hole alongside either a zombie or survivor
	 * @param batch Sprite Batch of EscapeGame
	 */
	public void render(SpriteBatch batch) {
		batch.draw(holeTexture, x, y);
		
		// If there is either a zombie or a survivor, draw.
		if (zombie != null) {
			zombie.render(batch);
		} else if (survivor != null) {
			survivor.render(batch);
		} else {
			;;
		}
		
	}
	
	/**
	 * 
	 * @return Return the zombie found in the hole
	 */
	public DefenderZombie getZombie() {
		return zombie;
	}
	
	/**
	 * Create a zombie to be placed in the hole.
	 */
	public void makeZombie() {
		zombie = new DefenderZombie(x, y);
	}
	
	/**
	 *  
	 * @return The player found in the hole
	 */
	public DefenderSurvivor getSurvivor() {
		return survivor;
	}
	
	/**
	 * Create a survivor to be placed in the hole
	 */
	public void makeSurvivor() {
		survivor = new DefenderSurvivor(x, y);
	}

	/**
	 * Set survivor to empty if saved
	 */
	public void saveSurvivor() {
		survivor = null;
	}
	
	/**
	 * Set either entity to empty if killed
	 */
	public void killEntity() {
		survivor = null;
		zombie = null;
	}
	
	/**
	 * Checks for left click mouse input and will kill an entity if one is found
	 * @param hole The hole being checked for an entity to be killed
	 * @return the score change depending on if an entity was killed
	 */
	public int checkInputLeft(DefenderHole hole) {
		
		int scoreChange = 0; // The type of entity which was killed, if any, see above.
		
		if (Gdx.input.getX() < hole.getX() + DefenderHole.DEFENDER_HOLE_WIDTH && Gdx.input.getX() > hole.getX() && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() < hole.getY() + DefenderHole.DEFENDER_HOLE_HEIGHT && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() > hole.getY() && Gdx.input.isButtonJustPressed(Buttons.LEFT)) {
			if (hole.isOccupied()) {
				if (hole.zombie != null) {
					scoreChange = 1;
				} else if (hole.survivor != null) {
					scoreChange = 2;
				} else {
					scoreChange = 0;
				}
				hole.killEntity();
			}
		}
		
		return scoreChange;
	}
	
		/**
		 * Checks for right click mouse input and will save a survivor if one is found
		 * @param hole The hole being checked for an entity to be survivor
		 * @return the score change depending on if an entity was saved
		 */
	public int checkInputRight(DefenderHole hole) {
		
		int scoreChange = 0; // Will adjust if a survivor is saved
		
		if (Gdx.input.getX() < hole.getX() + DefenderHole.DEFENDER_HOLE_WIDTH && Gdx.input.getX() > hole.getX() && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() < hole.getY() + DefenderHole.DEFENDER_HOLE_HEIGHT && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() > hole.getY() && Gdx.input.isButtonJustPressed(Buttons.RIGHT)) {
			if (hole.isOccupied() && hole.survivor != null) {
				scoreChange = 1; // Saved Survivor
				hole.killEntity();
			} else if (hole.isOccupied() && hole.zombie != null) {
				scoreChange = 2; // Saved Zombie
			}
		}
		
		return scoreChange;
		
	}
	
	/**
	 * Checks to see if a hole is filled by either type of entity
	 * @return If filled
	 */
	public boolean isOccupied() {
		if (zombie != null || survivor != null) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * @return X coordinate of hole
	 */
	public float getX() {
		return x;
	}
	
	/**
	 * 
	 * @return Y coordinate of hole
	 */
	public float getY() {
		return y;
	}
}
