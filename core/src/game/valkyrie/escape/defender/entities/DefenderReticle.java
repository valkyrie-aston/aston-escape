package game.valkyrie.escape.defender.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * DefenderReticle displays where the players mouse cursor lies, and will change colours to show the previous action of shoot or save
 * @author LukeC, Team Valkryie
 *
 */
public class DefenderReticle {
	
	// Many static variables that are used within the class several times.
	public static final float RETICLE_HEIGHT = 64;
	public static final float RETICLE_WIDTH = 64;
	
	
	Texture defenderReticleInactive; // Texture of the regular reticle
	Texture defenderReticleActiveLeft; // texture of the reticle when attempting to kill an entity
	Texture defenderReticleActiveRight; // texture of the reticle when attempting to save an entity
	
	float x; // X coordinate of the reticle
	float y; // Y coordinate of the reticle
	float activeTime; // How long reticle should remain activr
	boolean active; // If reticle is active
	int button; // Either 0 or 1 depending on button pressed. 2 for inactive state.
	
	public DefenderReticle (float x, float y) {
		this.x = x;
		this.y = y;
		activeTime = 0;
		active = false;
		button = 2; // Inactive state
		
		
		// Create textures
		defenderReticleActiveLeft = new Texture("sprites/defender_reticle_left_active.png");
		defenderReticleActiveRight = new Texture("sprites/defender_reticle_right_active.png");
		defenderReticleInactive = new Texture("sprites/defender_reticle_inactive.png");
	}
	
	/**
	 * Update the location of the reticle and alter the texture to display after a timer
	 * @param x X coordinate of mouse cursor
	 * @param y Y coordinate of mouse cursor
	 * @param delta Delta time
	 */
	public void update (float x, float y, float delta) {
		this.x = x;
		this.y = y;
		
		// Lower timer
		activeTime -= delta;
		
		// If activeTime is done, change active to inactive, change button state to 2
		if (activeTime <= 0) {
			active = false;
			button = 2;
		}
	}
	
	/**
	 * Render the reticle, texture determined by if the reticle is active and either left/right click is being pressed
	 * @param batch Sprite batch of EscapeGame
	 */
	public void render (SpriteBatch batch) {
		
		if (active == false) {
			batch.draw(defenderReticleInactive, x, y);
		} else if (active == true && button == 0) {
			batch.draw(defenderReticleActiveLeft, x ,y);
		} else if (active == true && button == 1) {
			batch.draw(defenderReticleActiveRight, x, y);
		}
	}
	
	/**
	 * Set the reticle to active, as well as setting which mouse button is being pressed
	 * @param button Either 0 for left click being pressed (kill), or 1 for right click being pressed (save)
	 */
	public void setActive(int button) {
		active = true;
		activeTime = 0.2f;
		this.button = button;
	}
	
	/**
	 * @return true if reticle is active
	 */
	public boolean isActive() {
		return active;
	}
	
}
