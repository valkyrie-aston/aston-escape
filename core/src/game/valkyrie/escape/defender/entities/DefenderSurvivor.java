package game.valkyrie.escape.defender.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * DefenderSurvivor is a class that represent a survivor entiy that can be saved in DefenderGameScreen
 * @author LukeC, Team Valkyrie
 *
 */
public class DefenderSurvivor {
	
	Texture survivorTexture; // Texture of the survivor
	
	float x; // X coordinate of the survivor
	float y; // Y coordinate of the survivor
	float aliveTimer; // How long the survivor lasts before they die and are removed
	
	public DefenderSurvivor (float x, float y) {
		this.x = x;
		this.y = y;
		aliveTimer = 4.0f;
		
		// Apply survivor texture
		survivorTexture = new Texture("sprites/defender_survivor_alive.png");
	}
	
	/**
	 * Updates the timer of how long the survivor stays alive for
	 * @param delta Delta time
	 */
	public void update(float delta) {
		aliveTimer -= delta;
	}
	
	/**
	 * Render the survivor
	 * @param batch Sprite Batch of EscapeGame
	 */
	public void render (SpriteBatch batch) {
		batch.draw(survivorTexture, x, y);
	}
	
	/**
	 * @return How long the survivor will remain before it is removed/dies
	 */
	public float getTimer() {
		return aliveTimer;
	}
	
}
