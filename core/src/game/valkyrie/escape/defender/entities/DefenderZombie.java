package game.valkyrie.escape.defender.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * DefenderZombie is a class that represent a survivor entity that can be killed in DefenderGameScreen
 * @author LukeC, Team Valkyrie
 *
 */
public class DefenderZombie {
	
	Texture zombieTexture; // Texture of the zombie
	
	float x; // X coordinate of the zombie
	float y; // Y coordinate of the zombie
	float aliveTimer; // How long the zombie lasts before it attacks the player
	
	public DefenderZombie (float x, float y) {
		this.x = x;
		this.y = y;
		aliveTimer = 4.0f;
		
		// Apply zombie texture
		zombieTexture = new Texture("sprites/defender_zombie_alive.png");
	}
	
	/**
	 * Updates the timer of how long the zombie remains before the player loses a life
	 * @param delta Delta time
	 */
	public void update(float delta) {
		aliveTimer -= delta;
	}
	
	/**
	 * Render the zombie
	 * @param batch Sprite Batch of EscapeGame
	 */
	public void render(SpriteBatch batch) {
		batch.draw(zombieTexture, x, y);
	}
	
	/**
	 * @return How long the zombie will remain before it is removed
	 */
	public float getTimer() {
		return aliveTimer;
	}
}
