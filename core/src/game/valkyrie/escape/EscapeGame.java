package game.valkyrie.escape;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import game.valkyrie.escape.audio.Audio;
import game.valkyrie.escape.screens.*;

/**
 * EscapeGame is a class used to generator the game, it's SpriteBatch and hold player information
 * @author LukeC, Team Valkyrie
 *
 */

public class EscapeGame extends Game {
	
	// ** Static variables determining screen size alongside a SpriteBatch accessible by other classes **
	public static final int SCREEN_WIDTH = 1280;
	public static final int SCREEN_HEIGHT = 720;
	public SpriteBatch batch;
	
	// Player Variables That Are Held - Hel through entire game
	int playerHealth;
	int playerScore;
	boolean hasFlareGun;
	int zombiesKilled;
	
	/**
	 * Create the game, load the audio, set default player values and set the screen as the Main Menu
	 */
	@Override
	public void create () {
		playerHealth = 3;
		playerScore = 0;
		zombiesKilled = 0;
		hasFlareGun = false;
		Audio.load("audio/main_menu_theme.mp3", "theme");
		Audio.load("audio/level_music.mp3", "level one music");
		Audio.load("audio/game_over.ogg", "game over");
		Audio.load("audio/button_select.mp3", "button select");
		Audio.load("audio/bomb_explosion.mp3", "bomb explosion");
		Audio.load("audio/pass_bomb.mp3", "pass bomb");
		Audio.load("audio/item_collect.mp3", "item collect");
		Audio.load("audio/zombie_scream.mp3", "zombie scream");
		Audio.load("audio/you_win.mp3", "you win");
		Audio.load("audio/taking_damage.mp3", "taking damage");
		Audio.load("audio/human_scream.mp3", "human scream");
		Audio.load("audio/win_minigame.mp3", "win minigame");
		Audio.load("audio/lose_minigame.mp3", "lose minigame");
		batch = new SpriteBatch(); // Batch to hold objects on screen.
		Gdx.graphics.setTitle("Aston Escape");
		this.setScreen(new MainMenuScreen(this));
	}
	
	/**
	 * Method not called as this class does not display anything
	 */
	@Override
	public void render () {
		super.render();
	}
	
	/**
	 * When a minigame is won, the number of zombies killed is increased, and score is added.
	 */
	public void winMiniGame() {	
		addScore(250);	
		zombiesKilled += 1;
	}
	
	/**
	 * When a minigame is lost, player health is decreaed
	 */
	public void loseMiniGame() {
		playerHealth -= 1;
	}
	
	/**
	 * When food is picked up, gain health, to a maximum of 3.
	 */
	public void addHealth() {
		// Max HP is 3
		if (playerHealth < 3) {
			playerHealth += 1;
		}
	}
	
	/**
	 * When game is launched, reset the player's stats so previous game attempts do not carry over.
	 */
	public void resetStats() {
		playerHealth = 3;
		playerScore = 0;
		zombiesKilled = 0;
		hasFlareGun = false;
	}
	
	/**
	 * @return the player's current health
	 */
	public int getHealth() {
		return playerHealth;
	}
	
	/**
	 * Add score to player's total score
	 * @param score The amount the score should increase by
	 */
	public void addScore(int score) {
		playerScore += score;
	}
	
	/**
	 * @return The player's total score
	 */
	public int getScore() {
		return playerScore;
	}
	
	/**
	 * @return True if the player has a flare gun powerup, false if not
	 */
	public boolean hasFlareGun() {
		return hasFlareGun;
	}
	
	/**
	 * Set if a player has a flaregun, either from pick up or from using it
	 * @param has True or False if they should have or if they should not
	 */
	public void setHasFlareGun(boolean has) {
		hasFlareGun = has;	
	}
	
	/**
	 * Reset the zombies killed to 0
	 */
	public void resetZombiesKilled() {
		zombiesKilled = 0;
	}
	
	/**
	 * Get the number of zombies killed
	 * @return The value of zombiesKilled
	 */
	public int getZombiesKilled() {
		return zombiesKilled;
	}
	
}
