package game.valkyrie.escape.screens;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.utils.Align;

import game.valkyrie.escape.EscapeGame;
import game.valkyrie.escape.audio.Audio;
import game.valkyrie.escape.key.entities.KeyFallKey;
import game.valkyrie.escape.key.entities.KeyFallPlayer;

/**
 * KeyFallGameScreen - Objective, a key falls after a set delay, the player has one chance to catch the key, otherwise the game is lost
 * @author LukeC, Team Valkyrie
 *
 */
public class KeyFallGameScreen implements Screen {

	// Many static variables that are used within the class several times.
	public static final float UI_DEFAULT_Y = Gdx.graphics.getHeight() - 100; // Default y Position for the UI elements
	
	EscapeGame game; // Our escape game used across all screens
	KeyFallPlayer player; // Player Object
	KeyFallKey key; // Key object
	Texture keyBackground; // Background for the game
	Texture brokenKey; // Broken Key for when a game is lost
	Random random;
	float dropTimer; // How long before the key drops
	float gameOverTimer; // After a miss/catch, start counting down before returning to the previous screen
	boolean gameEnded; // Set to true after a catch attempt is made
	boolean playerWon; // If the player won or player lost
	BitmapFont uiFont; // The font for the ui
	private Screen screen; // The current map screen
	
	public KeyFallGameScreen (EscapeGame game, Screen screen) {
		this.game = game;
		this.screen = screen;
		random = new Random();
		
		// Random dropTimer
		float min = 2.0f;
		float max = 3.5f;
		dropTimer = min + random.nextFloat() * (max - min);
		
		gameEnded = false;
		playerWon = false;
		gameOverTimer = 2.0f;
		
		// Load Background Texture and Key
		keyBackground = new Texture("sprites/key_background.png");
		brokenKey = new Texture("sprites/key_key_broke.png");
		
		// Load Other Objects
		int keySize = random.nextInt(3) + 1;
		key = new KeyFallKey(keySize);
		player = new KeyFallPlayer();
		uiFont = new BitmapFont(Gdx.files.internal("fonts/lunchtime.fnt"));
	}
	
	@Override
	public void show() {
	}
	
	@Override
	public void render(float delta) {
		
		// ** Flare Gun Code
		if (game.hasFlareGun() == true && Gdx.input.isKeyJustPressed(Keys.F)) {
			game.winMiniGame();
			game.setHasFlareGun(false);
			this.dispose();
			game.setScreen(screen);
		}
		
		// ** PLAYER CODE
		
		// If player presses space, check if key overlaps with hand, if yes, set as a win, if no, set as a loss
		if (player.getOpenHand() == true && Gdx.input.isKeyJustPressed(Keys.SPACE) && gameEnded == false) {
			if (player.getCollisionRect().collidesWith(key.getCollisionRect())) {
				playerWon = true;
				gameEnded = true;
				player.closeHand();
				Audio.play("pass bomb");
			} else {
				playerWon = false;
				gameEnded = true;
				player.closeHand();
				Audio.play("bomb explosion");
			}
		}
		
		// ** KEY CODE
		dropTimer -= delta;
		
		// If drop timer falls below 0, drop the key
		if (dropTimer < 0 && key.isDropped() == false) {
			key.dropKey();
		}
		
		// If Key reaches below bottom of the screen, set game over
		
		// If game hasn't ended and the key has been dropped, update key's position
		if (key.isDropped() == true && gameEnded == false) {
			key.update(delta);
		}
		
		if (key.getY() < -250) {
			gameEnded = true;
		}
		
		// ** TIMER CODE
		if (gameEnded == true) {
			gameOverTimer -= delta;
		}
		
		// ** GAME OVER CHECK
		if (gameOverTimer < 0) {
			if (playerWon == true) {
				game.winMiniGame();
				this.dispose();
				Audio.play("win minigame");
				game.setScreen(screen);
			} else {
				game.loseMiniGame();
				this.dispose();
				Audio.play("lose minigame");
				game.setScreen(screen);
			}
		}
		
		// ** SPRITE RENDER CODE
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		game.batch.begin();
		
		// Draw Background
		game.batch.draw(keyBackground, 0, 0);
		
		// Draw Player
		player.render(game.batch);
		
		// Draw Key
		
		// When game is active
		if (gameEnded == false) {
			key.render(game.batch);
		}
		
		// When game is over
		if (gameEnded == true && playerWon == true) {
			key.render(game.batch);
		} else if (gameEnded == true && playerWon == false) {
			game.batch.draw(brokenKey, Gdx.graphics.getWidth() / 2 - 100, Gdx.graphics.getHeight() / 2, 200, 70); 
		}
		
		// Win Requirement Display
		GlyphLayout scoreRequiredLayout = new GlyphLayout(uiFont, "Catch the Key!", Color.WHITE, 0, Align.center, false);
		uiFont.draw(game.batch, scoreRequiredLayout, Gdx.graphics.getWidth() / 2, 650);
		
		// Draw Winning Message
		String winOrLose = "";
		
		if (playerWon == false) {
			winOrLose = "You Lose.";
		} else if (playerWon == true) {
			winOrLose = "You Win!";
		}
		
		GlyphLayout winDisplayLayout = new GlyphLayout(uiFont, "" + winOrLose, Color.WHITE, 1f, Align.center, true);
		if (gameEnded == true) {
			uiFont.draw(game.batch, winDisplayLayout, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);
		}
		
		// If Skip is available, draw saying so
		
		if (game.hasFlareGun()) {
			GlyphLayout flareGunLayout = new GlyphLayout(uiFont, "PRESS F TO SKIP", Color.YELLOW, 0, Align.left, false);
			uiFont.draw(game.batch, flareGunLayout, 50, 50);
		}
		
		game.batch.end();
		
	}
	
	@Override
	public void resize(int width, int height) {
	}
	
	@Override
	public void pause() {
	}
	
	@Override
	public void resume() {
	}
	
	@Override
	public void hide() {
	}
	
	@Override
	public void dispose() {
	}
	
}
