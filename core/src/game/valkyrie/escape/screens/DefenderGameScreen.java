package game.valkyrie.escape.screens;

import java.util.ArrayList;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.utils.Align;

import game.valkyrie.escape.EscapeGame;
import game.valkyrie.escape.audio.Audio;
import game.valkyrie.escape.defender.entities.DefenderHole;
import game.valkyrie.escape.defender.entities.DefenderReticle;
import game.valkyrie.escape.tools.Notify;
import game.valkyrie.escape.tools.Timer;

/**
 * DefenderGameScreen - Objective, shoot zombies that appear in holes whilst saving survivors who appear there. Gain a certain amount of points to win without losing 3 lives for missing a zombie or survivor.
 * @author LukeC, ArjunS Team Valkyrie
 *
 */
public class DefenderGameScreen implements Screen {
	
	// Many static variables that are used within the class several times.
	public static final float MIN_ENTITY_SPAWN_TIME = 0.2f; // Minimum spawn time for zombie/survivor.
	public static final float MAX_ENTITY_SPAWN_TIME = 0.4f; // Maximum spawn time for zombie/survivor.
	public static final float UI_DEFAULT_Y = Gdx.graphics.getHeight() - 100; // Default y Position for the UI elements
	public static final float LIFE_WIDTH_PIXEL = 32; // Width of heart in pixel
	public static final float LIFE_HEIGHT_PIXEL = 32; // Height of heart in pixel
	public static final float LIFE_WIDTH = 50;
	public static final float LIFE_HEIGHT = 50;
	
	EscapeGame game; // Our escape game used across all screens
	Timer timer; // Timer for the game
	DefenderHole[] holes; // Array of DefenderHoles, fixed amount so only regular Array is needed.
	DefenderReticle reticle; // Reticle display.
	Texture defenderBackground; // Background image for game
	Texture healthHeart; // Represents life count
	Texture healthHeartEmpty; // Represents empty life
	ArrayList<Notify> notifyItems; // Messages for items
	Random random; // For placing zombies/survivors
	float spawnTimer; // Spawn Timer for entity
	BitmapFont uiFont; // The font for the UI
	int score; // Score for the game, zombies give 50 points, survivors give 100 points, lose 200 upon killing survivor
	int scoreRequired; // Score needed to win the game
	int healthCount = 3; // Represents the players HP
	private Screen screen; // Holds the map screen
	
	
	public DefenderGameScreen (EscapeGame game, Screen screen) {
		this.game = game;
		this.screen = screen;
		spawnTimer = 1;
		score = 0;
		scoreRequired = 1500;
		
		// DefenderHoles Objects within an Array
		holes = new DefenderHole[9];
		holes[0] = new DefenderHole(414, 428); // Location of top left hole.
		holes[1] = new DefenderHole(577, 428); // Location of the top middle hole.
		holes[2] = new DefenderHole(740, 428); // Location of the top right hole.
		holes[3] = new DefenderHole(414, 263); // Location of the left middle hole.
		holes[4] = new DefenderHole(577, 263); // Location of the middle middle hole.
		holes[5] = new DefenderHole(740, 263); // Location of the right middle hole.
		holes[6] = new DefenderHole(414, 98); // Location of the bottom left hole.
		holes[7] = new DefenderHole(577, 98); // Location of the bottom middle hole.
		holes[8] = new DefenderHole(740, 98); // Location of the bottom right hole.
		
		// 720 - 292, 720 - 457, 720 - 622
		
		// Create other objects
		random = new Random();
		timer = new Timer(100, UI_DEFAULT_Y, 20); // Create timer, place it, set a time
		defenderBackground = new Texture("sprites/defender_background.png");
		reticle = new DefenderReticle(0, 0);
		uiFont = new BitmapFont(Gdx.files.internal("fonts/lunchtime.fnt"));
		healthHeart = new Texture("sprites/shooter_heart.png");
		healthHeartEmpty = new Texture("sprites/shooter_heart_empty.png");
		notifyItems = new ArrayList<Notify>();
		
	}
	
	@Override
	public void render(float delta) {
		
		// ** Notify Code
		
		ArrayList<Notify> notifyItemsToRemove = new ArrayList<Notify>();
		for (Notify notify : notifyItems) {
					
			notify.update(delta);
					
			if (notify.toBeRemoved()) {
				notifyItemsToRemove.add(notify);
			}
					
		}
		
		// ** Flare Gun Code
		
		if (game.hasFlareGun() == true && Gdx.input.isKeyJustPressed(Keys.F)) {
			game.winMiniGame();
			game.setHasFlareGun(false);
			this.dispose();
			game.setScreen(screen);
		}
		
		// ** Game Over Conditions - If Health Count = 0 or Timer = 0
		
		// Go to map screen if life is depleted
		if (healthCount <= 0) {
			this.dispose();
			Audio.play("lose minigame");
			game.loseMiniGame();
			game.setScreen(screen);
		}
		
		// Go to map screen if time is depleted
		if (timer.getTime() <= 0) {
			this.dispose();
			Audio.play("lose minigame");
			game.loseMiniGame();
			game.setScreen(screen);
		}
		
		// Go to map screen if you beat the required number of points
		if (score >= scoreRequired) {
			this.dispose();
			Audio.play("win minigame");
			game.winMiniGame();
			game.setScreen(screen);
		}
		
		// ** Timer Code
		
		// Update Timer
		timer.update(delta);
		
		// ** Entity Code
		
		// Update Entities in Holes
		for (int i = 0; i < holes.length; i++) {
			
			// Check to see if Survivor/Zombie Timers end, if so, kill the entity and lose a life.
			
			// Update Zombie's Alive Timer, remove health if below 0
			if (holes[i].zombie != null) {
				holes[i].zombie.update(delta);
				if (holes[i].zombie.getTimer() < 0) {
					holes[i].killEntity(); 
					healthCount--;
					notifyItems.add(new Notify(game, "-Life", holes[i].getX() + holes[i].DEFENDER_HOLE_WIDTH / 2, holes[i].getY() + holes[i].DEFENDER_HOLE_HEIGHT));
				}
			}
			
			if (holes[i].survivor != null) {
				holes[i].survivor.update(delta);
				if (holes[i].survivor.getTimer() < 0) {
					holes[i].killEntity(); 
					healthCount--;
					notifyItems.add(new Notify(game, "-Life", holes[i].getX() + holes[i].DEFENDER_HOLE_WIDTH / 2, holes[i].getY() + holes[i].DEFENDER_HOLE_HEIGHT));
				}
			}
		}
		
		// Minus delta from timer, if it goes below 0, choose a random hole from the array, if it's not full, spawn a random entity
		spawnTimer -= delta;
		int holeIndex = random.nextInt(holes.length); // Random Index in the holes array
		
		if (spawnTimer < 0) {
			spawnTimer = random.nextFloat() * (MAX_ENTITY_SPAWN_TIME - MIN_ENTITY_SPAWN_TIME) + MIN_ENTITY_SPAWN_TIME; // Spawn timer between MIN/MAX spawn time
			
			// Check to see if Hole has an entity, if no entity, create one at random.
			if (!holes[holeIndex].isOccupied()) {
				int entityChance = random.nextInt(5) + 1; // Number between 1-6. 1 = Survivor, 2/3/4/5/6 = Zombie.
					if (entityChance < 2) {
						holes[holeIndex].makeSurvivor();
					} else if (entityChance > 1) {
						holes[holeIndex].makeZombie();
					}
			} else {
				;; // Do nothing if hole is occupied
			}
			
		}
		
		// Kill code: If Left Clicked, remove Entity, adjust score depending upon entity (returned value)
		
		for (int i = 0; i < holes.length; i++) {
			int scoreChange = holes[i].checkInputLeft(holes[i]);
			if (scoreChange == 1) {
				Audio.play("zombie scream");
				score += 50;
				notifyItems.add(new Notify(game, "+50", holes[i].getX() + holes[i].DEFENDER_HOLE_WIDTH / 2, holes[i].getY() + holes[i].DEFENDER_HOLE_HEIGHT));
			} else if (scoreChange == 2) {
				Audio.play("human scream");
				score -= 200;
				healthCount--;
				notifyItems.add(new Notify(game, "-200", holes[i].getX() + holes[i].DEFENDER_HOLE_WIDTH / 2, holes[i].getY() + holes[i].DEFENDER_HOLE_HEIGHT));
				notifyItems.add(new Notify(game, "-Life", holes[i].getX() + holes[i].DEFENDER_HOLE_WIDTH / 2, holes[i].getY() + holes[i].DEFENDER_HOLE_HEIGHT + 30));
			}
		}
		
		// Save code: if Right Clicked, remove Survivor Entity, adjust score
		
		for (int i = 0; i < holes.length; i++) {
			int scoreChange = holes[i].checkInputRight(holes[i]);
			if (scoreChange == 1) {
				score += 100;
				notifyItems.add(new Notify(game, "+100", holes[i].getX() + holes[i].DEFENDER_HOLE_WIDTH / 2, holes[i].getY() + holes[i].DEFENDER_HOLE_HEIGHT));
			} else if (scoreChange == 2) {
				healthCount--;
				notifyItems.add(new Notify(game, "-Life", holes[i].getX() + holes[i].DEFENDER_HOLE_WIDTH / 2, holes[i].getY() + holes[i].DEFENDER_HOLE_HEIGHT));
			}
		}
		
		// ** Reticle Code
		
		if (Gdx.input.isButtonJustPressed(Buttons.LEFT)) {
			reticle.setActive(0); // 0 = left click
		}
		
		if (Gdx.input.isButtonJustPressed(Buttons.RIGHT)) {
			reticle.setActive(1);
		}
		
		// Update Recticle - Placing reticle at mouse cursor for each frame
		reticle.update(Gdx.input.getX() - DefenderReticle.RETICLE_WIDTH / 2, Gdx.graphics.getHeight() - Gdx.input.getY() - DefenderReticle.RETICLE_HEIGHT / 2, delta);
		
		// ** REMOVE UNEEDED SPRITES
		
		notifyItems.removeAll(notifyItemsToRemove);
		
		// Render sprites
		
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		game.batch.begin();
		
		// Draw Background
		
		game.batch.draw(defenderBackground, 0, 0);
		
		// Draw Holes
		
		for (int i = 0; i < holes.length; i++) {
			holes[i].render(this.game.batch);
		}
		
		// Draw reticle
		
		reticle.render(game.batch);
		
		// Draw Text
		
		//Score
		GlyphLayout scoreHeaderLayout = new GlyphLayout(uiFont, "SCORE:", Color.WHITE, 1f, Align.right, true);
		GlyphLayout scoreCountLayout = new GlyphLayout(uiFont, "" + score, Color.YELLOW, 1f, Align.right, true);
		uiFont.draw(game.batch, scoreHeaderLayout, Gdx.graphics.getWidth() - 10 - scoreHeaderLayout.width, UI_DEFAULT_Y);
		uiFont.draw(game.batch, scoreCountLayout, Gdx.graphics.getWidth() - 10 - scoreHeaderLayout.width, UI_DEFAULT_Y - 40);
		
		
		//Life
		GlyphLayout hpHeaderLayout = new GlyphLayout(uiFont, "HEALTH:", Color.WHITE, 1f, Align.right, true);
		uiFont.draw(game.batch, hpHeaderLayout, Gdx.graphics.getWidth() - 10 - hpHeaderLayout.width, UI_DEFAULT_Y - 140); 
		
		// Score Requirement display	
		GlyphLayout scoreRequiredLayout = new GlyphLayout(uiFont, "Score " + scoreRequired + " Points!", Color.WHITE, 0, Align.center, false);
		uiFont.draw(game.batch, scoreRequiredLayout, Gdx.graphics.getWidth() / 2, 650);
		
		// Draw player health alongside empty hearts -- THIS NEEDS CLEANUP, I didn't do any calculation for X/Y position, all trial and error!
		
		int heartOffset = 0; // Variables for offset
		int blankHeartOffset = 0;
				
		// Render empty hearts first, 
		for (int i = 0; i < 3; i++) {
			game.batch.draw(healthHeartEmpty, Gdx.graphics.getWidth() - 290 + blankHeartOffset, UI_DEFAULT_Y - 175 - LIFE_HEIGHT, LIFE_WIDTH, LIFE_WIDTH);
			blankHeartOffset += LIFE_WIDTH + 10;
		}
				
		for (int i = 0; i < healthCount; i++) {
			game.batch.draw(healthHeart, Gdx.graphics.getWidth() - 290 + heartOffset, UI_DEFAULT_Y - 175 - LIFE_HEIGHT, LIFE_WIDTH, LIFE_WIDTH);
			heartOffset += LIFE_WIDTH + 10;
		}
		
		// Draw Timer
		timer.render(game.batch);
		
		// If Skip is available, draw saying so
		
		if (game.hasFlareGun()) {
			GlyphLayout flareGunLayout = new GlyphLayout(uiFont, "PRESS F TO SKIP", Color.YELLOW, 0, Align.left, false);
			uiFont.draw(game.batch, flareGunLayout, 50, 50);
		}	
		
		// Render Notify
		
		for (Notify notify : notifyItems) {
			notify.render(game.batch);
		}
		
		game.batch.end();
		
	}
	
	
	// Inherited, empty methods.
	
	@Override
	public void show() {}
	
	@Override
	public void resize(int width, int height) {}

	@Override
	public void pause() {}

	@Override
	public void resume() {}

	@Override
	public void hide() {}

	@Override
	public void dispose() {}

}
