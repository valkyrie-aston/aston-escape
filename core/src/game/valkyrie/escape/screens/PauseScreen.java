package game.valkyrie.escape.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.utils.Align;
import game.valkyrie.escape.EscapeGame;
import game.valkyrie.escape.audio.Audio;

/**
 * The pause screen which ia accessed from the MapLevelScreen and pauses the game. You can continue or exit to the menu
 * @author LukeC, Team Valkyrie
 *
 */
public class PauseScreen implements Screen {
	
	// Many static variables that are used within the class several times.
	private static final int PAUSE_BUTTON_WIDTH = 136;
	private static final int PAUSE_BUTTON_HEIGHT = 43;
	
	// Game Used by Every Screen
	EscapeGame game;
	
	private BitmapFont uiFont;
	private int score;
	private SpriteBatch uiBatch;
	Texture resume;
	Texture exit;
	private Screen screen; // The current map screen
	
	public PauseScreen (EscapeGame game, Screen screen) {
		
		this.game = game;
		this.screen = screen;
		score = game.getScore();
		uiBatch = new SpriteBatch();
		
		// Load Font and Textures
		uiFont = new BitmapFont(Gdx.files.internal("fonts/lunchtime.fnt"));
		resume = new Texture("sprites/pause_resume.png");
		exit = new Texture("sprites/pause_exit.png");
		
	}

	@Override
	public void show() {
	}

	@Override
	public void render(float delta) {
		
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		// HUD BATCH TO DRAW UI, SETUP NEW CAMERA WHICH IS A FIXED UI
		
		OrthographicCamera cam = new OrthographicCamera();
		Matrix4 uiMatrix = cam.combined.cpy();
		uiMatrix.setToOrtho2D(0, 0, 1280, 720);
		uiBatch.setProjectionMatrix(uiMatrix);
		uiBatch.begin();
		
		// Draw Pause Text
		GlyphLayout pauseLayout = new GlyphLayout(uiFont, "PAUSED", Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, pauseLayout, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 100);	
		
		// Draw score text
		GlyphLayout scoreLayout = new GlyphLayout(uiFont, "Current Score: " + score, Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, scoreLayout, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 200);
		
		// Draw the buttons
		uiBatch.draw(resume, Gdx.graphics.getWidth() / 4 - PAUSE_BUTTON_WIDTH / 2, Gdx.graphics.getHeight() - 500, PAUSE_BUTTON_WIDTH, PAUSE_BUTTON_HEIGHT);
		uiBatch.draw(exit, Gdx.graphics.getWidth() / 4 + Gdx.graphics.getWidth() / 2 - PAUSE_BUTTON_WIDTH / 2, Gdx.graphics.getHeight() - 500, PAUSE_BUTTON_WIDTH, PAUSE_BUTTON_HEIGHT);
		
		float xRight = Gdx.graphics.getWidth() / 4 + Gdx.graphics.getWidth() / 2 - PAUSE_BUTTON_WIDTH / 2 + PAUSE_BUTTON_WIDTH;
		float xLeft = Gdx.graphics.getWidth() / 4 + Gdx.graphics.getWidth() / 2 - PAUSE_BUTTON_WIDTH / 2;
		float yTop = Gdx.graphics.getHeight() - 500 + PAUSE_BUTTON_HEIGHT;
		float yBottom = Gdx.graphics.getHeight() - 500;
		
		if (Gdx.input.getX() < xRight && Gdx.input.getX() > xLeft && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() < yTop && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() > yBottom) {
			if (Gdx.input.justTouched()) {
				this.dispose();
				Audio.stop("level one music"); // Level one music will begin
				game.setScreen(new MainMenuScreen(game));
			}
		}
		
		float resumeXRight = Gdx.graphics.getWidth() / 4 - PAUSE_BUTTON_WIDTH / 2 + PAUSE_BUTTON_WIDTH;
		float resumeXLeft = Gdx.graphics.getWidth() / 4  - PAUSE_BUTTON_WIDTH / 2;
		
		if (Gdx.input.getX() < resumeXRight && Gdx.input.getX() > resumeXLeft && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() < yTop && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() > yBottom) {
			if (Gdx.input.justTouched()) {
				this.dispose();
				game.setScreen(screen);
			}
		}
		
		uiBatch.end();
		
		
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void dispose() {
	}
	
}
