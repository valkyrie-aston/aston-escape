package game.valkyrie.escape.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Align;

import game.valkyrie.escape.EscapeGame;

/**
 * Unused Class
 * @author AmyW, Team Valkyrie
 *
 */
public class InstructionsScreen implements Screen{

	private BitmapFont uiFont;
	private static final int MENU_BUTTON_WIDTH = 313;
	private static final int MENU_BUTTON_HEIGHT = 50;
	private static final int MENU_LOGO_WIDTH = 256;
	private static final int MENU_LOGO_HEIGHT = 128;
	
	private SpriteBatch uiBatch;
	
	EscapeGame game; // Our EscapeGame used across all screens.
	
	private static final int MENU_LOGO_Y = 500; // Y Value of the logo.
	private static final int MENU_BUTTON_Y = 100;
	
	Texture logo;
	Texture menuButtonInactive;
	Texture menuButtonActive;
	
	public InstructionsScreen(EscapeGame game) {
		// TODO Auto-generated constructor stub
		this.game = game;
		uiBatch = new SpriteBatch();
		logo = new Texture("sprites/menu_logo.png");
		menuButtonInactive = new Texture("sprites/menu_button_inactive.png");
		menuButtonActive = new Texture("sprites/menu_button_active.png");
		
		
		// Load Font
		uiFont = new BitmapFont(Gdx.files.internal("fonts/lunchtime.fnt"));
		uiFont.getData().setScale(0.7f);
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(float delta) {
		// Set the background as a dark gray and clear the space. In Float.
		Gdx.gl.glClearColor(0.4f, 0.4f,	0.4f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		// Begin SpriteBatch
		uiBatch.begin();			
					

		//  Draw text
		GlyphLayout mapLayout = new GlyphLayout(uiFont, "To navigate around the map use the keyboard arrow keys.", Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, mapLayout, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 150);
		
		GlyphLayout secondlineLayout = new GlyphLayout(uiFont, "Try to collect as many of the resources on the map as possible" , Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, secondlineLayout, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 190);
		
		GlyphLayout thirdlineLayout = new GlyphLayout(uiFont, "in order to increase your score and gain extra lives." , Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, thirdlineLayout, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 230);
		
		GlyphLayout fourthlineLayout = new GlyphLayout(uiFont, "If you touch a zombie, a random mini games will start" , Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, fourthlineLayout, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 270);
		
		GlyphLayout fifthlineLayout = new GlyphLayout(uiFont, "if you win, the zombie will be defeated and your score increased." , Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, fifthlineLayout, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 310);
		
				
		int buttonX = EscapeGame.SCREEN_WIDTH / 2 - MENU_BUTTON_WIDTH / 2; // Local variable holding the centre of the screen for all buttons
		// Exit Button
		
				if (Gdx.input.getX() < buttonX + MENU_BUTTON_WIDTH && Gdx.input.getX() > buttonX && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() < MENU_BUTTON_Y + MENU_BUTTON_HEIGHT && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() > MENU_BUTTON_Y) {
					uiBatch.draw(menuButtonActive, buttonX, MENU_BUTTON_Y, MENU_BUTTON_WIDTH, MENU_BUTTON_HEIGHT);
								
					// If button is clicked, exit program as there is no current screen for this option
						if (Gdx.input.justTouched()) {
							this.dispose();
							game.setScreen(new MainMenuScreen(game));
						}	
					} else { 
						uiBatch.draw(menuButtonInactive, buttonX, MENU_BUTTON_Y, MENU_BUTTON_WIDTH, MENU_BUTTON_HEIGHT);
				}
		
		// Stop SpriteBatch
		
		uiBatch.end();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
