package game.valkyrie.escape.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Align;

import game.valkyrie.escape.EscapeGame;
import game.valkyrie.escape.audio.Audio;

/**
 * Secondary instructions screen, which has specific minigame information and you can practise each minigame
 * @author LukeC, Team Valkyrie
 *
 */
public class InstructionsScreen3 implements Screen {

	private static final int MENU_BUTTON_WIDTH = 313;
	private static final int MENU_BUTTON_HEIGHT = 50;
	private static final int MENU_BUTTON_Y = 40;
	private static final int ARROW_WIDTH = 64;
	private static final int ARROW_HEIGHT = 64;
	private static final int ARROW_BUTTON_Y = 405;
	private static final int PLAY_BUTTON_WIDTH = 180;
	private static final int PLAY_BUTTON_HEIGHT = 40;
	private static final int PLAY_BUTTON_Y = 120;
	
	private BitmapFont uiFont;
	private SpriteBatch uiBatch;
	EscapeGame game; // Our EscapeGame used across all screens.
	Texture menuButtonInactive;
	Texture menuButtonActive;
	Texture arrowLeft;
	Texture arrowRight;
	Texture playButtonActive;
	Texture playButtonInactive;
	int gameIndex;
	String description1;
	String description2;
	String description3;
	String description4;
	String description5;
	
	public InstructionsScreen3 (EscapeGame game) {
		this.game = game;
		uiBatch = new SpriteBatch();
		menuButtonInactive = new Texture("sprites/menu_button_inactive.png");
		menuButtonActive = new Texture("sprites/menu_button_active.png");
		arrowLeft = new Texture("sprites/minigame_arrow_left.png");
		arrowRight = new Texture("sprites/minigame_arrow_right.png");
		playButtonInactive = new Texture("sprites/play_button_inactive.png");
		playButtonActive = new Texture("sprites/play_button_active.png");
		gameIndex = 1; // Represent which minigame to display
		description1 = "1";
		description2 = "2";
		description3 = "3";
		description4 = "4";
		description5 = "5";
		
		// Load Font
		uiFont = new BitmapFont(Gdx.files.internal("fonts/lunchtime.fnt"));
	}
	
	@Override
	public void render(float delta) {
		// Check if index is beyond bounds, if so, change value
		if (gameIndex <= 0) {
			gameIndex = 6;
		} else if (gameIndex > 6) {
			gameIndex = 1;
		}
		
		// Set the background as a dark gray and clear the space. In Float.
		Gdx.gl.glClearColor(0.4f, 0.4f,	0.4f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		uiBatch.begin();
		
		// Header
		uiFont.getData().setScale(1.0f);
		GlyphLayout screenHeader = new GlyphLayout(uiFont, "Minigames:", Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, screenHeader, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 50);
		
		// Base Instructions
		uiFont.getData().setScale(0.7f);
		GlyphLayout firstBaseLine = new GlyphLayout(uiFont, "Winning a minigame gains you 250 Score. Losing a minigame loses a life.", Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, firstBaseLine, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 120);
		GlyphLayout secondBaseLine = new GlyphLayout(uiFont, "When you have a flare gun, you can press the 'F' key to skip a minigame - counting as a win.", Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, secondBaseLine, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 150);
		
		// Minigame display and buttons
		uiFont.getData().setScale(1.0f);
		GlyphLayout minigameHeader = new GlyphLayout(uiFont, "" + getMinigame(), Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, minigameHeader, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 270);
		
		int leftArrowX = EscapeGame.SCREEN_WIDTH / 4 - ARROW_WIDTH / 2 + 100;
		if (Gdx.input.getX() < leftArrowX + ARROW_WIDTH && Gdx.input.getX() > leftArrowX && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() < ARROW_BUTTON_Y + ARROW_HEIGHT && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() > ARROW_BUTTON_Y) {
			if (Gdx.input.justTouched()) {
				Audio.play("pass bomb");
				gameIndex -= 1;
			}
		}
		uiBatch.draw(arrowLeft, leftArrowX, ARROW_BUTTON_Y, ARROW_WIDTH, ARROW_HEIGHT);
		
		int rightArrowX = EscapeGame.SCREEN_WIDTH / 4 + EscapeGame.SCREEN_WIDTH / 2 - ARROW_WIDTH / 2 - 120;
		if (Gdx.input.getX() < rightArrowX + ARROW_WIDTH && Gdx.input.getX() > rightArrowX && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() < ARROW_BUTTON_Y + ARROW_HEIGHT && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() > ARROW_BUTTON_Y) {
			if (Gdx.input.justTouched()) {
				Audio.play("pass bomb");
				gameIndex += 1;
			}
		}
		uiBatch.draw(arrowRight, rightArrowX, ARROW_BUTTON_Y, ARROW_WIDTH, ARROW_HEIGHT);
		
		// Description for game
		uiFont.getData().setScale(0.8f);
		getDescription();
		GlyphLayout minigameDescription1 = new GlyphLayout(uiFont, "" + description1, Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, minigameDescription1, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 340);
		GlyphLayout minigameDescription2 = new GlyphLayout(uiFont, "" + description2, Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, minigameDescription2, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 380);
		GlyphLayout minigameDescription3 = new GlyphLayout(uiFont, "" + description3, Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, minigameDescription3, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 420);
		GlyphLayout minigameDescription4 = new GlyphLayout(uiFont, "" + description4, Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, minigameDescription4, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 460);
		GlyphLayout minigameDescription5 = new GlyphLayout(uiFont, "" + description5, Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, minigameDescription5, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 500);
		
		// Play practise button
		int playButtonX = EscapeGame.SCREEN_WIDTH / 2 - PLAY_BUTTON_WIDTH / 2;
		if (Gdx.input.getX() < playButtonX + PLAY_BUTTON_WIDTH && Gdx.input.getX() > playButtonX && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() < PLAY_BUTTON_Y + PLAY_BUTTON_HEIGHT && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() > PLAY_BUTTON_Y) {
			uiBatch.draw(playButtonActive, playButtonX, PLAY_BUTTON_Y, PLAY_BUTTON_WIDTH, PLAY_BUTTON_HEIGHT);
								
			// If button is clicked, exit program as there is no current screen for this option
			if (Gdx.input.justTouched()) {
				Audio.play("button select");
				launchMinigame();
			}
			
		} else { 
			uiBatch.draw(playButtonInactive, playButtonX, PLAY_BUTTON_Y, PLAY_BUTTON_WIDTH, PLAY_BUTTON_HEIGHT);
		}
		
		// Exit Button
		int buttonX = EscapeGame.SCREEN_WIDTH / 2 - MENU_BUTTON_WIDTH / 2;
		if (Gdx.input.getX() < buttonX + MENU_BUTTON_WIDTH && Gdx.input.getX() > buttonX && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() < MENU_BUTTON_Y + MENU_BUTTON_HEIGHT && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() > MENU_BUTTON_Y) {
			uiBatch.draw(menuButtonActive, buttonX, MENU_BUTTON_Y, MENU_BUTTON_WIDTH, MENU_BUTTON_HEIGHT);
								
			// If button is clicked, exit program as there is no current screen for this option
			if (Gdx.input.justTouched()) {
				this.dispose();
				Audio.play("button select");
				game.setScreen(new MainMenuScreen(game));
			}
			
		} else { 
			uiBatch.draw(menuButtonInactive, buttonX, MENU_BUTTON_Y, MENU_BUTTON_WIDTH, MENU_BUTTON_HEIGHT);
		}
				
		uiBatch.end();
	}
	
	public String getMinigame() {
		String returnString = "";
		switch(gameIndex) {
		case 1:
			returnString = "Shooter";
			break;
		case 2:
			returnString = "Defender";
			break;
		case 3:
			returnString = "Bomb";
			break;
		case 4:
			returnString = "Key";
			break;
		case 5:
			returnString = "Breakout";
			break;
		case 6:
			returnString = "Dodge";
			break;
		}
		return returnString;
	}
	
	public void getDescription() {
		switch(gameIndex) {
		case 1: // Shooter
			description1 = "Score points by shooting zombies within the time limit.";
			description2 = "Use the spacebar to shoot and the arrow keys to move left and right.";
			description3 = "If you are hit 3 times or the timer hits 0, you lose.";
			description4 = "Pick up ammo crates to refill your ammo.";
			description5 = "Your ammo count can be seen on the right side of the screen.";
			break;
		case 2: // Defender
			description1 = "Score points by shooting zombies and rescuing survivors within the time limit.";
			description2 = "Use the mouse to aim the reticle. Left click to shoot zombies,";
			description3 = "and right click to save survivors. If you don't react to";
			description4 = "what appears, you will lose a life.  If you are reduced to 0";
			description5 = "you will lose the minigame.";
			break;
		case 3: // Bomb
			description1 = "Win the minigame by finishing with the bomb";
			description2 = "on the zombies side of the field. Press the";
			description3 = "spacebar in order to throw the bomb.";
			description4 = "Pay attention to the timer, you can't hold on";
			description5 = "to the bomb forever.";
			break;
		case 4: // Key
			description1 = "Win the minigame by catching the key as it falls.";
			description2 = "When the key overlaps your hand, press the spacebar.";
			description3 = "If you do it to early, or the key falls below your hand";
			description4 = "you will lose the minigame.";
			description5 = "";
			break;
		case 5: // Breakout
			description1 = "Score points by destroying bricks with your ball within the time limit.";
			description2 = "Move the paddle left and right using the arrow keys.";
			description3 = "If the ball hits the bottom of the screen or the timer hits 0";
			description4 = "you will lose the minigame.";
			description5 = "";
			break;
		case 6: // Dodge
			description1 = "Win the minigame by avoiding the obstacles ahead of you.";
			description2 = "Use the up arrow key to jump over zombies.";
			description3 = "Use the down arrow key to duck under pillars.";
			description4 = "If you hit any obstacle, you will lose the minigame.";
			description5 = "If you avoid the obstacles, you will win thee minigame.";
			break;
		}
	}
	
	public void launchMinigame() {
		this.dispose();
		switch(gameIndex) {
		case 1: // Shooter
			game.setScreen(new ShooterGameScreen(game, this));
			break;
		case 2: // Defender
			game.setScreen(new DefenderGameScreen(game, this));
			break;
		case 3: // Bomb
			game.setScreen(new BombGameScreen(game, this));
			break;
		case 4: // Key
			game.setScreen(new KeyFallGameScreen(game, this));
			break;
		case 5: // Breakout
			game.setScreen(new BreakoutGameScreen(game, this));
			break;
		case 6: // Dodge
			game.setScreen(new DodgeGameScreen(game, this));
			break;
		}
	}

	@Override
	public void resize(int width, int height) {
	}
	
	@Override
	public void show() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void dispose() {
	}

}
