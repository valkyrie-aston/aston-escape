package game.valkyrie.escape.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.utils.Align;

import game.valkyrie.escape.EscapeGame;

/**
 * Highscore Screen accessed from the menu, displays the high score.
 * @author LukeC, Team Valkyrie
 *
 */
public class HighscoreScreen implements Screen {

		private static final int MENU_BUTTON_WIDTH = 313;
		private static final int MENU_BUTTON_HEIGHT = 50;
		private static final int MENU_BUTTON_Y = 200; // Y Value of the Menu Button
		
		// Game Used by Every Screen
		EscapeGame game;
		
		private BitmapFont uiFont;
		private int highscore;
		private SpriteBatch uiBatch;
		private Preferences prefs;
		Texture menuButtonInactive;
		Texture menuButtonActive;
		Texture highscoreTexture;
	
	public HighscoreScreen(EscapeGame game) {
		
		this.game = game;
		uiBatch = new SpriteBatch();
		
		// Searching for highscore in preferences file
		prefs = Gdx.app.getPreferences("escapegame");
		highscore = prefs.getInteger("highscore", 0);
		
		// Load Font
		uiFont = new BitmapFont(Gdx.files.internal("fonts/lunchtime.fnt"));
		uiFont.getData().setScale(1.5f);
	
		// Load Exit Button
		menuButtonInactive = new Texture("sprites/menu_button_inactive.png");
		menuButtonActive = new Texture("sprites/menu_button_active.png");
		
		highscoreTexture = new Texture("sprites/highscore_button_inactive.png");
		
	}
		
	@Override
	public void render(float delta) {
		
		Gdx.gl.glClearColor(0.4f, 0.4f,	0.4f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		// HUD BATCH TO DRAW UI, SETUP NEW CAMERA WHICH IS A FIXED UI
		
		OrthographicCamera cam = new OrthographicCamera();
		Matrix4 uiMatrix = cam.combined.cpy();
		uiMatrix.setToOrtho2D(0, 0, 1280, 720);
		uiBatch.setProjectionMatrix(uiMatrix);
		uiBatch.begin();
		
		int buttonX = EscapeGame.SCREEN_WIDTH / 2 - MENU_BUTTON_WIDTH / 2; // Local variable holding the centre of the screen for all buttons
		
		// Exit Button
		
		if (Gdx.input.getX() < buttonX + MENU_BUTTON_WIDTH && Gdx.input.getX() > buttonX && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() < MENU_BUTTON_Y + MENU_BUTTON_HEIGHT && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() > MENU_BUTTON_Y) {
			uiBatch.draw(menuButtonActive, buttonX, MENU_BUTTON_Y, MENU_BUTTON_WIDTH, MENU_BUTTON_HEIGHT);
						
			// If button is clicked, exit program as there is no current screen for this option
				if (Gdx.input.justTouched()) {
					this.dispose();
					game.setScreen(new MainMenuScreen(game));
				}	
			} else { 
				uiBatch.draw(menuButtonInactive, buttonX, MENU_BUTTON_Y, MENU_BUTTON_WIDTH, MENU_BUTTON_HEIGHT);
		}
		
		uiBatch.draw(highscoreTexture, buttonX, MENU_BUTTON_Y + 300, MENU_BUTTON_WIDTH, MENU_BUTTON_HEIGHT);
		
		GlyphLayout highscoreLayout = new GlyphLayout(uiFont, "" + highscore, Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, highscoreLayout, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 250);
		
		uiBatch.end();
		
	}

	@Override
	public void resize(int width, int height) {}

	@Override
	public void pause() {}

	@Override
	public void resume() {}

	@Override
	public void hide() {}

	@Override
	public void dispose() {
	}
	
	@Override
	public void show() {}

}
