package game.valkyrie.escape.screens;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.utils.Align;
import game.valkyrie.escape.EscapeGame;
import game.valkyrie.escape.audio.Audio;
import game.valkyrie.escape.bomb.entities.BombBomb;
import game.valkyrie.escape.bomb.entities.BombExplosion;
import game.valkyrie.escape.bomb.entities.BombPlayer;
import game.valkyrie.escape.bomb.entities.BombZombie;
import game.valkyrie.escape.bomb.tools.BombTimer;

/**
 * BombGameScreen - Objective, throw the bomb to the zombie, whilt it throws it back to you. End with the bomb on the zombies side of the screen.
 * @author LukeC, ArjunS, Team Valkyrie
 *
 */
public class BombGameScreen implements Screen {

	// Many static variables that are used within the class several times.
	public static final float UI_DEFAULT_Y = Gdx.graphics.getHeight() - 100; // Default y Position for the UI elements
	
	EscapeGame game; // Our escape game used across all screens
	BombPlayer player; // Player Object
	BombZombie zombie; // Zombie object
	BombBomb bomb; // Bomb object
	Texture bombBackground; // Background
	Random random;
	BombTimer timerDisplay; // Timer for the game, when it hits 0, the bomb explodes
	float timer; // Timer to be parsed into the display
	float gameOverTimer; // After bomb has exploded, start counting down, once 0, go to game over screen
	boolean gameEnded; // Set to true after bomb has exploded
	boolean playerWon; // if player won or player lost
	int score; // No score for this game, required for current game over screen
	BombExplosion explosion; // Explosion after timer runs out
	BitmapFont uiFont; // The font for the UI
	private Screen screen; // The map screen
	
	public BombGameScreen (EscapeGame game, Screen screen) {
		this.game = game;
		this.screen = screen;
		random = new Random();
		timer = 10; // How long the game lasts before the bomb explodes
		gameEnded = false;
		playerWon = false;
		gameOverTimer = 2.0f;
		score = 0;
		
		// Load background texture
		bombBackground = new Texture("sprites/bomb_background.png");
		
		// Load other objects
		bomb = new BombBomb(random.nextInt(2)); // 0 = Going left, 1 = Going Right, 2 = Stationary, Randomly determine
		player = new BombPlayer();
		zombie = new BombZombie();
		timerDisplay = new BombTimer(Gdx.graphics.getWidth() / 2, 200, 15, Color.WHITE); // Place timer in centre of screen, use a 15 second timer
		uiFont = new BitmapFont(Gdx.files.internal("fonts/lunchtime.fnt"));
		explosion = null; // Created once bomb explodes
	}
	
	@Override
	public void render(float delta) {
		
		// ** Flare Gun Code
		
		if (game.hasFlareGun() == true && Gdx.input.isKeyJustPressed(Keys.F)) {
			game.winMiniGame();
			game.setHasFlareGun(false);
			this.dispose();
			game.setScreen(screen);
		}
		
		// ** Explosion Code
		
		// Update Explosion
		if (gameEnded == true) {
			explosion.update(delta);
		}
		
		// ** Timer Code
		
		// Update Timer
		timer -= delta;
		timerDisplay.update(timer);
		
		if (gameEnded == true) {
			gameOverTimer -= delta;
		}
		
		// If < 10, change color to yellow, if below 5, change to red
		if (timer < 10 && timer > 5) {
			timerDisplay.setColor(Color.YELLOW);
		} else if (timer <= 5) {
			timerDisplay.setColor(Color.RED);
		}
		
		// ** Game Over Check
		
		if (timer <= 0 && gameEnded == false) {
			
			// Check who's side of the screen the bomb is on, if zombie side, player wins
			if (bomb.getX() + BombBomb.BOMB_WIDTH / 2 >= 640) {
				playerWon = true;
				score = 1000;
			} else if (bomb.getX() + BombBomb.BOMB_WIDTH / 2 < 640) {
				playerWon = false;
			}
			
			gameEnded = true;
			player.throwBomb();
			zombie.throwBomb();
			
			// Set explosion to play at bomb location
			explosion = new BombExplosion(bomb.getX(), bomb.getY());
			
			bomb.remove();
			
		}
		
		// Go to game over screen if Game Over Timer goes below 0
		
		if (gameOverTimer < 0) {
			if (playerWon == true) {
				game.winMiniGame();
				this.dispose();
				Audio.play("win minigame");
				game.setScreen(screen);
			} else {
				game.loseMiniGame();
				this.dispose();
				Audio.play("lose minigame");
				game.setScreen(screen);
			}
		}
		
		// ** Player Code
		
		// If player presses spacebar whilst holding bomb, throw bomb.
		if (player.holdingBomb() && Gdx.input.isKeyJustPressed(Keys.SPACE)) {
			Audio.play("pass bomb");// Sound will play when bomb is thrown
			player.throwBomb(); // Throw bomb, set to not be holding bomb
			bomb.setState(1); // Set state to be going right.
		}
		
		// If holding bomb, and hold timer goes below 0, throw bomb.
		if (player.holdingBomb() && player.getHoldTimer() < 0) {
			Audio.play("pass bomb");// Sound will play when bomb is thrown
			player.throwBomb(); // Throw bomb, set to not be holding bomb
			bomb.setState(1); // Set state to going Right
		}
		
		// Update Player
		player.update(delta);
		
		// ** Zombie Code
		
		// If holding bomb, and hold timer goes below 0, throw bomb.
		if (zombie.holdingBomb() && zombie.getHoldTimer() < 0) {
			Audio.play("pass bomb");// Sound will play when bomb is thrown
			zombie.throwBomb(); // Throw bomb, set to not be holding bomb
			bomb.setState(0); // Set state to going Right
		}
		
		// If holding bomb, and throw timer goes below 0, throw bomb, may be earlier than holdTimer
		if (zombie.holdingBomb() && zombie.getThrowTimer() < 0) {
			Audio.play("pass bomb");// Sound will play when bomb is thrown
			zombie.throwBomb(); // Throw bomb, set to not be holding bomb
			bomb.setState(0); // Set state to going right.
		}
		
		// Update Zombie
		zombie.update(delta);
		
		// ** Bomb Code
		
		// Update Bomb
		bomb.update(delta);
		
		// ** AFTER ALL UPDATES, CHECK FOR COLLISSIONS
		
		// Bomb and Player
		if (bomb.getState() == 0) {
			if (bomb.getCollisionRect().collidesWith(player.getCollisionRect())) { // Collision Occured
				bomb.setState(2); // Set to HOLD
				player.holdBomb(); // Player is set to be holding bomb
			}
		}
		
		// Bomb and Zombie
		if (bomb.getState() == 1) {
			if (bomb.getCollisionRect().collidesWith(zombie.getCollisionRect())) { // Collision Occured
				bomb.setState(2); // Set to HOLD
				zombie.holdBomb(); // Player is set to be holding bomb
			}
		}
		
		// Render sprites
		
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		game.batch.begin();
				
		// Objects rendered last = on top, like a layering system
				
		// Draw background
		
		game.batch.draw(bombBackground, 0, 0);
		
		// Draw Player
		player.render(game.batch);
		
		// Draw Zombie
		zombie.render(game.batch);
		
		// Draw bomb
		bomb.render(game.batch);
		
		// Draw Timer

		GlyphLayout timerHeaderLayout = new GlyphLayout(uiFont, "TIME:", Color.WHITE, 1f, Align.center, true);
		if (gameEnded == false) { 
			timerDisplay.render(game.batch); 
			uiFont.draw(game.batch, timerHeaderLayout, Gdx.graphics.getWidth() / 2, 250);
		}
		
		// Draw Explosion
		
		if (gameEnded == true && explosion.remove == false) {
			explosion.render(game.batch);
			Audio.play("bomb explosion");
		}
		
		// Win Requirement display	
		
		GlyphLayout scoreRequiredLayout = new GlyphLayout(uiFont, "Finish With the Bomb on Their Side!", Color.WHITE, 0, Align.center, false);
		uiFont.draw(game.batch, scoreRequiredLayout, Gdx.graphics.getWidth() / 2, 650);
		
		// Draw winning message
		
		String winOrLose = "";
		
		if (playerWon == false) {
			winOrLose = "You Lose.";
		} else if (playerWon == true) {
			winOrLose = "You Win!";
		}
		
		GlyphLayout winDisplayLayout = new GlyphLayout(uiFont, "" + winOrLose, Color.WHITE, 1f, Align.center, true);
		if (gameEnded == true) {
			uiFont.draw(game.batch, winDisplayLayout, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);
		}
		
		// If Skip is available, draw saying so
		
		if (game.hasFlareGun()) {
			GlyphLayout flareGunLayout = new GlyphLayout(uiFont, "PRESS F TO SKIP", Color.YELLOW, 0, Align.left, false);
			uiFont.draw(game.batch, flareGunLayout, 50, 50);
		}
				
		game.batch.end();
		
	}

	// Inherited empty methods
	
	@Override
	public void show() {}
	
	@Override
	public void resize(int width, int height) {}

	@Override
	public void pause() {}

	@Override
	public void resume() {}

	@Override
	public void hide() {}

	@Override
	public void dispose() {}

}
