package game.valkyrie.escape.screens;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;

import game.valkyrie.escape.EscapeGame;
import game.valkyrie.escape.audio.Audio;

/**
 * The main menu screen for the game that access various other screens such as playing the game or instructions
 * @author AmyW, LukeC, Team Valkyrie
 *
 */
public class MainMenuScreen implements Screen{
	// Many static variables that are used within the class several times.
	
		private static final int MENU_BUTTON_WIDTH = 200;
		private static final int MENU_BUTTON_HEIGHT = 50;
		private static final int MENU_BUTTON_Y1 = 400; // Y Value of the first button.
		private static final int MENU_BUTTON_Y2 = 300; // Y Value of the second button.
		private static final int MENU_BUTTON_Y3 = 200; // Y Value of the third button.
		private static final int MENU_BUTTON_Y4 = 100; // Y Value of the fourth button.
		private static final int MENU_LOGO_WIDTH = 256;
		private static final int MENU_LOGO_HEIGHT = 128;
		private static final int MENU_LOGO_Y = 500; // Y Value of the logo.
		
		private SpriteBatch uiBatch;
		float timeDelay; // So you cannot press a button immediately
		
		EscapeGame game; // Our EscapeGame used across all screens.
		
		// Create Texture fields
		Texture exitButtonInactive;
		Texture exitButtonActive;
		Texture logo;
		Texture playButtonActive;
		Texture playButtonInactive;
		Texture highscoreButtonActive;
		Texture highscoreButtonInactive;
		Texture instructionsButtonActive;
		Texture instructionsButtonInactive;
		
		
		// Constructor takes the EscapeGame object in order to render objects to the SpriteBatch
	
		public MainMenuScreen(EscapeGame game) {
			
			Audio.play("theme");// Plays audio on the main menu screen
			Audio.loop("theme");// Ensures the audio loops
			
			// Set constructor argument as field
			this.game = game;
			uiBatch = new SpriteBatch(); // SpriteBatch for this screen
			timeDelay = 0.3f;
			
			// Set textures to each corresponding field
			exitButtonInactive = new Texture("sprites/exit_button_inactive.png");
			exitButtonActive = new Texture("sprites/exit_button_active.png");
			logo = new Texture("sprites/menu_logo.png");
			playButtonActive = new Texture("sprites/play_button_active.png");
			playButtonInactive = new Texture("sprites/play_button_inactive.png");
			highscoreButtonActive = new Texture("sprites/highscore_button_active.png");
			highscoreButtonInactive = new Texture("sprites/highscore_button_inactive.png");
			instructionsButtonActive = new Texture("sprites/instruction_button_active.png");
			instructionsButtonInactive = new Texture("sprites/instruction_button_inactive.png");
			
		}

		

		@Override
		public void render(float delta) {
			
			timeDelay -= delta;
			
			// Set the background as a dark gray and clear the space. In Float.
			Gdx.gl.glClearColor(0.4f, 0.4f,	0.4f, 1f);
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
			OrthographicCamera cam = new OrthographicCamera();
			Matrix4 uiMatrix = cam.combined.cpy();
			uiMatrix.setToOrtho2D(0, 0, 1280, 720);
			uiBatch.setProjectionMatrix(uiMatrix);
			
			// Begin SpriteBatch
			uiBatch.begin();
			
			// Draw Logo
			int logoX = EscapeGame.SCREEN_WIDTH / 2 - MENU_LOGO_WIDTH / 2; // Local variable holding the centre of the screen for the logo
			uiBatch.draw(logo, logoX, MENU_LOGO_Y, MENU_LOGO_WIDTH, MENU_LOGO_HEIGHT);
			
			// ** Draw Buttons ** //
			//    Draw Buttons    //
			// ** Draw Buttons ** //
			
			int buttonX = EscapeGame.SCREEN_WIDTH / 2 - MENU_BUTTON_WIDTH / 2; // Local variable holding the centre of the screen for all buttons
			
			// ---------------------------------------------
			
			// Play - 1st button
			// Checks to see if mouse is inside the texture region, changes to active texture if so, otherwise render inactive, same for remaining buttons
			
			if (Gdx.input.getX() < buttonX + MENU_BUTTON_WIDTH && Gdx.input.getX() > buttonX && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() < MENU_BUTTON_Y1 + MENU_BUTTON_HEIGHT && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() > MENU_BUTTON_Y1) {
				uiBatch.draw(playButtonActive, buttonX, MENU_BUTTON_Y1, MENU_BUTTON_WIDTH, MENU_BUTTON_HEIGHT);
			
		
				if (Gdx.input.justTouched()) {
					Audio.stop("theme"); // Menu theme will stop
					Audio.play("button select"); // Plays audio when button is pressed	
				}
				
				
				// If button is clicked, launch the first Level
				if (Gdx.input.justTouched() && timeDelay < 0) {
					this.dispose();
					game.resetStats();
					game.setScreen(new MapLevelScreen(game, 1));
				}
				
			} else {
				uiBatch.draw(playButtonInactive, buttonX, MENU_BUTTON_Y1, MENU_BUTTON_WIDTH, MENU_BUTTON_HEIGHT);
			}
			
			// ---------------------------------------------
			
			// Instructions - 2nd button
			if (Gdx.input.getX() < buttonX + MENU_BUTTON_WIDTH && Gdx.input.getX() > buttonX && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() < MENU_BUTTON_Y2 + MENU_BUTTON_HEIGHT && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() > MENU_BUTTON_Y2) {
				uiBatch.draw(instructionsButtonInactive, buttonX, MENU_BUTTON_Y2, MENU_BUTTON_WIDTH, MENU_BUTTON_HEIGHT);
				
				if (Gdx.input.justTouched()) {
					Audio.play("button select"); // Plays audio when button is pressed	
					Audio.stop("theme");
				}
				
				
				// If button is clicked, exit program as there is no current screen for this option
				if (Gdx.input.justTouched() && timeDelay < 0) {
					this.dispose();
					game.setScreen(new InstructionsScreen2(game));
				}	
			} else { 
				uiBatch.draw(instructionsButtonActive, buttonX, MENU_BUTTON_Y2, MENU_BUTTON_WIDTH, MENU_BUTTON_HEIGHT);
			}
			
			// ---------------------------------------------
			
			// Highscore - 3rd button
			if (Gdx.input.getX() < buttonX + MENU_BUTTON_WIDTH && Gdx.input.getX() > buttonX && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() < MENU_BUTTON_Y3 + MENU_BUTTON_HEIGHT && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() > MENU_BUTTON_Y3) {
				uiBatch.draw(highscoreButtonActive, buttonX, MENU_BUTTON_Y3, MENU_BUTTON_WIDTH, MENU_BUTTON_HEIGHT);
				
				if (Gdx.input.justTouched()) {
					Audio.play("button select"); // Plays audio when button is pressed
					Audio.stop("theme");
				}	
				
				// If button is clicked, exit program as there is no current screen for this option
				if (Gdx.input.justTouched() && timeDelay < 0) {
					this.dispose();
					game.setScreen(new HighscoreScreen(game));
				}	
			} else { 
				uiBatch.draw(highscoreButtonInactive, buttonX, MENU_BUTTON_Y3, MENU_BUTTON_WIDTH, MENU_BUTTON_HEIGHT);
			}
			
			// ---------------------------------------------
			
			// Exit - 4th button
			
			if (Gdx.input.getX() < buttonX + MENU_BUTTON_WIDTH && Gdx.input.getX() > buttonX && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() < MENU_BUTTON_Y4 + MENU_BUTTON_HEIGHT && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() > MENU_BUTTON_Y4) {
				uiBatch.draw(exitButtonActive, buttonX, MENU_BUTTON_Y4, MENU_BUTTON_WIDTH, MENU_BUTTON_HEIGHT);
				
				// If button is clicked, exit program as there is no current screen for this option
				if (Gdx.input.justTouched() && timeDelay < 0) {
					Gdx.app.exit();
				}	
			} else { 
				uiBatch.draw(exitButtonInactive, buttonX, MENU_BUTTON_Y4, MENU_BUTTON_WIDTH, MENU_BUTTON_HEIGHT);
			}
			
			// Stop SpriteBatch
			
			uiBatch.end();
			
		}
		
		@Override
		public void show() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void resize(int width, int height) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void pause() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void resume() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void hide() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void dispose() {
			// TODO Auto-generated method stub
			
		}
}

