package game.valkyrie.escape.screens;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.utils.Align;

import game.valkyrie.escape.EscapeGame;
import game.valkyrie.escape.audio.Audio;
import game.valkyrie.escape.dodge.entities.DodgePillar;
import game.valkyrie.escape.dodge.entities.DodgePlayer;
import game.valkyrie.escape.dodge.entities.DodgeZombie;
import game.valkyrie.escape.dodge.tools.DodgeScrollingBackground;

/**
 * DodgeGameScreen - Objective, dodge obstacles in your way by jumping over zombies and dodging under pillars. Win by surviving all the obstacles.
 * @author LukeC, ArjunS, Team Valkyrie
 *
 */
public class DodgeGameScreen implements Screen {
	
	// Static Variables
	public static final float UI_DEFAULT_Y = Gdx.graphics.getHeight() - 100; // Default y Position for the UI elements
	public static final float SKATEBOARD_X = 285; // Position of skateboard
	public static final float SKATEBOARD_Y = 145;
	public static final float SKATEBOARD_WIDTH = 191;
	public static final float SKATEBOARD_HEIGHT = 39;
	
	EscapeGame game; // Our escape game used across all screens
	DodgePlayer player;
	DodgePillar pillar;
	DodgePillar pillar2;
	DodgeZombie zombie;
	DodgeZombie zombie2;
	DodgeScrollingBackground background;
	Texture skateboard;
	Texture foreground; // Image that goes atop the scrolling background
	int behaviour; // Determines the obstacles the player will face
	Random random;
	float winPoint; // x coordinate, when reaches a certain point, win minigame
	BitmapFont uiFont; // The font for the UI
	private Screen screen;
	
	public DodgeGameScreen (EscapeGame game, Screen screen) {
		this.game = game;
		this.screen = screen;
		random = new Random();
		
		// Create Objects
		background = new DodgeScrollingBackground();
		
		// Determine position of zombie/pillar based on rando value
		int behaviour = random.nextInt(5) + 1;
		switch (behaviour) {
			case 1: // Two Pillars
				pillar = new DodgePillar(1400);
				pillar2 = new DodgePillar(2200);
				zombie = new DodgeZombie(-1000);
				zombie2 = new DodgeZombie(-1000);
				winPoint = 2600;
			break;
			case 2: // One Zombie One Pillar
				pillar = new DodgePillar(2200);
				pillar2 = new DodgePillar(-2200);
				zombie = new DodgeZombie(1400);
				zombie2 = new DodgeZombie(-1000);
				winPoint = 2600;
			break;
			case 3: // One Zombie Two Pillar
				pillar = new DodgePillar(1200);
				pillar2 = new DodgePillar(3200);
				zombie = new DodgeZombie(2300);
				zombie2 = new DodgeZombie(-1000);
				winPoint = 3600;
			break;
			case 4: // Two Zombie Two Pillar
				pillar = new DodgePillar(3200);
				pillar2 = new DodgePillar(3900);
				zombie = new DodgeZombie(1400);
				zombie2 = new DodgeZombie(2300);
				winPoint = 4300;
			break;
			case 5: // Two Zombie Two Pillar
				pillar = new DodgePillar(1300);
				pillar2 = new DodgePillar(4000);
				zombie = new DodgeZombie(2100);
				zombie2 = new DodgeZombie(3100);
				winPoint = 4400;
			break;
		}
		
		player = new DodgePlayer();
		
		// Load Textures
		skateboard = new Texture("sprites/dodge_skateboard.png");
		foreground = new Texture("sprites/dodge_background.png");
		
		uiFont = new BitmapFont(Gdx.files.internal("fonts/lunchtime.fnt"));
	}
	
	@Override
	public void render(float delta) {
		
		// ** Flare Gun Code
		if (game.hasFlareGun() == true && Gdx.input.isKeyJustPressed(Keys.F)) {
			game.winMiniGame();
			game.setHasFlareGun(false);
			this.dispose();
			game.setScreen(screen);
		}
		
		// ** Scrolling Background Code
		background.update(delta);
		
		// ** Player Code
		player.update(delta);
		
		// ** Pillar and Zombie code
		pillar.update(delta);
		pillar2.update(delta);
		zombie.update(delta);
		zombie2.update(delta);
		
		// ** Win Check
		winPoint -= 850 * delta;
		
		if (winPoint < 170) {
			game.winMiniGame();
			Audio.play("win minigame");
			this.dispose();
			game.setScreen(screen);
		}
		
		// ** After everything, check for collisions
		// Player and Pillar - if collides with either pillar, game over
		if (player.getCollisionRect().collidesWith(pillar.getCollisionRect()) || player.getCollisionRect().collidesWith(pillar2.getCollisionRect())) {
			if (player.getCrouching() == false) {
				this.dispose();
				game.loseMiniGame();
				Audio.play("lose minigame");
				game.setScreen(screen);
			}
		}
		
		// Player and Zombie
		if (player.getCollisionRect().collidesWith(zombie.getCollisionRect()) || player.getCollisionRect().collidesWith(zombie2.getCollisionRect())) {
			this.dispose();
			game.loseMiniGame();
			Audio.play("lose minigame");
			game.setScreen(screen);
		}
		
		// ** CAMERA SETUP AND SPRITE RENDERING
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		game.batch.begin();
		
		// Draw background
		background.render(game.batch);
		
		// Draw skateboard
		game.batch.draw(skateboard, SKATEBOARD_X, SKATEBOARD_Y, SKATEBOARD_WIDTH, SKATEBOARD_HEIGHT);
		
		// Draw player
		player.render(game.batch);
		
		// Draw Pillars and Zombies
		pillar.render(game.batch);
		pillar2.render(game.batch);
		zombie.render(game.batch);
		zombie2.render(game.batch);
		
		// Draw Foreground
		game.batch.draw(foreground, 0, 0, 1280, 720);
		
		// Draw UI
		// If Skip is available, draw saying so
		if (game.hasFlareGun()) {
			GlyphLayout flareGunLayout = new GlyphLayout(uiFont, "PRESS F TO SKIP", Color.YELLOW, 0, Align.left, false);
			uiFont.draw(game.batch, flareGunLayout, 50, 50);
		}
		
		// Win Requirement display	
		GlyphLayout scoreRequiredLayout = new GlyphLayout(uiFont, "Dodge the Obstacles!", Color.WHITE, 0, Align.center, false);
		uiFont.draw(game.batch, scoreRequiredLayout, Gdx.graphics.getWidth() / 2, 650);
		
		game.batch.end();
	}

	@Override
	public void resize(int width, int height) {
	}
	
	@Override
	public void show() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void dispose() {
	}

}
