package game.valkyrie.escape.screens;

import java.util.ArrayList;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.utils.Align;

import game.valkyrie.escape.EscapeGame;
import game.valkyrie.escape.audio.Audio;
import game.valkyrie.escape.map.entities.MapFlareGun;
import game.valkyrie.escape.map.entities.MapFood;
import game.valkyrie.escape.map.entities.MapMoney;
import game.valkyrie.escape.map.entities.MapOverlay;
import game.valkyrie.escape.map.entities.MapPlayer;
import game.valkyrie.escape.map.entities.MapStairs;
import game.valkyrie.escape.map.entities.MapZombie;
import game.valkyrie.escape.map.entities.MapPlayer.PlayerDirection;
import game.valkyrie.escape.map.entities.MapZombie.ZombieDirection;
import game.valkyrie.escape.tools.Notify;

/**
 * The main screen for the game, these generate each of the 3 levels and hold all the functionality for controlling yourself on the map
 * @author LukeC, PriteshP, AmyW, Team Valkyrie
 *
 */
public class MapLevelScreen implements Screen  {

public static final float UI_DEFAULT_Y = Gdx.graphics.getHeight() - 100; // Default y Position for the UI elements
	
	EscapeGame game; // Global EscapeGame variable
	private int levelNumber; // Level number, either 1, 2 or 3
	private TiledMap map; // The map
	private OrthogonalTiledMapRenderer mapRenderer; // The renderer for the map
	private OrthographicCamera camera; // The camera for the screen/minigames
	private MapPlayer mapPlayer;  // Map Player Object
	private MapStairs mapStairs; // Map Stairs Object
	private MapOverlay mapOverlay; // Map overlay object
	private ArrayList<MapZombie> mapZombies; // Holds Zombies
	private ArrayList<MapFlareGun> mapFlareGuns; // Holds flare guns
	private ArrayList<MapFood> mapFoodItems; // Holds food items
	private ArrayList<MapMoney> mapMoneyItems; // Holds money objects
	private ArrayList<Notify> notifyItems; // ArrayList that holds score notifications
	private static Random random; // Random number generator
	private boolean zombieEncountered; // If zombie is encountered, set to true, run game after render.
	private boolean stairsContact; // If in contact with stairs
	private float gameTimer; // Timer, when hits 0, launch a game
	private int zombiesRequired; // How many zombies are needed to progress
	private boolean enoughZombiesRequired; // Check if enough zombies are killed.
	private BitmapFont uiFont; // Font for the UI
	private SpriteBatch uiBatch; // Sprite batch for the UI, seperate from main SpriteBatch
	
	public MapLevelScreen (EscapeGame game, int levelNumber) {
		
		this.game = game;
		this.levelNumber = levelNumber;
		Audio.play("level one music"); // Level one music will begin
		Audio.loop("level one music"); // Level one music will loop
		map = new TiledMap();
		
		// Load map based on level number provided, set number of zombies based on this
		String levelDirectory = "";
		switch (levelNumber) {
			case 1:
			levelDirectory = "maps/level1/level_one_map.tmx";
			zombiesRequired = 3;
			break;
			case 2:
			levelDirectory = "maps/level2/level_two_map.tmx";
			zombiesRequired = 6;
			break;
			case 3:
			levelDirectory = "maps/level3/level_three_map.tmx";
			zombiesRequired = 11;
			break;
		}
		
		map = new TmxMapLoader().load(levelDirectory);
		mapRenderer = new OrthogonalTiledMapRenderer(map);
		mapOverlay = new MapOverlay();
		camera = new OrthographicCamera();
		
		// Setup Populate Arrays
		mapZombies = new ArrayList<MapZombie>();
		mapFlareGuns = new ArrayList<MapFlareGun>();
		mapFoodItems = new ArrayList<MapFood>();
		mapMoneyItems = new ArrayList<MapMoney>();
		notifyItems = new ArrayList<Notify>();
		
		// Populate map based on level number provided
		populatePlayer();
		populateStairs();
		populateFood();
		populateMoney();
		populateFlaregun();
		populateZombie();
		
		zombieEncountered = false;
		gameTimer = 0.05f;
		random = new Random();
		
		uiFont = new BitmapFont(Gdx.files.internal("fonts/lunchtime.fnt"));
		uiBatch = new SpriteBatch();
		
	}
	
	public void populatePlayer() {
		switch (levelNumber) {
			case 1:
				mapPlayer = new MapPlayer(24 * mapPlayer.PLAYER_DIMENSION, 79 * mapPlayer.PLAYER_DIMENSION, map.getLayers().get(0));
			break;
			case 2:
				mapPlayer = new MapPlayer(124 * mapPlayer.PLAYER_DIMENSION, 35 * mapPlayer.PLAYER_DIMENSION, map.getLayers().get(0));
			break;
			case 3:
				mapPlayer = new MapPlayer(11 * mapPlayer.PLAYER_DIMENSION, 156 * mapPlayer.PLAYER_DIMENSION, map.getLayers().get(0));
			break;
		}
	}
	
	public void populateStairs() {
		switch (levelNumber) {
			case 1:
				mapStairs = new MapStairs(82 * mapStairs.STAIRS_DIMENSION, 51 * mapStairs.STAIRS_DIMENSION, map.getLayers().get(0));
			break;
			case 2:
				mapStairs = new MapStairs(122 * mapStairs.STAIRS_DIMENSION, 136 * mapStairs.STAIRS_DIMENSION, map.getLayers().get(0));
			break;
			case 3:
				mapStairs = new MapStairs(148 * mapStairs.STAIRS_DIMENSION, 81 * mapStairs.STAIRS_DIMENSION, map.getLayers().get(0));
			break;
		}
	}
	
	public void populateFood() {
		switch (levelNumber) {
			case 1:
				mapFoodItems.add(new MapFood(86 * MapFood.FOOD_DIMENSION, 44 * MapFood.FOOD_DIMENSION, map.getLayers().get(0))); // Starting position and Collision layer
				mapFoodItems.add(new MapFood(7 * MapFood.FOOD_DIMENSION + 32, 58 * MapFood.FOOD_DIMENSION, map.getLayers().get(0))); 
				mapFoodItems.add(new MapFood(73 * MapFood.FOOD_DIMENSION, 25 * MapFood.FOOD_DIMENSION, map.getLayers().get(0))); 
			break;
			case 2:
				mapFoodItems.add(new MapFood(9 * MapFood.FOOD_DIMENSION, 77 * MapFood.FOOD_DIMENSION, map.getLayers().get(0)));
				mapFoodItems.add(new MapFood(29 * MapFood.FOOD_DIMENSION, 128 * MapFood.FOOD_DIMENSION, map.getLayers().get(0)));
				mapFoodItems.add(new MapFood(126 * MapFood.FOOD_DIMENSION, 77 * MapFood.FOOD_DIMENSION, map.getLayers().get(0)));
				mapFoodItems.add(new MapFood(60 * MapFood.FOOD_DIMENSION, 29 * MapFood.FOOD_DIMENSION, map.getLayers().get(0)));
			break;
			case 3:
				mapFoodItems.add(new MapFood(72 * MapFood.FOOD_DIMENSION, 149 * MapFood.FOOD_DIMENSION, map.getLayers().get(0)));
				mapFoodItems.add(new MapFood(80 * MapFood.FOOD_DIMENSION, (174 - 132) * MapFood.FOOD_DIMENSION, map.getLayers().get(0)));
				mapFoodItems.add(new MapFood(76 * MapFood.FOOD_DIMENSION, (174 - 91) * MapFood.FOOD_DIMENSION, map.getLayers().get(0)));
			break;
		}
	}
	
	public void populateMoney() {
		switch (levelNumber) {
			case 1:
				mapMoneyItems.add(new MapMoney(23 * MapMoney.MONEY_DIMENSION, 84 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0))); // Starting position and Collision layer
				mapMoneyItems.add(new MapMoney(25 * MapMoney.MONEY_DIMENSION, 84 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0))); 
				mapMoneyItems.add(new MapMoney(69 * MapMoney.MONEY_DIMENSION, 70 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0))); 
				mapMoneyItems.add(new MapMoney(73 * MapMoney.MONEY_DIMENSION, 70 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0))); 
				mapMoneyItems.add(new MapMoney(3 * MapMoney.MONEY_DIMENSION, 49 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0))); 
				mapMoneyItems.add(new MapMoney(12 * MapMoney.MONEY_DIMENSION, 48 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0))); 
				mapMoneyItems.add(new MapMoney(27 * MapMoney.MONEY_DIMENSION, 33 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(83 * MapMoney.MONEY_DIMENSION, 44 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0))); 
				mapMoneyItems.add(new MapMoney(84 * MapMoney.MONEY_DIMENSION, 44 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0))); 
				mapMoneyItems.add(new MapMoney(85 * MapMoney.MONEY_DIMENSION, 44 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0))); 
				mapMoneyItems.add(new MapMoney(83 * MapMoney.MONEY_DIMENSION, 43 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0))); 
				mapMoneyItems.add(new MapMoney(84 * MapMoney.MONEY_DIMENSION, 43 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0))); 
				mapMoneyItems.add(new MapMoney(85 * MapMoney.MONEY_DIMENSION, 43 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0))); 
				mapMoneyItems.add(new MapMoney(83 * MapMoney.MONEY_DIMENSION, 42 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0))); 
				mapMoneyItems.add(new MapMoney(84 * MapMoney.MONEY_DIMENSION, 42 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(85 * MapMoney.MONEY_DIMENSION, 42 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(21 * MapMoney.MONEY_DIMENSION, 21 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(21 * MapMoney.MONEY_DIMENSION, 20 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(21 * MapMoney.MONEY_DIMENSION, 19 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(73 * MapMoney.MONEY_DIMENSION, 15 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
			break;
			case 2:
				mapMoneyItems.add(new MapMoney(137 * MapMoney.MONEY_DIMENSION, 33 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(137 * MapMoney.MONEY_DIMENSION, 34 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(84 * MapMoney.MONEY_DIMENSION, 38 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(53 * MapMoney.MONEY_DIMENSION, 37 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(54 * MapMoney.MONEY_DIMENSION, 37 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(55 * MapMoney.MONEY_DIMENSION, 37 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(53 * MapMoney.MONEY_DIMENSION, 36 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(53 * MapMoney.MONEY_DIMENSION, 35 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(21 * MapMoney.MONEY_DIMENSION, 49 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(21 * MapMoney.MONEY_DIMENSION, 48 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(43 * MapMoney.MONEY_DIMENSION, 49 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(43 * MapMoney.MONEY_DIMENSION, 48 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(9 * MapMoney.MONEY_DIMENSION, 68 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(10 * MapMoney.MONEY_DIMENSION, 68 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(12 * MapMoney.MONEY_DIMENSION, 86 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(12 * MapMoney.MONEY_DIMENSION, 85 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(19 * MapMoney.MONEY_DIMENSION, 129 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(21 * MapMoney.MONEY_DIMENSION, 129 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(23 * MapMoney.MONEY_DIMENSION, 129 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(85 * MapMoney.MONEY_DIMENSION, 96 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(86 * MapMoney.MONEY_DIMENSION, 96 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(90 * MapMoney.MONEY_DIMENSION, 58 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(91 * MapMoney.MONEY_DIMENSION, 59 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(93 * MapMoney.MONEY_DIMENSION, 84 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(94 * MapMoney.MONEY_DIMENSION, 84 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(105 * MapMoney.MONEY_DIMENSION, 84 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(106 * MapMoney.MONEY_DIMENSION, 84 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(51 * MapMoney.MONEY_DIMENSION, 56 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(52 * MapMoney.MONEY_DIMENSION, 56 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(53 * MapMoney.MONEY_DIMENSION, 56 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(51 * MapMoney.MONEY_DIMENSION, 45 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(52 * MapMoney.MONEY_DIMENSION, 45 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(53 * MapMoney.MONEY_DIMENSION, 45 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(57 * MapMoney.MONEY_DIMENSION, 62 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(58 * MapMoney.MONEY_DIMENSION, 62 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(59 * MapMoney.MONEY_DIMENSION, 62 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(60 * MapMoney.MONEY_DIMENSION, 62 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
			break;
			case 3:
				mapMoneyItems.add(new MapMoney(39 * MapMoney.MONEY_DIMENSION, 122 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(10 * MapMoney.MONEY_DIMENSION, 100 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(10 * MapMoney.MONEY_DIMENSION, 66 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(10 * MapMoney.MONEY_DIMENSION, 67 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(54 * MapMoney.MONEY_DIMENSION, 44 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(99 * MapMoney.MONEY_DIMENSION, 44 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(117 * MapMoney.MONEY_DIMENSION, 65 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(118 * MapMoney.MONEY_DIMENSION, 65 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(114 * MapMoney.MONEY_DIMENSION, 122 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(114 * MapMoney.MONEY_DIMENSION, 121 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(113 * MapMoney.MONEY_DIMENSION, 149 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(98 * MapMoney.MONEY_DIMENSION, 120 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(59 * MapMoney.MONEY_DIMENSION, 149 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(60 * MapMoney.MONEY_DIMENSION, 149 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(39 * MapMoney.MONEY_DIMENSION, 101 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				mapMoneyItems.add(new MapMoney(39 * MapMoney.MONEY_DIMENSION, 100 * MapMoney.MONEY_DIMENSION, map.getLayers().get(0)));
				
				// Randomise money in the main room
				Random random = new Random();
				int minX = 48;
				int maxX = 105;
				int minY = 54;
				int maxY = 111;
				for (int i = 0; i <= 50; i++) {	
					int moneyX = random.nextInt(maxX - minX + 1) + minX;
					int moneyY = random.nextInt(maxY - minY + 1) + minY;
					mapMoneyItems.add(new MapMoney(moneyX * MapZombie.ZOMBIE_DIMENSION, moneyY * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0)));
				}
			break;
		}
	}
	
	public void populateFlaregun() {
		switch (levelNumber) {
			case 1:
				mapFlareGuns.add(new MapFlareGun(71 * MapFlareGun.FLARE_GUN_DIMENSION, 84 * MapFlareGun.FLARE_GUN_DIMENSION, map.getLayers().get(0))); // Starting position and Collision layer
				mapFlareGuns.add(new MapFlareGun(17 * MapFlareGun.FLARE_GUN_DIMENSION, 33 * MapFlareGun.FLARE_GUN_DIMENSION, map.getLayers().get(0)));
				mapFlareGuns.add(new MapFlareGun(86 * MapFlareGun.FLARE_GUN_DIMENSION, 34 * MapFlareGun.FLARE_GUN_DIMENSION, map.getLayers().get(0)));
			break;
			case 2:
				mapFlareGuns.add(new MapFlareGun(68 * MapFlareGun.FLARE_GUN_DIMENSION, 38 * MapFlareGun.FLARE_GUN_DIMENSION, map.getLayers().get(0)));
				mapFlareGuns.add(new MapFlareGun(69 * MapFlareGun.FLARE_GUN_DIMENSION, 62 * MapFlareGun.FLARE_GUN_DIMENSION, map.getLayers().get(0)));
				mapFlareGuns.add(new MapFlareGun(54 * MapFlareGun.FLARE_GUN_DIMENSION, 94 * MapFlareGun.FLARE_GUN_DIMENSION, map.getLayers().get(0)));
			break;
			case 3:
				mapFlareGuns.add(new MapFlareGun(11 * MapFlareGun.FLARE_GUN_DIMENSION, 150 * MapFlareGun.FLARE_GUN_DIMENSION, map.getLayers().get(0)));
				mapFlareGuns.add(new MapFlareGun(81 * MapFlareGun.FLARE_GUN_DIMENSION, 149 * MapFlareGun.FLARE_GUN_DIMENSION, map.getLayers().get(0)));
				mapFlareGuns.add(new MapFlareGun(72 * MapFlareGun.FLARE_GUN_DIMENSION, 44 * MapFlareGun.FLARE_GUN_DIMENSION, map.getLayers().get(0)));
			break;
		}
	}
	
	public void populateZombie() {
		switch (levelNumber) {
			case 1:
				mapZombies.add(new MapZombie(34 * MapZombie.ZOMBIE_DIMENSION, 79 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.LEFT, 75)); // Starting position and Collision layer and Direction and Speed
				mapZombies.add(new MapZombie(40 * MapZombie.ZOMBIE_DIMENSION, 75 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.DOWN, 35));
				mapZombies.add(new MapZombie(47 * MapZombie.ZOMBIE_DIMENSION, 72 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.DOWN, 140)); 
				mapZombies.add(new MapZombie(45 * MapZombie.ZOMBIE_DIMENSION, 72 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.DOWN, 60));
				mapZombies.add(new MapZombie(48 * MapZombie.ZOMBIE_DIMENSION, 72 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.DOWN, 110)); 
				mapZombies.add(new MapZombie(70 * MapZombie.ZOMBIE_DIMENSION, 81 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.RIGHT, 50)); 
				mapZombies.add(new MapZombie(41 * MapZombie.ZOMBIE_DIMENSION, 52 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.NONE, 0));
				mapZombies.add(new MapZombie(42 * MapZombie.ZOMBIE_DIMENSION, 45 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.NONE, 0));
				mapZombies.add(new MapZombie(52 * MapZombie.ZOMBIE_DIMENSION, 43 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.NONE, 0));
				mapZombies.add(new MapZombie(72 * MapZombie.ZOMBIE_DIMENSION, 52 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.UP, 90));
				mapZombies.add(new MapZombie(78 * MapZombie.ZOMBIE_DIMENSION, 40 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.NONE, 0));
				mapZombies.add(new MapZombie(79 * MapZombie.ZOMBIE_DIMENSION, 41 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.NONE, 0));
				mapZombies.add(new MapZombie(79 * MapZombie.ZOMBIE_DIMENSION, 40 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.NONE, 0));
				mapZombies.add(new MapZombie(47 * MapZombie.ZOMBIE_DIMENSION, 25 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.LEFT, 75));
				mapZombies.add(new MapZombie(63 * MapZombie.ZOMBIE_DIMENSION, 22 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.DOWN, 75));
				mapZombies.add(new MapZombie(32 * MapZombie.ZOMBIE_DIMENSION, 20 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.DOWN, 75));
				mapZombies.add(new MapZombie(21 * MapZombie.ZOMBIE_DIMENSION, 39 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.LEFT, 110));
				mapZombies.add(new MapZombie(22 * MapZombie.ZOMBIE_DIMENSION, 36 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.RIGHT, 100));
			break;
			case 2:
				mapZombies.add(new MapZombie(106 * MapZombie.ZOMBIE_DIMENSION, 34 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.RIGHT, 170));
				mapZombies.add(new MapZombie(106 * MapZombie.ZOMBIE_DIMENSION, 33 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.LEFT, 180));
				mapZombies.add(new MapZombie(124 * MapZombie.ZOMBIE_DIMENSION, 45 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.UP, 150));
				mapZombies.add(new MapZombie(125 * MapZombie.ZOMBIE_DIMENSION, 45 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.DOWN, 200));
				mapZombies.add(new MapZombie(91 * MapZombie.ZOMBIE_DIMENSION, 38 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.DOWN, 40));
				mapZombies.add(new MapZombie(38 * MapZombie.ZOMBIE_DIMENSION, 23 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.RIGHT, 30));
				mapZombies.add(new MapZombie(37 * MapZombie.ZOMBIE_DIMENSION, 22 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.RIGHT, 30));
				mapZombies.add(new MapZombie(36 * MapZombie.ZOMBIE_DIMENSION, 21 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.RIGHT, 30));
				mapZombies.add(new MapZombie(38 * MapZombie.ZOMBIE_DIMENSION, 20 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.RIGHT, 30));
				mapZombies.add(new MapZombie(58 * MapZombie.ZOMBIE_DIMENSION, 28 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.NONE, 0));
				mapZombies.add(new MapZombie(59 * MapZombie.ZOMBIE_DIMENSION, 28 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.NONE, 0));
				mapZombies.add(new MapZombie(60 * MapZombie.ZOMBIE_DIMENSION, 28 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.NONE, 0));
				mapZombies.add(new MapZombie(61 * MapZombie.ZOMBIE_DIMENSION, 28 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.NONE, 0));
				mapZombies.add(new MapZombie(31 * MapZombie.ZOMBIE_DIMENSION, 60 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.UP, 90));
				mapZombies.add(new MapZombie(32 * MapZombie.ZOMBIE_DIMENSION, 60 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.UP, 110));
				mapZombies.add(new MapZombie(33 * MapZombie.ZOMBIE_DIMENSION, 60 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.DOWN, 80));
				mapZombies.add(new MapZombie(34 * MapZombie.ZOMBIE_DIMENSION, 60 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.UP, 120));
				mapZombies.add(new MapZombie(12 * MapZombie.ZOMBIE_DIMENSION, 73 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.UP, 50));
				mapZombies.add(new MapZombie(98 * MapZombie.ZOMBIE_DIMENSION, 60 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.UP, 150));
				mapZombies.add(new MapZombie(101 * MapZombie.ZOMBIE_DIMENSION, 60 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.DOWN, 140));
				mapZombies.add(new MapZombie(85 * MapZombie.ZOMBIE_DIMENSION, 67 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.RIGHT, 210));
				mapZombies.add(new MapZombie(107 * MapZombie.ZOMBIE_DIMENSION, 66 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.RIGHT, 190));
				mapZombies.add(new MapZombie(81 * MapZombie.ZOMBIE_DIMENSION, 83 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.DOWN, 90));
				mapZombies.add(new MapZombie(83 * MapZombie.ZOMBIE_DIMENSION, 83 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.UP, 60));
				mapZombies.add(new MapZombie(56 * MapZombie.ZOMBIE_DIMENSION, 70 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.RIGHT, 60));
				mapZombies.add(new MapZombie(60 * MapZombie.ZOMBIE_DIMENSION, 67 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.RIGHT, 60));
				mapZombies.add(new MapZombie(21 * MapZombie.ZOMBIE_DIMENSION, 104 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.UP, 160));
				mapZombies.add(new MapZombie(23 * MapZombie.ZOMBIE_DIMENSION, 104 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.DOWN, 140));
				mapZombies.add(new MapZombie(80 * MapZombie.ZOMBIE_DIMENSION, 136 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.RIGHT, 30));
				mapZombies.add(new MapZombie(81 * MapZombie.ZOMBIE_DIMENSION, 135 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.RIGHT, 30));
				mapZombies.add(new MapZombie(82 * MapZombie.ZOMBIE_DIMENSION, 134 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.RIGHT, 30));
				mapZombies.add(new MapZombie(81 * MapZombie.ZOMBIE_DIMENSION, 133 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.RIGHT, 30));
				mapZombies.add(new MapZombie(84 * MapZombie.ZOMBIE_DIMENSION, 104 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.RIGHT, 230));
				mapZombies.add(new MapZombie(84 * MapZombie.ZOMBIE_DIMENSION, 103 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.RIGHT, 240));
				mapZombies.add(new MapZombie(84 * MapZombie.ZOMBIE_DIMENSION, 102 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.RIGHT, 250));
				mapZombies.add(new MapZombie(84 * MapZombie.ZOMBIE_DIMENSION, 101 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.RIGHT, 260));
				mapZombies.add(new MapZombie(111 * MapZombie.ZOMBIE_DIMENSION, 123 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.DOWN, 20));
				mapZombies.add(new MapZombie(112 * MapZombie.ZOMBIE_DIMENSION, 123 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.DOWN, 40));
			break;
			case 3:
				mapZombies.add(new MapZombie(54 * MapZombie.ZOMBIE_DIMENSION, 136 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.RIGHT, 30));
				mapZombies.add(new MapZombie(55 * MapZombie.ZOMBIE_DIMENSION, 135 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.RIGHT, 30));
				mapZombies.add(new MapZombie(54 * MapZombie.ZOMBIE_DIMENSION, 134 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.RIGHT, 30));
				mapZombies.add(new MapZombie(53 * MapZombie.ZOMBIE_DIMENSION, 133 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.RIGHT, 30));
				mapZombies.add(new MapZombie(23 * MapZombie.ZOMBIE_DIMENSION, 101 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.UP, 50));
				mapZombies.add(new MapZombie(24 * MapZombie.ZOMBIE_DIMENSION, 100 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.UP, 50));
				mapZombies.add(new MapZombie(25 * MapZombie.ZOMBIE_DIMENSION, 101 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.UP, 40));
				mapZombies.add(new MapZombie(26 * MapZombie.ZOMBIE_DIMENSION, 98 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.UP, 40));
				mapZombies.add(new MapZombie(77 * MapZombie.ZOMBIE_DIMENSION, 31 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.LEFT, 60));
				mapZombies.add(new MapZombie(77 * MapZombie.ZOMBIE_DIMENSION, 30 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.RIGHT, 80));
				mapZombies.add(new MapZombie(77 * MapZombie.ZOMBIE_DIMENSION, 29 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.LEFT, 20));
				mapZombies.add(new MapZombie(127 * MapZombie.ZOMBIE_DIMENSION, 84 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.UP, 1));
				mapZombies.add(new MapZombie(128 * MapZombie.ZOMBIE_DIMENSION, 83 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.DOWN, 2));
				mapZombies.add(new MapZombie(129 * MapZombie.ZOMBIE_DIMENSION, 82 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.UP, 2));
				mapZombies.add(new MapZombie(130 * MapZombie.ZOMBIE_DIMENSION, 81 * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.DOWN, 1));
				
				// Randomise zombies in the main room
				Random random = new Random();
				int minX = 48;
				int maxX = 105;
				int minY = 54;
				int maxY = 111;
				for (int i = 0; i <= 300; i++) {	
					int zombieX = random.nextInt(maxX - minX + 1) + minX;
					int zombieY = random.nextInt(maxY - minY + 1) + minY;
					mapZombies.add(new MapZombie(zombieX * MapZombie.ZOMBIE_DIMENSION, zombieY * MapZombie.ZOMBIE_DIMENSION, map.getLayers().get(0), ZombieDirection.NONE, 0));
				}
			break;
		}
	}
	
	@Override
	public void show() {
	}

	@Override
	public void render(float delta) {
	
		// ** Pause Code
		if (Gdx.input.isKeyJustPressed(Keys.ESCAPE)) {
			game.setScreen(new PauseScreen(game, this));
		}
		
		// ** Notify Code
		ArrayList<Notify> notifyItemsToRemove = new ArrayList<Notify>();
		for (Notify notify : notifyItems) {		
			notify.update(delta);		
			if (notify.toBeRemoved()) {
				notifyItemsToRemove.add(notify);
			}		
		}
		
		// ** Player Movement Code
		// Update player location right, if Right key is pressed
		if (Gdx.input.isKeyPressed(Keys.RIGHT)) {		
			mapPlayer.update(delta, PlayerDirection.RIGHT);	
		}
				
		// Update player location left, if Left key is pressed
		if (Gdx.input.isKeyPressed(Keys.LEFT)) {	
			mapPlayer.update(delta, PlayerDirection.LEFT);	
		}
				
		// Update player location up, if Up key is pressed		
		if (Gdx.input.isKeyPressed(Keys.UP)) {			
			mapPlayer.update(delta, PlayerDirection.UP);			
		}
				
		// Update player location right, if Down key is pressed		
		if (Gdx.input.isKeyPressed(Keys.DOWN)) {		
			mapPlayer.update(delta, PlayerDirection.DOWN);
		}
		
		// ** Zombie Code
		// Update Zombie Locations
		for (MapZombie zombie : mapZombies) {
			zombie.update(delta);
		}
		
		// ** Post Update Code, checking for collisions between player and objects.
		// Player and Zombie
		ArrayList<MapZombie> zombiesToRemove = new ArrayList<MapZombie>(); // Array of zombies collided with
		for (MapZombie zombie : mapZombies) {	
			if (zombie.getCollisionRect().collidesWith(mapPlayer.getCollisionRect())) {	
				zombieEncountered = true;
				zombiesToRemove.add(zombie);	
			}	
		}
		
		// Player and Fire Gun
		ArrayList<MapFlareGun> flareGunsToRemove = new ArrayList<MapFlareGun>(); // Array of flare guns collided with
		for (MapFlareGun flareGun : mapFlareGuns) {	
			if (flareGun.getCollisionRect().collidesWith(mapPlayer.getCollisionRect())) {	
				Audio.play("item collect");
				notifyItems.add(new Notify(game, "Skip Available", mapPlayer.getX() + mapPlayer.PLAYER_DIMENSION / 2, mapPlayer.getY() + mapPlayer.PLAYER_DIMENSION));
				game.setHasFlareGun(true);
				flareGunsToRemove.add(flareGun);				
			}	
		}
		
		// Player and Food
		ArrayList<MapFood> mapFoodItemsToRemove = new ArrayList<MapFood>(); // Array of food collided with
		for (MapFood food : mapFoodItems) {
			if (food.getCollisionRect().collidesWith(mapPlayer.getCollisionRect())) {	
				Audio.play("item collect");
				notifyItems.add(new Notify(game, "+1 Life", mapPlayer.getX() + mapPlayer.PLAYER_DIMENSION / 2, mapPlayer.getY() + mapPlayer.PLAYER_DIMENSION));
				game.addHealth();
				mapFoodItemsToRemove.add(food);	
			}
		}
		
		// Player and Stairs
		stairsContact = false; // Reset for each frame, this is in order to display the warning message that more zombies need to be killed
		if (mapStairs.getCollisionRect().collidesWith(mapPlayer.getCollisionRect())) {
			stairsContact = true;
			if (game.getZombiesKilled() >= zombiesRequired) {
				enoughZombiesRequired = true;
			}
		}
		
		// Player and Money
		ArrayList<MapMoney> moneyToRemove = new ArrayList<MapMoney>(); // Array of money collided with
		for (MapMoney money : mapMoneyItems) {	
			if (money.getCollisionRect().collidesWith(mapPlayer.getCollisionRect())) {		
				Audio.play("item collect");
				notifyItems.add(new Notify(game, "+100 Score", mapPlayer.getX() + mapPlayer.PLAYER_DIMENSION / 2, mapPlayer.getY() + mapPlayer.PLAYER_DIMENSION));
				game.addScore(100);
				moneyToRemove.add(money);				
			}	
		}
		
		// ** CODE TO REMOVE UNEEDED OBJECTS/OBJECTS COLLIDED WITH
		mapZombies.removeAll(zombiesToRemove);
		mapFlareGuns.removeAll(flareGunsToRemove);
		mapFoodItems.removeAll(mapFoodItemsToRemove);
		mapMoneyItems.removeAll(moneyToRemove);
		notifyItems.removeAll(notifyItemsToRemove);
		
		// ** GAME OVER/LEVEL CLEAR CHECKS
		// If player health becomes 0, go to game over screen with a loss
		if (game.getHealth() <= 0) {
			this.dispose();
			game.setScreen(new GameOverScreen(game, game.getScore(), false));
		}
		
		// If player touches stairs and the number of zombies killed is equal to the number required, go to the next level.
		if (enoughZombiesRequired && stairsContact == true) {		
			Audio.stop("level one music");
			Audio.play("you win");
			this.dispose();
			
			// If not level three, set screen to next level, if level 3, set to game over/clear screen
			if (levelNumber == 3) {
				game.setScreen(new GameOverScreen(game, game.getScore(), true));
			} else {
				game.setScreen(new MapLevelScreen(game, levelNumber +1));
			}
		}
		
		// ** Camera setup that follows the player
		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		game.batch.begin();
		game.batch.setProjectionMatrix(camera.combined);
		if (zombieEncountered == false) {	
			camera.position.set(mapPlayer.getX(), mapPlayer.getY(), 0); // By default camera position on (0, 0 ,0)	
		}
		camera.update();
		mapRenderer.setView(camera);
		mapRenderer.render();	
		game.batch.end();
		
		// ** Drawing the various entities
		game.batch.begin();
		
		// Draw Flare Gun
		for (MapFlareGun flareGun : mapFlareGuns) {
			flareGun.render(game.batch);
		}
		
		// Draw Food
		for (MapFood food : mapFoodItems) {
			food.render(game.batch);
		}
		
		// Draw Money
		for (MapMoney money : mapMoneyItems) {
			money.render(game.batch);
		}
		
		// Draw Zombies
		for (MapZombie zombie : mapZombies) {
			zombie.render(game.batch);
		}
		
		// Draw Stairs
		mapStairs.render(game.batch);
		
		// If stairsContact is true, display a warning message if more zombies are required
		if (stairsContact == true && enoughZombiesRequired == false) {
			mapStairs.renderWarningText(game.batch);
		}
		
		// Draw score notification items
		for (Notify notify : notifyItems) {
			notify.render(game.batch);
		}
		
		game.batch.end();
		
		// Draw Overlay, this is the shadow effect around the player
		mapOverlay.render();
		
		// Draw UI
		renderUI();
		
		// Draw Player
		game.batch.begin();
		mapPlayer.render(game.batch);		
		game.batch.end();
		
		// ** Minigame launch, check for encounter after all update/render code
		if (zombieEncountered == true) {
			camera.position.set(1280 / 2, 720 / 2, 0); // By default camera position on (0, 0 ,0). This happens for a split second to enable the minigames' view to be correct	
			gameTimer -= delta; // Each frame counts down the gameTimer before launching the game at 0
			if (gameTimer < 0) {
				zombieEncountered = false;
				gameTimer = 0.05f;
				MiniGame miniGame = randomEnum(MiniGame.class); // Randomly dictate a minigame and then launch it
				//MiniGame miniGame = MiniGame.Breakout;
				switch (miniGame) {	
				case Shooter:
					game.setScreen(new ShooterGameScreen(game, this));
					break;
				case Defender:
					game.setScreen(new DefenderGameScreen(game, this));
					break;
				case Bomb:
					game.setScreen(new BombGameScreen(game, this));
					break;
				case Key:
					game.setScreen(new KeyFallGameScreen(game, this));
					break;
				case Breakout:
					game.setScreen(new BreakoutGameScreen(game, this));
					break;
				case Dodge:
					game.setScreen(new DodgeGameScreen(game, this));
				}
			}
		} else {
			;; // If zombie is not encountered, do nothing
		}

	}

	@Override
	public void resize(int width, int height) {
		camera.viewportWidth = width;
		camera.viewportHeight = height;
		camera.position.set(camera.viewportWidth, camera.viewportHeight, 1);
		camera.update();
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void dispose() {
		map.dispose();
	}
	
	public int getZombiesRequired() {
		return zombiesRequired;
	}
	
	// HUD BATCH TO DRAW UI, SETUP NEW CAMERA WHICH IS A FIXED UI, this is separate from the main sprite batch to prevent camera conflictions
	
	public void renderUI() {	
		OrthographicCamera cam = new OrthographicCamera();
		Matrix4 uiMatrix = cam.combined.cpy();
		uiMatrix.setToOrtho2D(0, 0, 1280, 720);
		uiBatch.setProjectionMatrix(uiMatrix);
		uiBatch.begin();
			
		//Lives
		GlyphLayout hpHeaderLayout = new GlyphLayout(uiFont, "LIVES:", Color.YELLOW, 1f, Align.left, true);
		uiFont.draw(uiBatch, hpHeaderLayout, 100, UI_DEFAULT_Y);
		GlyphLayout hpHeaderCount = new GlyphLayout(uiFont, game.getHealth() + "/3", Color.WHITE, 1f, Align.left, true);
		uiFont.draw(uiBatch, hpHeaderCount, 100, UI_DEFAULT_Y - 40);
			
		//Powerup held
		GlyphLayout flareGunHeaderLayout = new GlyphLayout(uiFont, "FLARE GUN:", Color.YELLOW, 1f, Align.left, true);
		uiFont.draw(uiBatch, flareGunHeaderLayout, 100, UI_DEFAULT_Y - 140);
		if (game.hasFlareGun()) {
			MapFlareGun uiFlareGun = new MapFlareGun(100, UI_DEFAULT_Y - 180 - MapFlareGun.FLARE_GUN_DIMENSION, map.getLayers().get(0));
			uiFlareGun.render(uiBatch);
		}
			
		//Score
		GlyphLayout scoreHeaderLayout = new GlyphLayout(uiFont, "SCORE:", Color.YELLOW, 1f, Align.right, true);
		uiFont.draw(uiBatch, scoreHeaderLayout, 1280 - 100, UI_DEFAULT_Y);
		GlyphLayout scoreCountLayout = new GlyphLayout(uiFont, "" + game.getScore(), Color.WHITE, 1f, Align.right, true);
		uiFont.draw(uiBatch, scoreCountLayout, 1280 - 100, UI_DEFAULT_Y - 40);
			
		//Zombies Killed
		GlyphLayout zombiesKilledHeaderLayout = new GlyphLayout(uiFont, "ZOMBIES\nKILLED:", Color.YELLOW, 1f, Align.right, true);
		uiFont.draw(uiBatch, zombiesKilledHeaderLayout, 1280 - 100, UI_DEFAULT_Y - 140); 
		GlyphLayout zombiesKilledCountLayout = new GlyphLayout(uiFont, game.getZombiesKilled() + "/" + zombiesRequired, Color.WHITE, 1f, Align.right, true);
		uiFont.draw(uiBatch, zombiesKilledCountLayout, 1280 - 100, UI_DEFAULT_Y - 220); 
		uiBatch.end();
		
	}
	
	// Holds all the available minigames and a method for choosing a random one
	
	public enum MiniGame {	
		Shooter, Defender, Bomb, Key, Breakout, Dodge
	}
	
	public static <T extends Enum<?>> T randomEnum (Class<T> game) {
		int x = random.nextInt(game.getEnumConstants().length);
		return game.getEnumConstants()[x];
	}

}
