package game.valkyrie.escape.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Align;

import game.valkyrie.escape.EscapeGame;
import game.valkyrie.escape.audio.Audio;

/**
 * Initial instructions screen which shows main details about the game
 * @author AmyW, LukeC, Team Valkyrie
 *
 */
public class InstructionsScreen2 implements Screen {

	private static final int MENU_BUTTON_WIDTH = 313;
	private static final int MENU_BUTTON_HEIGHT = 50;
	private static final int MINIGAME_BUTTON_WIDTH = 153;
	private static final int MINIGAME_BUTTON_HEIGHT = 35;
	private static final int MENU_BUTTON_Y = 40;
	private static final int MINIGAME_BUTTON_Y = 130;
	private static final int STAIRS_DIMENSIONS = 64;
	
	private BitmapFont uiFont;
	private SpriteBatch uiBatch;
	EscapeGame game; // Our EscapeGame used across all screens.
	Texture menuButtonInactive;
	Texture menuButtonActive;
	Texture minigameButtonInactive;
	Texture minigameButtonActive;
	Texture flaregun;
	Texture food;
	Texture money;
	Texture zombie;
	Texture stairs;
	
	public InstructionsScreen2(EscapeGame game) {
		this.game = game;
		uiBatch = new SpriteBatch();
		menuButtonInactive = new Texture("sprites/menu_button_inactive.png");
		menuButtonActive = new Texture("sprites/menu_button_active.png");
		minigameButtonInactive = new Texture("sprites/minigame_button_inactive.png");
		minigameButtonActive = new Texture("sprites/minigame_button_active.png");
		flaregun = new Texture("sprites/map_flare_gun.png");
		food = new Texture("sprites/map_food.png");
		money = new Texture("sprites/map_money.png");
		zombie = new Texture("sprites/dodge_zombie.png");
		stairs = new Texture("sprites/map_stairs.png");
		
		// Load Font
		uiFont = new BitmapFont(Gdx.files.internal("fonts/lunchtime.fnt"));
	}
	
	@Override
	public void render(float delta) {
		// Set the background as a dark gray and clear the space. In Float.
		Gdx.gl.glClearColor(0.4f, 0.4f,	0.4f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		uiBatch.begin();
		
		// Header
		uiFont.getData().setScale(1.0f);
		GlyphLayout mapLayout = new GlyphLayout(uiFont, "Instructions:", Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, mapLayout, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 50);
		
		// Base Instructions
		uiFont.getData().setScale(0.7f);
		GlyphLayout firstBaseLine = new GlyphLayout(uiFont, "Explore each floor, collect resources, fight zombies, gain points and hunt for the exit.", Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, firstBaseLine, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 120);
		GlyphLayout secondBaseLine = new GlyphLayout(uiFont, "To navigate around the map use the keyboard arrow keys.", Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, secondBaseLine, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 150);
		
		// Powerup Instructions
		uiFont.getData().setScale(0.8f);
		GlyphLayout firstPowerupLine = new GlyphLayout(uiFont, "POWERUPS:", Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, firstPowerupLine, Gdx.graphics.getWidth() / 8, Gdx.graphics.getHeight() - 250);
		uiFont.getData().setScale(0.6f);
		uiBatch.draw(flaregun, Gdx.graphics.getWidth() / 8 - flaregun.getWidth() * 2, Gdx.graphics.getHeight() - 374);
		GlyphLayout flaregunPowerupLine1 = new GlyphLayout(uiFont, "Flare Gun: Gives the", Color.WHITE, 0, Align.left, false);
		uiFont.draw(uiBatch, flaregunPowerupLine1, Gdx.graphics.getWidth() / 8 - 50, Gdx.graphics.getHeight() - 328);
		GlyphLayout flaregunPowerupLine2 = new GlyphLayout(uiFont, "ability to skip a minigame.", Color.WHITE, 0, Align.left, false);
		uiFont.draw(uiBatch, flaregunPowerupLine2, Gdx.graphics.getWidth() / 8 - 50, Gdx.graphics.getHeight() - 358);
		uiBatch.draw(food, Gdx.graphics.getWidth() / 8 - flaregun.getWidth() * 2, Gdx.graphics.getHeight() - 474);
		GlyphLayout foodPowerupLine1 = new GlyphLayout(uiFont, "Food: Gives you", Color.WHITE, 0, Align.left, false);
		uiFont.draw(uiBatch, foodPowerupLine1, Gdx.graphics.getWidth() / 8 - 50, Gdx.graphics.getHeight() - 428);
		GlyphLayout foodPowerupLine2 = new GlyphLayout(uiFont, "+1 Lives.", Color.WHITE, 0, Align.left, false);
		uiFont.draw(uiBatch, foodPowerupLine2, Gdx.graphics.getWidth() / 8 - 50, Gdx.graphics.getHeight() - 458);
		uiBatch.draw(money, Gdx.graphics.getWidth() / 8 - flaregun.getWidth() * 2, Gdx.graphics.getHeight() - 584);
		GlyphLayout moneyPowerupLine1 = new GlyphLayout(uiFont, "Money: Gives you", Color.WHITE, 0, Align.left, false);
		uiFont.draw(uiBatch, moneyPowerupLine1, Gdx.graphics.getWidth() / 8 - 50, Gdx.graphics.getHeight() - 528);
		GlyphLayout moneyPowerupLine2 = new GlyphLayout(uiFont, "+100 Score.", Color.WHITE, 0, Align.left, false);
		uiFont.draw(uiBatch, moneyPowerupLine2, Gdx.graphics.getWidth() / 8 - 50, Gdx.graphics.getHeight() - 558);
		
		// Zombie Instructions
		uiFont.getData().setScale(0.8f);
		GlyphLayout firstZombieLine = new GlyphLayout(uiFont, "ZOMBIES:", Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, firstZombieLine, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 250);
		uiBatch.draw(zombie, Gdx.graphics.getWidth() / 2 - 33, Gdx.graphics.getHeight() - 410, 66, 112);
		uiFont.getData().setScale(0.6f);
		GlyphLayout secondZombieLine = new GlyphLayout(uiFont, "Encountering a zombie begins", Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, secondZombieLine, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 444);
		GlyphLayout thirdZombieLine = new GlyphLayout(uiFont, "a minigame. Click below", Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, thirdZombieLine, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 474);
		GlyphLayout fourthZombieLine = new GlyphLayout(uiFont, "to find out more.", Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, fourthZombieLine, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 504);
		
		// Minigame Button
		int minigameButtonX = EscapeGame.SCREEN_WIDTH / 2 - MINIGAME_BUTTON_WIDTH / 2;
		if (Gdx.input.getX() < minigameButtonX + MINIGAME_BUTTON_WIDTH && Gdx.input.getX() > minigameButtonX && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() < MINIGAME_BUTTON_Y + MINIGAME_BUTTON_HEIGHT && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() > MINIGAME_BUTTON_Y) {
			uiBatch.draw(minigameButtonActive, minigameButtonX, MINIGAME_BUTTON_Y, MINIGAME_BUTTON_WIDTH, MINIGAME_BUTTON_HEIGHT);
								
			// If button is clicked, exit program as there is no current screen for this option
			if (Gdx.input.justTouched()) {
				this.dispose();
				Audio.play("button select");
				game.setScreen(new InstructionsScreen3(game));
			}
			
		} else { 
			uiBatch.draw(minigameButtonInactive, minigameButtonX, MINIGAME_BUTTON_Y, MINIGAME_BUTTON_WIDTH, MINIGAME_BUTTON_HEIGHT);
		}
		
		// Stairs Instructions
		uiFont.getData().setScale(0.8f);
		GlyphLayout firstStairsLine = new GlyphLayout(uiFont, "STAIRS:", Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, firstStairsLine, Gdx.graphics.getWidth() / 8 + 6 * Gdx.graphics.getWidth() / 8, Gdx.graphics.getHeight() - 250);
		uiBatch.draw(stairs, 1120 - STAIRS_DIMENSIONS / 2, Gdx.graphics.getHeight() - 410, STAIRS_DIMENSIONS, STAIRS_DIMENSIONS);
		uiFont.getData().setScale(0.6f);
		GlyphLayout secondStairsLine = new GlyphLayout(uiFont, "Find the stairs to", Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, secondStairsLine, Gdx.graphics.getWidth() / 8 + 6 * Gdx.graphics.getWidth() / 8, Gdx.graphics.getHeight() - 444);
		GlyphLayout thirdStairsLine = new GlyphLayout(uiFont, "go to the next level.", Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, thirdStairsLine, Gdx.graphics.getWidth() / 8 + 6 * Gdx.graphics.getWidth() / 8, Gdx.graphics.getHeight() - 474);
		GlyphLayout fourthStairsLine = new GlyphLayout(uiFont, "You need to kill a certain", Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, fourthStairsLine, Gdx.graphics.getWidth() / 8 + 6 * Gdx.graphics.getWidth() / 8, Gdx.graphics.getHeight() - 504);
		GlyphLayout fifthStairsLine = new GlyphLayout(uiFont, "number of zombies in order", Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, fifthStairsLine, Gdx.graphics.getWidth() / 8 + 6 * Gdx.graphics.getWidth() / 8, Gdx.graphics.getHeight() - 534);
		GlyphLayout sixthStairsLine = new GlyphLayout(uiFont, " to progress.", Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, sixthStairsLine, Gdx.graphics.getWidth() / 8 + 6 * Gdx.graphics.getWidth() / 8, Gdx.graphics.getHeight() - 564);
		
		// Exit Button
		int buttonX = EscapeGame.SCREEN_WIDTH / 2 - MENU_BUTTON_WIDTH / 2;
		if (Gdx.input.getX() < buttonX + MENU_BUTTON_WIDTH && Gdx.input.getX() > buttonX && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() < MENU_BUTTON_Y + MENU_BUTTON_HEIGHT && EscapeGame.SCREEN_HEIGHT - Gdx.input.getY() > MENU_BUTTON_Y) {
			uiBatch.draw(menuButtonActive, buttonX, MENU_BUTTON_Y, MENU_BUTTON_WIDTH, MENU_BUTTON_HEIGHT);
								
			// If button is clicked, exit program as there is no current screen for this option
			if (Gdx.input.justTouched()) {
				this.dispose();
				Audio.play("button select");
				game.setScreen(new MainMenuScreen(game));
			}
			
		} else { 
			uiBatch.draw(menuButtonInactive, buttonX, MENU_BUTTON_Y, MENU_BUTTON_WIDTH, MENU_BUTTON_HEIGHT);
		}
		
		uiBatch.end();
	}
	
	@Override
	public void show() {
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void dispose() {
	}

}
