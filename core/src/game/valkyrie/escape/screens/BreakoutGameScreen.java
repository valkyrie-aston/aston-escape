/**
 * @author Pritesh
 */
package game.valkyrie.escape.screens;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.utils.Align;

import game.valkyrie.escape.EscapeGame;
import game.valkyrie.escape.audio.Audio;
import game.valkyrie.escape.breakout.entities.BreakoutBall;
import game.valkyrie.escape.breakout.entities.BreakoutBrick;
import game.valkyrie.escape.breakout.entities.BreakoutPaddle;
import game.valkyrie.escape.tools.Notify;
import game.valkyrie.escape.tools.Timer;

/**
 * BreakoutGameScreen - Objective: Bounce the ball between the bricks and paddle and survive for 10 seconds to win. If the ball hits the bottom of the screen, you lose. 
 * @author PriteshP, LukeC, Team Valkyrie
 *
 */
public class BreakoutGameScreen implements Screen {
	
	// Many static variables that are used within the class several times.
	public static final int LEFT_BOUNDARY = 290; // The left side of the screen, which the player cannot go past
	public static final int RIGHT_BOUNDARY = 990; // The right side of the screen, which the player cannot go past
	public static final int TOP_BOUNDARY = 660; //The top of the screen
	public static final int BOTTOM_BOUNDARY = 60; //The bottom of the screen
	public static final float UI_DEFAULT_Y = Gdx.graphics.getHeight() - 100; // Default y Position for the UI elements
	
	EscapeGame game; // Our escape game used across all screens
	Timer timer; // Timer for the game
	Texture breakoutBackground; // Background image for the game
	ArrayList<BreakoutBrick> bricks; // Array List holding brick objects
	BreakoutBall ball; // Ball for game
	BreakoutPaddle paddle; // Paddle for game
	ArrayList<Notify> notifyItems; // Messages for items when you gain score
	int score; // Score for the game, zombies give 50 points, survivors give 100 points, lose 200 upon killing survivor
	int scoreRequired; // Score needed to win the game
	BitmapFont uiFont; // The font for the UI
	private Screen screen; // Holds the map screen
	
	public BreakoutGameScreen (EscapeGame game, Screen screen) {
		this.game = game;
		this.screen = screen;
		score = 0;

		timer = new Timer(100, UI_DEFAULT_Y, 15.0f); // Timer, X loc, Y Loc, time as a float
		
		// Load Textures
		breakoutBackground = new Texture("sprites/breakout_background.png");
				
		// Load Objects
		bricks = populateBricks(); // Array List for holding brick objects
		ball = new BreakoutBall();
		paddle = new BreakoutPaddle();
		notifyItems = new ArrayList<Notify>();
		
		// Load UI
		uiFont = new BitmapFont(Gdx.files.internal("fonts/lunchtime.fnt"));
	
	}
	
		// Populate brick objects here
		public ArrayList<BreakoutBrick> populateBricks() {
		ArrayList<BreakoutBrick> populateBricks = new ArrayList<BreakoutBrick>();
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j <= 4; j++) {
				populateBricks.add(new BreakoutBrick (j * 129 + 320, i * 55 + 413)); 
	        }
		}
		return populateBricks;
	}
	
	@Override
	public void render(float delta) {
		// ** Notify Code
		ArrayList<Notify> notifyItemsToRemove = new ArrayList<Notify>();
		for (Notify notify : notifyItems) {	
			notify.update(delta);	
			if (notify.toBeRemoved()) {
				notifyItemsToRemove.add(notify);
			}		
		}
		
		// ** Flare Gun Code
		if (game.hasFlareGun() == true && Gdx.input.isKeyJustPressed(Keys.F)) {
			game.winMiniGame();
			game.setHasFlareGun(false);
			this.dispose();
			game.setScreen(screen);
		}
		
		// ** Game Over Check
		
		// Update Timer
		timer.update(delta);
		
		// Go to map screen with a loss if time is depleted
		if (ball.getY() <= BOTTOM_BOUNDARY) {
			this.dispose();
			Audio.play("lose minigame");
			game.loseMiniGame();
			this.dispose();
			game.setScreen(screen);
		} 
		
		//  Go to map screen with a win if required score is beaten
		if (timer.getTime() <= 0 && (ball.getY() >= BOTTOM_BOUNDARY) ) {
			this.dispose();
			Audio.play("win minigame");
			game.winMiniGame();
			this.dispose();
			game.setScreen(screen);
		}
		
		// ** Paddle Code
		// Update player location left, if right key is pressed
		if (Gdx.input.isKeyPressed(Keys.RIGHT)) {								
			paddle.update(paddle.getX() + paddle.getSpeed() * delta);
			
			// If player is going beyond boundary, place them back in bounds
			if (paddle.getX() + BreakoutPaddle.PADDLE_WIDTH > RIGHT_BOUNDARY) {
				paddle.update(RIGHT_BOUNDARY - BreakoutPaddle.PADDLE_WIDTH);
			}
		}
		
		// Update player location left, if left key is pressed
		if (Gdx.input.isKeyPressed(Keys.LEFT)) {
			paddle.update(paddle.getX() - paddle.getSpeed() * delta);
					
			// If player is going beyond boundary, place them back in bounds
			if (paddle.getX() < LEFT_BOUNDARY) {
				paddle.update(LEFT_BOUNDARY);
				
			}
		}		

		// ** Ball code
		ball.update(delta);

		// ** AFTER ALL UPDATES CHECK FOR COLLISSIONS
		
		// Ball and Paddle
		if (ball.getCollisionRect().collidesWith(paddle.getCollisionRect())) {
			// Reverse Y direction of ball
			ball.changeYdir();
			// Increase speed of paddle and ball
			ball.increaseSpeed(33);
			paddle.increaseSpeed(33);
			System.out.println(ball.getSpeed());
		}
		
		// Ball and Bricks
		ArrayList<BreakoutBrick> bricksToRemove = new ArrayList<BreakoutBrick>();
		for (BreakoutBrick brick : bricks) {
			if (ball.getCollisionRect().collidesWith(brick.getCollisionRect())) { // Collision Occured
				bricksToRemove.add(brick);
				Audio.play("zombie scream");
				notifyItems.add(new Notify(game, "+50", ball.getX() + ball.BALL_WIDTH / 2, ball.getY() + ball.BALL_HEIGHT));
				score += 50; // Get points for brick broken
				ball.changeYdir();
			} else if (ball.getCollisionRect().collidesWith(brick.getRightSide())) {
				bricksToRemove.add(brick);
				Audio.play("zombie scream");
				notifyItems.add(new Notify(game, "+50", ball.getX() + ball.BALL_WIDTH / 2, ball.getY() + ball.BALL_HEIGHT));
				score += 50; // Get points for brick broken
				ball.changeXdir();
				ball.changeYdir();
			} else if (ball.getCollisionRect().collidesWith(brick.getLeftSide())) {
				bricksToRemove.add(brick);
				Audio.play("zombie scream");
				notifyItems.add(new Notify(game, "+50", ball.getX() + ball.BALL_WIDTH / 2, ball.getY() + ball.BALL_HEIGHT));
				score += 50; // Get points for brick broken
				ball.changeXdir();
				ball.changeYdir();

		}	
	}
		

		
		// ** Remove Uneeded Objects
		notifyItems.removeAll(notifyItemsToRemove);
		bricks.removeAll(bricksToRemove);
		
		// ** CAMERA SETUP
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		game.batch.begin();
		
		// Render Sprites
		
		// Draw Background
		game.batch.draw(breakoutBackground, 0, 0);
		
		// Draw bricks
		for (BreakoutBrick brick : bricks) {
			brick.render(game.batch);
		}
		
		// Draw Paddle
		paddle.render(game.batch);
		
		// Draw Ball
		ball.render(game.batch);
		
		// Draw Notify
		for (Notify notify : notifyItems) {
			notify.render(game.batch);
		}
		
		// Draw UI
		
		// Score
		GlyphLayout scoreHeaderLayout = new GlyphLayout(uiFont, "SCORE:", Color.WHITE, 1f, Align.right, true);
		GlyphLayout scoreCountLayout = new GlyphLayout(uiFont, "" + score, Color.YELLOW, 1f, Align.right, true);
		uiFont.draw(game.batch, scoreHeaderLayout, Gdx.graphics.getWidth() - 10 - scoreHeaderLayout.width, UI_DEFAULT_Y);
		uiFont.draw(game.batch, scoreCountLayout, Gdx.graphics.getWidth() - 10 - scoreHeaderLayout.width, UI_DEFAULT_Y - 40);
		
		// Score Requirement display	
		GlyphLayout scoreRequiredLayout = new GlyphLayout(uiFont, "Score " + scoreRequired + " Points!", Color.WHITE, 0, Align.center, false);
		uiFont.draw(game.batch, scoreRequiredLayout, Gdx.graphics.getWidth() / 2, 650);
		
		// Draw Timer
		timer.render(game.batch);
		
		// If Skip is available, draw saying so
		if (game.hasFlareGun()) {
			GlyphLayout flareGunLayout = new GlyphLayout(uiFont, "PRESS F TO SKIP", Color.YELLOW, 0, Align.left, false);
			uiFont.draw(game.batch, flareGunLayout, 50, 50);
		}
				
		game.batch.end();
	}
	
	@Override
	public void show() {
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void dispose() {
	}

}
