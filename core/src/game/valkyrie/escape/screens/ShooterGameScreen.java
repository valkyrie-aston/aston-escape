package game.valkyrie.escape.screens;

import java.util.ArrayList;
import java.util.Random;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

import game.valkyrie.escape.EscapeGame;
import game.valkyrie.escape.audio.Audio;
import game.valkyrie.escape.shooter.entities.ShooterAmmo;
import game.valkyrie.escape.shooter.entities.ShooterBlood;
import game.valkyrie.escape.shooter.entities.ShooterBullet;
import game.valkyrie.escape.shooter.entities.ShooterPlayer;
import game.valkyrie.escape.shooter.entities.ShooterZombie;
import game.valkyrie.escape.shooter.tools.ShooterScrollingBackground;
import game.valkyrie.escape.tools.Notify;
import game.valkyrie.escape.tools.Timer;

/**
 * ShooterGameScreen - Objective, shoot zombies whilst avoiding them hitting you and collect ammo to ensure you can shoot. Get a certain number of points to win
 * @author LukeC, AmyW, Team Valkyrie
 *
 */
public class ShooterGameScreen implements Screen {
	
	// Many static variables that are used within the class several times.
	public static final int RIGHT_BOUNDARY = 880; // the right side of the screen, which the player cannot go past
	public static final int LEFT_BOUNDARY = 400; // The left side of the screen, which the player cannot go past
	public static final float SHOOT_WAIT_TIME = 0.2f; // The minimum delay between shots
	public static final float MIN_ZOMBIE_SPAWN_TIME = 0.3f; // Minimum spawn time for zombie
	public static final float MAX_ZOMBIE_SPAWN_TIME = 0.9f; // Maximum spawn time for zombie
	public static final float MIN_AMMO_SPAWN_TIME = 3.0f; // Minimum spawn time for ammo
	public static final float MAX_AMMO_SPAWN_TIME = 7.0f; // Maximum spawn time for ammo
	public static final float UI_DEFAULT_Y = Gdx.graphics.getHeight() - 100; // Default y Position for the UI elements
	public static final float LIFE_WIDTH_PIXEL = 32; // Width of heart in pixel
	public static final float LIFE_HEIGHT_PIXEL = 32; // Height of heart in pixel
	public static final float LIFE_WIDTH = 50;
	public static final float LIFE_HEIGHT = 50;
	
	EscapeGame game; // Our escape game used across all screens
	ShooterPlayer player; // Player Object
	Timer timer; // Timer for the game
	ArrayList<ShooterBullet> bullets; // List to hold Bullet objects
	ArrayList<ShooterZombie> zombies; //List to hold Zombie objects
	ArrayList<ShooterAmmo> ammoCrates; //List to hold Ammo objects
	ArrayList<ShooterBlood> bloodSplats; // Lost to hold blood splats
	ArrayList<Notify> notifyItems; // Messages for items
	Texture shooterFloor; // Texture of the floor (TEMP)
	Texture shooterBlank; // Blank texture, used for the health bar
	Texture healthHeart; // Represents life count
	Texture healthHeartEmpty; // Represents empty life
	ShooterScrollingBackground background; // Background image
	float shootTimer; // Timer for tracking if Shooting is on cooldown
	float zombieSpawnTimer; // Tracking when a zombie should spawn
	float ammoSpawnTimer; // Tracking when an ammo box should spawn
	BitmapFont uiFont; // The font for the UI
	int ammoCount; // Counts ammo
	int score; // Score for the game, zombies give 50 points
	int scoreRequired; // Score needed to pass the game WILL BE BASED ON LEVEL IN FUTURE
	private Screen screen; // Holds the value of the map screen given
	Random random;
	
	public ShooterGameScreen (EscapeGame game, Screen screen) {
		this.game = game;
		this.screen = screen;
		shootTimer = 0; // Timer for cooldown between shots
		zombieSpawnTimer = 0;
		ammoSpawnTimer = 0;
		ammoCount = 10;
		score = 0;
		scoreRequired = 600;
		
		// Create objects/lists
		player = new ShooterPlayer();
		bullets = new ArrayList<ShooterBullet>(); // List holding bullets
		zombies = new ArrayList<ShooterZombie>(); // List holding zombies
		ammoCrates = new ArrayList<ShooterAmmo>(); // List holding ammmo creates
		bloodSplats = new ArrayList<ShooterBlood>(); // List holding blood splatters
		notifyItems = new ArrayList<Notify>();
		timer = new Timer(100, UI_DEFAULT_Y, 20); // Create timer, place it, set a time
		shooterFloor = new Texture("sprites/shooter_floor.png");
		shooterBlank = new Texture("sprites/shooter_blank.png");
		healthHeart = new Texture("sprites/shooter_heart.png");
		healthHeartEmpty = new Texture("sprites/shooter_heart_empty.png");
		background = new ShooterScrollingBackground();
		background.setSpeedFixed(false); // Have acceleration on the background
		random = new Random();
		uiFont = new BitmapFont(Gdx.files.internal("fonts/lunchtime.fnt"));
		
	}

	@Override
	public void render(float delta) { 
		
		// ** Notify Code
		
		ArrayList<Notify> notifyItemsToRemove = new ArrayList<Notify>();
		for (Notify notify : notifyItems) {
			
			notify.update(delta);
			
			if (notify.toBeRemoved()) {
				notifyItemsToRemove.add(notify);
			}
			
		}
		
		// ** Flare Gun Code
		
		if (game.hasFlareGun() == true && Gdx.input.isKeyJustPressed(Keys.F)) {
			game.winMiniGame();
			game.setHasFlareGun(false);
			this.dispose();
			game.setScreen(screen);
		}
		
		// ** Background Code
		
		// Update Background
		background.update(delta);
		
		// ** Timer Code
		
		// Update Timer
		timer.update(delta);
		
		// Go to map screen with a loss if time is depleted
		
		if (timer.getTime() <= 0) {
			this.dispose();
			Audio.play("lose minigame");
			game.loseMiniGame();
			game.setScreen(screen);
		}
		
		//  Go to map screen with a win if required score is beaten
		
		if (score >= scoreRequired) {
			this.dispose();
			Audio.play("win minigame");
			game.winMiniGame();
			game.setScreen(screen);
		}
		
		// ** Player Movement Code
		
		// Update player location right, if Right key is pressed
		if (Gdx.input.isKeyPressed(Keys.RIGHT)) {
			player.update(player.getX() + ShooterPlayer.SPEED * delta);
			
			// If player is going beyond boundary, place them back in bounds
			if (player.getX() + ShooterPlayer.PLAYER_WIDTH > RIGHT_BOUNDARY) {
				player.update(RIGHT_BOUNDARY - ShooterPlayer.PLAYER_WIDTH);
			}
		}
		
		// Update player location left, if left key is pressed
		if (Gdx.input.isKeyPressed(Keys.LEFT)) {
			player.update(player.getX() - ShooterPlayer.SPEED * delta);
					
			// If player is going beyond boundary, place them back in bounds
			if (player.getX() < LEFT_BOUNDARY) {
				player.update(LEFT_BOUNDARY);
			}
		}			
		
		// ** Shooting code
		
		// If SPACE is pressed, create bullet, whilst there is no cooldown on shooting
		shootTimer += delta; // Every frame, increase shootTimer
		if (Gdx.input.isKeyJustPressed(Keys.SPACE) && shootTimer >= SHOOT_WAIT_TIME && ammoCount > 0) {
			
			shootTimer = 0; // Reset shoot timer
			bullets.add(new ShooterBullet(player.getX() + 12)); // Add bullet at player position + offset
			ammoCount -= 1; // Minus one ammo, must have ammo to shoot
			Audio.play("pass bomb");
			
		} else if (Gdx.input.isKeyJustPressed(Keys.SPACE) && shootTimer >= SHOOT_WAIT_TIME && ammoCount <= 0) {
			
			shootTimer = 0; // Reset shoot timer
			notifyItems.add(new Notify(game, "NO AMMO", player.getX() + player.PLAYER_WIDTH / 2, player.getY() + player.PLAYER_HEIGHT));
			
		}
		
		//Update bullets
		ArrayList<ShooterBullet> bulletsToRemove = new ArrayList<ShooterBullet>();
		for(ShooterBullet bullet : bullets) {
			bullet.update(delta);
			if(bullet.remove) { // If bullet is to be removed, add bullets to the List
				bulletsToRemove.add(bullet);
			}
		}
		
		// ** Zombie Code
		
		// Spawn zombies at random interval at random location
		zombieSpawnTimer -= delta;
		if (zombieSpawnTimer <= 0) { // Count down to 0, make new timer with random time on 0, based on two spawn timer fields
			zombieSpawnTimer = random.nextFloat() * (MAX_ZOMBIE_SPAWN_TIME - MIN_ZOMBIE_SPAWN_TIME) + MIN_ZOMBIE_SPAWN_TIME;
			int low = LEFT_BOUNDARY;
			int high = RIGHT_BOUNDARY;
			int result = random.nextInt(high - low) + low;
			zombies.add(new ShooterZombie(result)); // Between the two boundaries
		}
		
		// Update zombies
		ArrayList<ShooterZombie> zombiesToRemove = new ArrayList<ShooterZombie>();
			for (ShooterZombie zombie : zombies) {
				zombie.update(delta);
				if (zombie.remove) {
					zombiesToRemove.add(zombie);
				}
		}
		
		// ** ammoCrate Code
			
		// Spawn ammo at random interval at random location
		ammoSpawnTimer -= delta;
		if (ammoSpawnTimer <= 0) { // Count down to 0, make new timer with random time on 0, based on two spawn timer fields
			ammoSpawnTimer = random.nextFloat() * (MAX_AMMO_SPAWN_TIME - MIN_AMMO_SPAWN_TIME) + MIN_AMMO_SPAWN_TIME;
			int low = LEFT_BOUNDARY;
			int high = RIGHT_BOUNDARY;
			int result = random.nextInt(high - low) + low + ShooterAmmo.AMMO_WIDTH;
			ammoCrates.add(new ShooterAmmo(result)); // Between the two boundaries
		}
		
		// Update ammo
		ArrayList<ShooterAmmo> ammoToRemove = new ArrayList<ShooterAmmo>();
			for (ShooterAmmo ammo : ammoCrates) {
				ammo.update(delta);
				if (ammo.remove) {
					ammoToRemove.add(ammo);
				}
		}
		
		// ** Blood Splat Code
			
		// Update Blood Splat
			
		ArrayList<ShooterBlood> bloodSplatsToRemove = new ArrayList<ShooterBlood>();
		for (ShooterBlood blood : bloodSplats) {
			blood.update(delta);
			if (blood.remove) {
				bloodSplatsToRemove.add(blood);
			}
		}
			
		// ** AFTER ALL UPDATES, CHECK FOR COLLISSIONS
		
		// Bullets and Zombies
		for (ShooterBullet bullet : bullets) {
			for (ShooterZombie zombie : zombies) {
				if (bullet.getCollisionRect().collidesWith(zombie.getCollisionRect())) { // Collision Occured
					bulletsToRemove.add(bullet);
					zombiesToRemove.add(zombie);
					Audio.play("zombie scream");
					notifyItems.add(new Notify(game, "+50", zombie.getX() + zombie.ZOMBIE_WIDTH / 2, zombie.getY() + zombie.ZOMBIE_HEIGHT));
					score += 50; // Get points for zombie kill
					bloodSplats.add(new ShooterBlood(zombie.getX(), zombie.getY()));
					
				}
			}
		}
		
		// Zombies and Player
		for (ShooterZombie zombie : zombies) {
			if (zombie.getCollisionRect().collidesWith(player.getCollisionRect())) {
				Audio.play("taking damage");
				zombiesToRemove.add(zombie);
				// player.updateHealth(player.getHealth() - 0.1f);
				player.updateHealthCount(player.getHealthCount() - 1);
				
				// Go to gameover screen is life is depleted
				
				if (player.getHealthCount() <= 0) {
					this.dispose();
					game.loseMiniGame();
					game.setScreen(screen);
				}
			}
		}
		
		// Ammo and Player
		for (ShooterAmmo ammo : ammoCrates) {
			if (ammo.getCollisionRect().collidesWith(player.getCollisionRect())) {
				Audio.play("item collect");
				ammoToRemove.add(ammo);
				int ammoToAdd = random.nextInt(10) + 3;
				notifyItems.add(new Notify(game, "+" + ammoToAdd + " Ammo", player.getX() + player.PLAYER_WIDTH / 2, player.getY() + player.PLAYER_HEIGHT));
				ammoCount += ammoToAdd;
			}
		}
			
		// ** Remove unneeded objects

		bullets.removeAll(bulletsToRemove);
		zombies.removeAll(zombiesToRemove);
		ammoCrates.removeAll(ammoToRemove);
		bloodSplats.removeAll(bloodSplatsToRemove);
		notifyItems.removeAll(notifyItemsToRemove);

		// ** CAMERA SETUP
		
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		
		// Render sprites

		game.batch.begin();
		
		game.batch.end();
		
		// Objects rendered last = on top, like a layering system
		
		game.batch.begin();
		
		// Draw background
		background.render(game.batch);
		
		// Draw ammo
		
		for(ShooterAmmo ammo : ammoCrates) {
			ammo.render(game.batch);
		}
		
		// Draw bullets
		
		for(ShooterBullet bullet : bullets) {
			bullet.render(game.batch);
		}
		
		// Draw Blood Splats
		
		for(ShooterBlood blood : bloodSplats) {
			blood.render(game.batch);
		}
		
		// Draw zombies
		
		for(ShooterZombie zombie : zombies) {
			zombie.render(game.batch);
		}	

		// Draw player
		
		player.render(game.batch);
		
		// Render Notify
		
		for (Notify notify : notifyItems) {
			notify.render(game.batch);
		}
		
		// Draw Text
		
		//Score
		GlyphLayout scoreHeaderLayout = new GlyphLayout(uiFont, "SCORE:", Color.WHITE, 1f, Align.right, true);
		GlyphLayout scoreCountLayout = new GlyphLayout(uiFont, "" + score, Color.YELLOW, 1f, Align.right, true);
		uiFont.draw(game.batch, scoreHeaderLayout, Gdx.graphics.getWidth() - 10 - scoreHeaderLayout.width, UI_DEFAULT_Y);
		uiFont.draw(game.batch, scoreCountLayout, Gdx.graphics.getWidth() - 10 - scoreHeaderLayout.width, UI_DEFAULT_Y - 40);
		
		//Ammo
		GlyphLayout ammoHeaderLayout = new GlyphLayout(uiFont, " AMMO:", Color.WHITE, 1f, Align.right, true);
		uiFont.draw(game.batch, ammoHeaderLayout, Gdx.graphics.getWidth() - 10 - ammoHeaderLayout.width, UI_DEFAULT_Y - 140);
		
		// If Ammo = 0, draw ammo count in red
		if (ammoCount == 0) {
			GlyphLayout ammoCountLayout = new GlyphLayout(uiFont, "" + ammoCount, Color.RED, 1f, Align.right, true);
			uiFont.draw(game.batch, ammoCountLayout, Gdx.graphics.getWidth() - 10 - ammoHeaderLayout.width, UI_DEFAULT_Y - 180);
		} else {
			GlyphLayout ammoCountLayout = new GlyphLayout(uiFont, "" + ammoCount, Color.YELLOW, 1f, Align.right, true);
			uiFont.draw(game.batch, ammoCountLayout, Gdx.graphics.getWidth() - 10 - ammoHeaderLayout.width, UI_DEFAULT_Y - 180);
		}
		
		//Life
		GlyphLayout hpHeaderLayout = new GlyphLayout(uiFont, "HEALTH:", Color.WHITE, 1f, Align.right, true);
		uiFont.draw(game.batch, hpHeaderLayout, Gdx.graphics.getWidth() - 10 - hpHeaderLayout.width, UI_DEFAULT_Y - 280);
		
		// Draw player health alongside empty hearts -- THIS NEEDS CLEANUP, I didn't do any calculation for X/Y position, all trial and error!
		
		int heartOffset = 0; // Variables for offset
		int blankHeartOffset = 0;
		
		// Render empty hearts first, 
			for (int i = 0; i < 3; i++) {
				game.batch.draw(healthHeartEmpty, Gdx.graphics.getWidth() - 290 + blankHeartOffset, UI_DEFAULT_Y - 315 - LIFE_HEIGHT, LIFE_WIDTH, LIFE_WIDTH);
				blankHeartOffset += LIFE_WIDTH + 10;
			}
		
			for (int i = 0; i < player.getHealthCount(); i++) {
				game.batch.draw(healthHeart, Gdx.graphics.getWidth() - 290 + heartOffset, UI_DEFAULT_Y - 315 - LIFE_HEIGHT, LIFE_WIDTH, LIFE_WIDTH);
				heartOffset += LIFE_WIDTH + 10;
			}
		
		// Score Requirement display	
		GlyphLayout scoreRequiredLayout = new GlyphLayout(uiFont, "Score " + scoreRequired + " Points!", Color.WHITE, 0, Align.center, false);
		uiFont.draw(game.batch, scoreRequiredLayout, Gdx.graphics.getWidth() / 2, 650);
			
		// Draw Timer
		timer.render(game.batch);
		
		// If Skip is available, draw saying so
		
		if (game.hasFlareGun()) {
			GlyphLayout flareGunLayout = new GlyphLayout(uiFont, "PRESS F TO SKIP", Color.YELLOW, 0, Align.left, false);
			uiFont.draw(game.batch, flareGunLayout, 50, 50);
		}
		
		game.batch.end();
}
	

	// Inherited, empty methods.
	
	@Override
	public void dispose() {}
	
	@Override
	public void show() {}
	
	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void pause() {}

	@Override
	public void resume() {}

	@Override
	public void hide() {}
	
}
