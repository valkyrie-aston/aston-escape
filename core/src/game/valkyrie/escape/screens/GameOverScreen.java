package game.valkyrie.escape.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.utils.Align;

import game.valkyrie.escape.EscapeGame;
import game.valkyrie.escape.audio.Audio;

/**
 * GameOverScreen which is displayed if the player runs out of lives or they escape.
 * @author LukeC, ArjunS, Team Valkyrie
 *
 */
public class GameOverScreen implements Screen {

	// Game Used by Every Screen
	EscapeGame game;
	
	private BitmapFont uiFont;
	private int score;
	private int highscore;
	private boolean winOrLoss;
	private SpriteBatch uiBatch;
	private Preferences prefs;
	
	public GameOverScreen (EscapeGame game, int score, boolean winOrLoss) {
		
		this.game = game;
		this.score = score;
		Audio.stop("level one music"); // Level music will stop playing
		Audio.play("game over");
		this.winOrLoss = winOrLoss;
		uiBatch = new SpriteBatch();
		
		// Searching for highscore in preferences file
		prefs = Gdx.app.getPreferences("escapegame");
		this.highscore = prefs.getInteger("highscore", 0);
		
		// Check if passed score beats highscore
		if (score > highscore) {
			prefs.putInteger("highscore", score);
			prefs.flush(); // Save the file, necessary
		}
		
		// Load Font
		uiFont = new BitmapFont(Gdx.files.internal("fonts/lunchtime.fnt"));
		
	}
	
	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(float delta) {
		
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		// HUD BATCH TO DRAW UI, SETUP NEW CAMERA WHICH IS A FIXED UI
		
		OrthographicCamera cam = new OrthographicCamera();
		Matrix4 uiMatrix = cam.combined.cpy();
		uiMatrix.setToOrtho2D(0, 0, 1280, 720);
		uiBatch.setProjectionMatrix(uiMatrix);
		uiBatch.begin();
		
		// Draw game over text based on win or loss
		if (winOrLoss) {
			GlyphLayout gameOverLayout = new GlyphLayout(uiFont, "YOU ESCAPED", Color.WHITE, 0, Align.center, false);
			uiFont.draw(uiBatch, gameOverLayout, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 100);
		} else {
			GlyphLayout gameOverLayout = new GlyphLayout(uiFont, "GAME OVER", Color.WHITE, 0, Align.center, false);
			uiFont.draw(uiBatch, gameOverLayout, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 100);
		}
		
		// Draw score text
		GlyphLayout scoreLayout = new GlyphLayout(uiFont, "Score:\n" + score, Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, scoreLayout, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 200);	
		GlyphLayout highscoreLayout = new GlyphLayout(uiFont, "Highscore:\n" + highscore, Color.WHITE, 0, Align.center, false);
		uiFont.draw(uiBatch, highscoreLayout, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 300);
		
		// See if menu buttons are being pressed
		
		GlyphLayout mainMenuLayout = new GlyphLayout(uiFont, "Click Anywhere to Return to Menu");
				
		float mainMenuX = Gdx.graphics.getWidth() / 2 - mainMenuLayout.width / 2;
		float mainMenuY = Gdx.graphics.getHeight() / 2- mainMenuLayout.height / 2 - 100;
		float touchX = Gdx.input.getX();
		float touchY = Gdx.graphics.getHeight() - Gdx.input.getY();
				
		uiFont.draw(uiBatch, mainMenuLayout, mainMenuX, mainMenuY);
		
		
		if (Gdx.input.isTouched()) {
			// Try Again
			if (touchX > mainMenuX && touchX < mainMenuX + mainMenuLayout.width && touchY > mainMenuY - mainMenuLayout.height && touchY < mainMenuY);
			this.dispose();
			game.setScreen(new MainMenuScreen(game));
			return;
		} 
		
		uiBatch.end();
		
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
